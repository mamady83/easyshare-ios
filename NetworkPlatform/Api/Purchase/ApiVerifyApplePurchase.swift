//
//  ApiVerifyApplePurchase.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/30/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Domain

public final class ApiVerifyApplePurchase
{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(reciept: String){
        print("verifying purchasse:\(reciept)")
        AF.request(
            MyURLScheme.appleVerify.url,
            method: .post,
            parameters: [
                "token" : reciept
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    print(json)
               
                    
                    if json["status"].bool ?? false {
                        parseClientResponse(json: json["data"])
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(let value):
                    print("verify error \(String(describing: value.errorDescription))")
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

