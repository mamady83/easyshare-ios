//
//  ApiSearch.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/12/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

public final class ApiSearch
{
    var request: DataRequest?
    
    let onSuccess: (([HomeTO])->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (([HomeTO])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func cancel(){
        request?.cancel()
    }
    public func call(query: String){
        request = AF.request(
            MyURLScheme.search.url,
            method: .get,
            parameters: [
                "query": query
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]

                        let files = data["files"]
                        let folders = data["folders"]

                        var items : [HomeTO] = Array()
                        let folderDAO = FolderDAO()
                        let fileDAO = FileDAO()
                        
                        for i in 0..<files.count {
                            let obj = files[i]
                            let fileTO = jsonToFile(json: obj)
                            fileDAO.publicCreateOrUpdate(file: fileTO)
                            items.append(HomeTO(type: HomeCellType.FILE, item: fileTO))
                        }
                        
                        
                        for i in 0..<folders.count {
                            let obj = folders[i]
                            let folderTO = jsonToFolder(json: obj)
                            folderDAO.publicCreateOrUpdate(folder: folderTO)
                            items.append(HomeTO(type: HomeCellType.FOLDER, item: folderTO))
                        }
                        
                        self.onSuccess(items)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(let value):
                    if !(value.errorDescription ?? "").contains("explicitly") {
                        self.onConnectionError()
                    }
                    break
                }
        })
    }
    
}

