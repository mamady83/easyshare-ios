//
//  ApiShareFilesStore.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

final public class ApiShareFilesStore{
    
    let onSuccess: ((LinkTO)->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: ((LinkTO)->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        fileId: Int,
        password: String,
        expires_at: String
    ){
        var params = [
            "file_id": fileId
        ] as [String: Any]
        if password != "" {
            params.updateValue(password, forKey: "password")
        }
        if expires_at != "" {
            params.updateValue(expires_at, forKey: "expired_at")
        }
        AF.request(
            MyURLScheme.shareFilesStore.url,
            method: .post,
            parameters: params,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                       
                        var link = jsonToLink(json: data)
                        link.fileId = fileId
                        LinkDAO().publicCreate(link: link)
                        EventDispatcher.shared().sendGlobalUpdateEvent()
                        self.onSuccess(link)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
