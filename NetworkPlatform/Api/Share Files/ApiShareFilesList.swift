//
//  ApiShareFileList.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

public final class ApiShareFilesList
{
    
    let onSuccess: (([LinkTO])->Void)?
    let onError: ((String)->Void)?
    let onConnectionError: (()->Void)?
    let onInvalidToken: (()->Void)?
    
    public init(onSuccess: (([LinkTO])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(
        file_id: Int,
        page: Int,
        count: Int
    ){
        AF.request(
            MyURLScheme.shareFilesGet.url,
            method: .get,
            parameters: [
                "page": page,
                "count": count,
                "file_id": file_id
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let items = data["items"]
                   
                        var links: [LinkTO] = Array()
                        
                        let linkDao = LinkDAO()
                        linkDao.removeFileLinks(fileId: file_id)
                        
                        for i in 0..<items.count {
                            let obj = items[i]
                            let link = jsonToLink(json: obj)
                            links.append(link)
                            linkDao.publicCreate(link: link)
                        }
                        
                        self.onSuccess?(links)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken?()
                        }else {
                            self.onError?(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError?()
                    break
                }
        })
    }
    
}

