//
//  ApiShareFilesEdit.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

final public class ApiShareFilesUpdate{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        linkId: Int,
        password: String,
        expire_date: String,
        remove_password: Bool,
        remove_expire: Bool
    ){
        var params = [:] as [String: Any]
        if password != "" {
            params.updateValue(password, forKey: "password")
        }
        if expire_date != "" {
            params.updateValue(expire_date, forKey: "expired_at")
        }
        if remove_password {
            params.updateValue(1, forKey: "remove_password")
        }
        if remove_expire {
            params.updateValue(1, forKey: "remove_expire")
        }
        AF.request(
            MyURLScheme.shareFilesEdit(linkId: linkId).url,
            method: .post,
            parameters: params,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    print(json)
                    print(params)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let link = jsonToLink(json: data)
                        LinkDAO().publicCreate(link: link)
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
