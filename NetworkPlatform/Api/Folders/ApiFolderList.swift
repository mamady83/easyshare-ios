//
//  ApiFolderList.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

public final class ApiFolderList
{
    
    let onSuccess: (([FolderTO])->Void)?
    let onError: ((String)->Void)?
    let onConnectionError: (()->Void)?
    let onInvalidToken: (()->Void)?
    
    public init(onSuccess: (([FolderTO])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(page: Int,
              count: Int,
              sort: String){
        AF.request(
            MyURLScheme.foldersGet.url,
            method: .get,
            parameters: [
                "page": page,
                "count": count,
                "sort": sort
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let items = data["items"]
                   
                        var folders : [FolderTO] = Array()
                        let folderDao = FolderDAO()
                        
                        folderDao.removeAll()
                        
                        for i in 0..<items.count {
                            let obj = items[i]
                            let folder = jsonToFolder(json: obj)
                            folders.append(folder)
                            folderDao.publicCreateOrUpdate(folder: folder)
                        }
                        
                        self.onSuccess?(folders)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken?()
                        }else {
                            self.onError?(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError?()
                    break
                }
        })
    }
    
}

