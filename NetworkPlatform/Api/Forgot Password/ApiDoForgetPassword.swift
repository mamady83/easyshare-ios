//
//  ApiDoForgetPassword.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import Domain
import SwiftyJSON

public final class ApiDoForgetPassword
{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
    }
    
    public func call(email: String,
              token: String,
              password: String,
              password_confirmation: String){
        AF.request(
            MyURLScheme.doForgetPassword.url,
            method: .post,
            parameters: [
                "email": email,
                "token": token,
                "password": password,
                "password_confirmation": password_confirmation
            ],
            encoding: URLEncoding()
        ).responseJSON(
            completionHandler: { response in
            
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {

                        let data = json["data"]
                        let client = data["client"]
                        let package = client["package"]
                        
                        let storage = LocalStorage.shared()
                        
                        storage.token = "Bearer \(data["token"].stringValue)"
                        
                        storage.firstName = client["first_name"].stringValue
                        storage.lastName = client["last_name"].stringValue
                        storage.email = client["email"].stringValue
                        storage.avatar = client["avatar"].stringValue
                        
                        storage.packageID = package["id"].intValue
                        storage.packageStartDate = package["start_date"].doubleValue
                        storage.packageEndDate = package["finish_date"].doubleValue
                        
                        storage.isLoggedIn = true
                        self.onSuccess()
                    }else {
                        self.onError(json["message"].stringValue)
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

