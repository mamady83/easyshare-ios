//
//  SendForgotPasswordToken.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON

public final class ApiSendForgotPasswordToken
{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
    }
    
    public func call(email: String){
        AF.request(
            MyURLScheme.sendForgetPasswordToken.url,
            method: .post,
            parameters: [
                "email": email
            ],
            encoding: URLEncoding()
        ).responseJSON(
            completionHandler: {response in
            
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        self.onSuccess()
                    }else {
                        self.onError(json["message"].stringValue)
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
