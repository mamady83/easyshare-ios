//
//  ApiClientUpdate.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import Alamofire
import SwiftyJSON
import Domain

final public class ApiClientUpdate
{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
                onError: ((String)->Void)?,
                onConnectionError: (()->Void)?,
                onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    //MARK: - Update Name
    public func updateName(first_name: String,
                           last_name: String){
        
        AF.request(
            MyURLScheme.clientUpdate.url,
            method: .post,
            parameters: [
                "first_name": first_name,
                "last_name": last_name
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: {response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        LocalStorage.shared().firstName = first_name
                        LocalStorage.shared().lastName = last_name
                        
                        NotificationCenter.default.post(name: Notification.Name("ProfileUpdated"), object: nil)

                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
    //MARK: - Update Password
    public func updatePassword(password: String){
        
        AF.request(
            MyURLScheme.clientUpdate.url,
            method: .post,
            parameters: [
                "password": password
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: {response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
    
    
    //MARK: - Update FCM ID
    public func updateFcmID(fcmID: String){
        
        AF.request(
            MyURLScheme.clientUpdate.url,
            method: .post,
            parameters: [
                "ios_fcm_id": fcmID
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: {response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
    
    
    //MARK: - Update AVATAR
    public func updateAvatar(avatar: Data?){
        if avatar == nil {
            return
        }
        
        AF.upload(multipartFormData: { (formData) in
            let stream = InputStream(data: avatar!)
            formData.append(stream, withLength: 1024, name: "avatar", fileName: "avatar", mimeType: "image/png")
            //formData.append(avatar!, withName: "avatar", mimeType: "image/png")
        }, to: MyURLScheme.clientUpdate.url,
           method: .post,
           headers: [
            "Authorization": LocalStorage.shared().token,
            "Accept": "application/json"
        ]).responseJSON { (response) in
            
            switch(response.result) {
            case .success(let value):
                
                let json = JSON(value)
                if json["status"].bool ?? false {
                    let data = json["data"]
                    LocalStorage.shared().avatar = data["avatar"].stringValue
                    
                    NotificationCenter.default.post(name: Notification.Name("ProfileUpdated"), object: nil)

                    self.onSuccess()
                }else {
                    if response.response?.statusCode == 401 {
                        self.onInvalidToken()
                    }else {
                        self.onError(json["message"].stringValue)
                    }
                    
                }
                
                break
            case .failure(_):
                self.onConnectionError()
                break
            }
        }
        
        
    }
    
    
    
}
