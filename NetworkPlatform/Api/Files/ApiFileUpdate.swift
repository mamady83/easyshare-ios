//
//  ApiFileUpdate.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/17/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

final public class ApiFileUpdate{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        fileId: Int,
        fileName: String,
        folderId: Int
    ){
        AF.request(
            MyURLScheme.filesEdit(fileId: fileId).url,
            method: .post,
            parameters: [
                "name": fileName,
                "folder_id": folderId
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    
                    if json["status"].bool ?? false {
                        FileDAO().update(
                            fileId: fileId,
                            fileName: fileName,
                            folderId: folderId
                        )
                        EventDispatcher.shared().sendGlobalUpdateEvent()
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
