//
//  ApiFilesList.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/17/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

public final class ApiFileList
{
    
    let onSuccess: (([FileTO])->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (([FileTO])->Void)?,
                onError: ((String)->Void)?,
                onConnectionError: (()->Void)?,
                onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(
        folderId: Int,
        page: Int,
        count: Int,
        sort: String
    ){
        AF.request(
            MyURLScheme.filesGet.url,
            method: .get,
            parameters: [
                "page": page,
                "count": count,
                "sort": sort,
                "folder_id": folderId
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let items = data["items"]
                        
                        var files : [FileTO] = Array()
                        let filesDAO = FileDAO()
                        
                        for i in 0..<items.count {
                            let obj = items[i]
                            let file = jsonToFile(json: obj)
                            filesDAO.publicCreateOrUpdate(file: file)
                            files.append(file)
                        }
                        FolderDAO().setFiles(folderId: folderId, files: files)
                        self.onSuccess(files)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

