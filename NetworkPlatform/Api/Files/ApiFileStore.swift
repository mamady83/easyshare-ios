//
//  ApiStore.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/14/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Domain

final public class ApiFileStore{
    
    let onSuccess: ((FileTO)->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: ((FileTO)->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        name: String,
        folder_id: Int,
        key: String,
        fileExtension: String
    ){
        print("extension = \(fileExtension)")

        AF.request(
            MyURLScheme.filesStore.url,
            method: .post,
            parameters: [
                "name": name,
                "folder_id": folder_id,
                "key": key,
                "extension": fileExtension
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    let json = JSON(value)

                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let file = jsonToFile(json: data)
                        self.onSuccess(file)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
