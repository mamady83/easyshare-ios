//
//  ApiFileDelete.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

final public class ApiFileDelete{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
                onError: ((String)->Void)?,
                onConnectionError: (()->Void)?,
                onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        fileId: Int
    ){
        AF.request(
            MyURLScheme.filesDelete(fileId: fileId).url,
            method: .post,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        
                        
                        
                        FileDAO().removeSingle(fileId: fileId)
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
