//
//  ApiFileSingle.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/18/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

public final class ApiFileSingle
{
    
    let onSuccess: ((FileTO)->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: ((FileTO)->Void)?,
                onError: ((String)->Void)?,
                onConnectionError: (()->Void)?,
                onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(
        fileId: Int
    ){
        AF.request(
            MyURLScheme.filesShow(fileId: fileId).url,
            method: .get,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let file = jsonToFile(json: data)
                        FileDAO().publicCreateOrUpdate(file: file)
                        self.onSuccess(file)
              
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

