//
//  ApiRootFilesAndFolders.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Domain

public final class ApiRootFilesAndFolders
{
    
    let onSuccess: (([Any])->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (([Any])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(){
        AF.request(
            MyURLScheme.rootFilesAndFolders.url,
            method: .get,
            parameters: [
                "page": 1,
                "count": 50
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]

                        let files = data["files"]
                        let folders = data["folders"]
                        
                        var items:[Any] = Array()

                        let fileDAO = FileDAO()
                        let folderDAO = FolderDAO()
                        
                        for i in 0..<files.count {
                            let obj = files[i]
                            let file = jsonToFile(json: obj)
                            fileDAO.publicCreateOrUpdate(file: file)
                            items.append(file)
                        }
                        for i in 0..<folders.count {
                            let obj = folders[i]
                            let folder = jsonToFolder(json: obj)
                            folderDAO.publicCreateOrUpdate(folder: folder)
                            items.append(folder)
                        }

                        
                        self.onSuccess(items)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

