//
//  ApiSupportList.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain

final public class ApiSupportList{
    
    let onSuccess: (([SupportTO])->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (([SupportTO])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(){
        AF.request(
            MyURLScheme.supportList.url,
            method: .get,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let messages = json["data"]["messages"]
                        var items: [SupportTO] = Array()
                        for i in 0..<messages.count {
                            let obj = messages[i]
                            items.append(
                                SupportTO(
                                    message: obj["message"].stringValue,
                                    isAdmin: obj["is_admin"].boolValue,
                                    date: obj["created_at"].stringValue
                                )
                            )
                        }
                        self.onSuccess(items.reversed())
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            if json["message"].stringValue == "" {
                                self.onSuccess(Array())
                            }else {
                                self.onError(json["message"].stringValue)
                            }
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
