//
//  ApiGoogleLogin.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/14/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Domain


final public class ApiGoogleLogin{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
    }
    
    public func call(tokenId: String){
        AF.request(
            MyURLScheme.loginGoogle.url,
            method: .post,
            parameters: [
                "token_id": tokenId
            ],
            encoding: URLEncoding()
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        
                        let data = json["data"]
                        
                        parseClientResponse(json: data["client"])
                        
                        LocalStorage.shared().token = "Bearer \(data["token"].stringValue)"
                        LocalStorage.shared().isLoggedIn = true
                        
                        self.onSuccess()
                    }else {
                        self.onError(json["message"].stringValue)
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
