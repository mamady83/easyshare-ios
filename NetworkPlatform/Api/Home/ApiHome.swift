//
//  ApiHome.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/15/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Domain

public final class ApiHome
{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(){
        AF.request(
            MyURLScheme.home.url,
            method: .get,
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]

                        var items:[Any] = Array()
                        
                        for i in 0..<data.count {
                            let obj = data[i]
                            if obj["type"].stringValue == "folder" {
                                items.append(jsonToFolder(json: obj))
                            }else if obj["type"].stringValue == "file" {
                                items.append(jsonToFile(json: obj))
                            }
                        }
                        HomeDAO().saveItems(items: items)
                        
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

