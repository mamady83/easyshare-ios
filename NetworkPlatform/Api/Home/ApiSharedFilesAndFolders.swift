//
//  ApiSharedFilesAndFolders.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/24/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Domain

public final class ApiSharedFilesAndFolders
{
    
    let onSuccess: (([HomeTO])->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (([HomeTO])->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    public func call(sort: String){
        AF.request(
            MyURLScheme.sharedFilesAndFolders.url,
            method: .post,
            parameters: [
                "sort": sort
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": LocalStorage.shared().token,
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        let filesDAO = FileDAO()
                        var files : [HomeTO] = Array()
                        
                        for i in 0..<data.count {
                            let obj = data[i]
                            let file = jsonToFile(json: obj)
                            filesDAO.publicCreateOrUpdate(file: file)
                            files.append(HomeTO(type: HomeCellType.FILE, item: file))
                        }
                        
                        self.onSuccess(files)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
    }
    
}

