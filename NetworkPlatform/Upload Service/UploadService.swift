//
//  UploadService.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/15/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import Domain


public class UploadService {
    
    private static var sharedUploadService: UploadService = {
        return UploadService()
    }()
    
    var requests : [Int:DataRequest?]? = [:]
    
    public class func shared() -> UploadService {
        return sharedUploadService
    }
    
    let uploadDAO = UploadDAO()
    public init(){
        
    }
    var activeUpload: UploadTO? {
        return uploadDAO.activeUpload()
    }
    
    var isEmpty : Bool {
        return uploadDAO.all().isEmpty
    }
    
    var totalUploadCount: Int {
        return uploadDAO.all().count
    }
    
    var notUploadedFilesCount: Int {
        return uploadDAO.all().count
    }
    
    
    
    public func addToUploadQueue(
        uploadId: Int,
        filename: String,
        folderId: Int,
        filepath: URL,
        filesize: Int64,
        filetype: Int,
        fileExtension: String
    ){
        
        print("adding file to upload \(fileExtension) \(folderId)")
        ApiFileRequestStore(onSuccess: {[weak self] (url, key) in
            
            //MARK: - Create new upload table instance
            self?.uploadDAO.createNewUpload(
                uploadId: uploadId,
                folderId: folderId,
                filename: filename,
                filepath: filepath.absoluteString,
                filesize: filesize,
                filetype: filetype,
                fileExtension: fileExtension,
                uploadKey: key
            )
            
            //MARK: - Start the actual upload to Amazon
            if let this = self {
                this.startUpload(
                    uploadId: uploadId,
                    url: url,
                    key: key,
                    filepath: filepath,
                    filesize: filesize
                )
                print("starting actual upload")

            }
            
            }, onError: { (message) in
                
        }, onConnectionError: {
            
        }, onInvalidToken: {
            
        }).call(
            filename: filename,
            folderId: folderId,
            fileExtension: fileExtension
        )
    }
    
    //MARK:- Retry Upload
    public func retryUpload(uploadId: Int) {
        guard let upload = uploadDAO.getSingleUpload(uploadId: uploadId) else {
            return
        }
        guard let filePathURL = URL(string: upload.filepath) else {
            uploadDAO.removeSingle(uploadId: uploadId)
            notifyUploadChange()
            return
        }
        
        var file : Data? = nil
        do{
            file = try Data(contentsOf: filePathURL)
        }catch {
            print(error)
            uploadDAO.removeSingle(uploadId: uploadId)
            notifyUploadChange()
        }
        if file == nil {
            uploadDAO.removeSingle(uploadId: uploadId)
            notifyUploadChange()
            return
        }
        
        ApiFileRequestStore(onSuccess: {[weak self] (url, key) in
            
            //MARK: - Update Upload key
            self?.uploadDAO.updateUploadKey(uploadId: uploadId, key: key)
            
            //MARK: - Start the actual upload to Amazon
            if let this = self {
                this.startUpload(
                    uploadId: uploadId,
                    url: url,
                    key: key,
                    filepath: filePathURL,
                    filesize: upload.filesize
                )
            }
            
            }, onError: { (message) in
                
        }, onConnectionError: {
            
        }, onInvalidToken: {
            
        }).call(
            filename: upload.filename,
            folderId: upload.folderId,
            fileExtension: upload.fileExtension
        )
    }
    
    //MARK: - upload to amazon
    fileprivate func startUpload(
        uploadId: Int,
        url: String,
        key: String,
        filepath: URL,
        filesize: Int64
    ){
        var data : Data? = nil
        do {
           data = try Data(contentsOf: filepath)
        }catch {
            
        }
        
        LocalStorage.shared().activeUploadId = uploadId
        
        uploadDAO.setUploadStatus(
            uploadId: uploadId,
            status: UploadStatus.UPLOADING
        )
        
        notifyUploadChange()
        
        var request = URLRequest(url: URL(string: url)!)
        
        request.httpBody = data
        request.httpMethod = "PUT"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).uploadProgress(queue: .main, closure: { (progress) in
            print("setting progress \(Int(progress.fractionCompleted * 100))")
            self.uploadDAO.setUploadPercent(
                uploadId: uploadId,
                percent: Int(progress.fractionCompleted * 100)
            )
            self.notifyUploadChange()
        }).response {response in
            switch(response.result) {
            case .success(_):
                
                self.validateUpload(uploadId: uploadId)
                print("upload success")
                break
            case .failure(_):
                print("upload failed")
                self.uploadDAO.setUploadStatus(
                    uploadId: uploadId,
                    status: UploadStatus.FAILED
                )
                self.notifyUploadChange()
                
                break
            }
        }
    }
    
    
    //MARK: - validate uploaded file with server
    
    fileprivate func validateUpload(uploadId: Int){
        print("validating upload")
        
        guard let upload = uploadDAO.getSingleUpload(uploadId: uploadId) else{
            uploadDAO.setUploadStatus(
                uploadId: uploadId,
                status: UploadStatus.FAILED
            )
            self.notifyUploadChange()
            return
        }
        ApiFileStore(onSuccess: { [weak self] file in
            var mFile = file
            if let copiedFileUrl = secureCopyItem(at: URL(fileURLWithPath: upload.filepath)) {
                mFile.filePathOffline = copiedFileUrl.absoluteString
                FileDAO().publicCreateOrUpdate(file: mFile)
            }
            FolderDAO().increaseFileCount(folderId: self?.activeUpload?.folderId ?? 0)
            self?.uploadDAO.removeSingle(uploadId: uploadId)
            LocalStorage.shared().activeUploadId = 0
            self?.notifyUploadCompleted()
            }, onError: { [weak self] (message) in
                self?.uploadDAO.setUploadStatus(
                    uploadId: uploadId,
                    status: UploadStatus.FAILED
                )
                self?.notifyUploadChange()
            }, onConnectionError: {[weak self] in
                self?.uploadDAO.setUploadStatus(
                    uploadId: uploadId,
                    status: UploadStatus.FAILED
                )
                self?.notifyUploadChange()
            }, onInvalidToken: {[weak self] in
                self?.uploadDAO.setUploadStatus(
                    uploadId: uploadId,
                    status: UploadStatus.FAILED
                )
                self?.notifyUploadChange()
        }).call(
            name: upload.filename,
            folder_id: upload.folderId,
            key: upload.uploadKey,
            fileExtension: upload.fileExtension
        )
    }
    
    
    fileprivate func notifyUploadChange(){
        EventDispatcher.shared().sendProgressUpdateEvent()
    }
    
    fileprivate func notifyUploadCompleted(){
        EventDispatcher.shared().sendGlobalUpdateEvent()
    }
    
    //MARK:- Cancel Upload
    public func cancelUpload(uploadId: Int) {
        if let request = requests?[uploadId] as? DataRequest {
            request.cancel()
            notifyUploadCompleted()
        }
    }
}

func getDocumentsDirectory() -> URL {
    // find all possible documents directories for this user
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)

    // just send back the first one, which ought to be the only one
    return paths[0]
}

func secureCopyItem(at srcURL: URL) -> URL? {
     let dstURL = getDocumentsDirectory().appendingPathComponent(srcURL.lastPathComponent)
     do {
         if FileManager.default.fileExists(atPath: dstURL.path) {
             try FileManager.default.removeItem(at: dstURL)
         }
         try FileManager.default.copyItem(at: srcURL, to: dstURL)
     } catch (let error) {
         print("Cannot copy item at \(srcURL): \(error)")
         return nil
     }
     return dstURL
}

