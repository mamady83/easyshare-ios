//
//  MyUrlScheme.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//
import Domain
import Alamofire
fileprivate let API_URL = "https://sweetsha.re/api/v1/"

enum MyURLScheme: URLConvertible{
    func asURL() throws -> URL {
        return URL(string: API_URL)!
    }
    
    
    
    //MARK: - Authentication
    case register
    case login
    case loginGoogle
    case loginApple
    case sendVerifyToken
    case doVerify
    //MARK: - Forget Password
    case sendForgetPasswordToken
    case doForgetPassword
    //MARK: - Client
    case clientUpdate
    case me
    //MARK: - Folders
    case foldersGet
    case foldersStore
    case foldersShow(folderId: Int)
    case foldersEdit(folderId: Int)
    case foldersDelete(folderId: Int)
    //MARK: - Files
    case filesGet
    case filesStore
    case filesRequestStore
    case filesShow(fileId: Int)
    case filesEdit(fileId: Int)
    case filesDelete(fileId: Int)
    //MARK: - Share Files
    case shareFilesGet
    case shareFilesStore
    case shareFilesShow
    case shareFilesEdit(linkId: Int)
    case shareFilesDelete(linkId: Int)
    //MARK: - Search
    case search
    //MARK: - Home
    case home
    case products
    case appleVerify
    case sharedFilesAndFolders
    case rootFilesAndFolders
    //MARK: - Support
    case supportSend
    case supportList

    var url : URL  {
        switch self {
        //MARK: - Authentication
        case .register:
            let url = URL(string: API_URL + "register")
            return url!
        case .login:
            let url = URL(string: API_URL + "login")
            return url!
        case .loginGoogle:
            let url = URL(string: API_URL + "login/google")
            return url!
        case .loginApple:
            let url = URL(string: API_URL + "login/apple")
            return url!
        case .sendVerifyToken:
            let url = URL(string: API_URL + "verify/send")
            return url!
        case .doVerify:
            let url = URL(string: API_URL + "verify")
            return url!
        //MARK: - Forget Password
        case .sendForgetPasswordToken:
            let url = URL(string: API_URL + "forgot/send")
            return url!
        case .doForgetPassword:
            let url = URL(string: API_URL + "forgot")
            return url!
        //MARK: - Client
        case .clientUpdate:
            let url = URL(string: API_URL + "update")
            return url!
        case .me:
            let url = URL(string: API_URL + "me")
            return url!
        //MARK: - Folders
        case .foldersGet:
            let url = URL(string: API_URL + "folders")
            return url!
        case .foldersStore:
            let url = URL(string: API_URL + "folders/store")
            return url!
        case  .foldersDelete(folderId: let folderId):
            return URL(string: API_URL + "folders/destroy/\(folderId)")!
        case .foldersEdit(folderId: let folderId):
            return URL(string: API_URL + "folders/update/\(folderId)")!
        case .foldersShow(folderId: let folderId):
            let url = URL(string: API_URL + "folders/\(folderId)")
            return url!
        //MARK: - Files
        case .filesRequestStore:
            return  URL(string: API_URL + "files/requestStore")!
        case .filesStore:
            return  URL(string: API_URL + "files/store")!
        case .filesDelete(fileId: let fileId):
            return  URL(string: API_URL + "files/destroy/\(fileId)")!
        case .filesGet:
            return URL(string: API_URL + "files")!
        case .filesEdit(fileId: let fileId):
            return URL(string: API_URL + "files/update/\(fileId)")!
        case .filesShow(fileId: let fileId):
            return URL(string: API_URL + "files/show/\(fileId)")!
        //MARK: - Share Files
        case .shareFilesGet:
            return  URL(string: API_URL + "share-files")!
        case .shareFilesStore:
            return  URL(string: API_URL + "share-files/store")!
        case .shareFilesShow:
            return  URL(string: API_URL + "share-files/show")!
        case .shareFilesEdit(linkId: let linkId):
            return URL(string: API_URL + "share-files/update/\(linkId)")!
        case .shareFilesDelete(linkId: let linkId):
            return URL(string: API_URL + "share-files/destroy/\(linkId)")!
        //MARK: - Search
        case .search:
            return URL(string: API_URL + "search")!
        //MARK: - Home
        case .home:
            return URL(string: API_URL + "home")!
        case .products:
            return URL(string: API_URL + "products")!
        case .appleVerify:
            return URL(string: API_URL + "transcation/apple/verify")!
        case .sharedFilesAndFolders:
            return URL(string: API_URL + "shared-file-folder")!
        case .rootFilesAndFolders:
            return URL(string: API_URL + "get-files-folders")!
        //MARK: - Support
        case .supportSend:
            return URL(string: API_URL + "support/sendmessage")!
        case .supportList:
            return URL(string: API_URL + "support")!
        }
    }
    
}
