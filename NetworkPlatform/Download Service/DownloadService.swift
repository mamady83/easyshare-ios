//
//  DownloadService.swift
//  NetworkPlatform
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import Domain


public class DownloadService {
    
    private static var sharedDownloadService: DownloadService = {
        return DownloadService()
    }()
    
    var requests : [Int:DataRequest?]? = [:]
    
    public class func shared() -> DownloadService {
        return sharedDownloadService
    }
    
    let downloadDao = DownloadDAO()
    public init(){
        
    }
    
    
    
   
    
    fileprivate func notifyUploadChange(){
        EventDispatcher.shared().sendProgressUpdateEvent()
    }
    
    fileprivate func notifyUploadCompleted(){
        EventDispatcher.shared().sendGlobalUpdateEvent()
    }
    
    //MARK:- Cancel Upload
    public func cancelDownload(downloadId: Int) {
        if let request = requests?[downloadId] as? DataRequest {
            request.cancel()
            notifyUploadCompleted()
        }
    }
}
