//
//  UploadService.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire


public class UploadService {
    
    private static var sharedUploadService: UploadService = {
        return UploadService()
    }()
    
    var requests : [Int:DataRequest?]? = [:]
    
    public class func shared() -> UploadService {
        return sharedUploadService
    }
    
    public init(){
        
    }
    
    var onCompleted: (()->())?
    var onProgressUpadated: ((Double)->()) = {p in}
    var tempUploadUrl = ""
    var tempKey = ""
    var tempFolderId = 0
    var tempFilePath : URL?
    var tempFileExtension = ""
    var tempFileName = ""
    
    public func addToUploadQueue(
        filename: String,
        folderId: Int,
        filepath: URL,
        fileExtension: String
    ){
        tempFolderId = folderId
        tempFilePath = filepath
        tempFileExtension = fileExtension
        tempFileName = filename
        
        ApiFileRequestStore(onSuccess: {[weak self] (url, key) in
            self?.tempUploadUrl = url
            self?.tempKey = key
            
            self?.startUpload()
            
        }, onError: { (message) in
            
        }, onConnectionError: {
            
        }, onInvalidToken: {
            
        }).call(
            filename: filename,
            folderId: folderId,
            fileExtension: fileExtension
        )
    }
    
    
    //MARK: - upload to amazon
    fileprivate func startUpload(){
        var data : Data? = nil
        do {
            if let url = tempFilePath {
                data = try Data(contentsOf: url)
            }
        }catch {
            
        }
        
        var request = URLRequest(url: URL(string: tempUploadUrl)!)
        
        request.httpBody = data
        request.httpMethod = "PUT"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        AF.request(request).uploadProgress(queue: .main, closure: { (progress) in
            print("progress in service = \(progress.fractionCompleted)")
            self.onProgressUpadated(progress.fractionCompleted)
            
        }).response {response in
            switch(response.result) {
            case .success(_):
                
                self.validateUpload()
                print("upload success")
                break
            case .failure(_):
                print("upload failed")
                
                self.onCompleted?()
                
                break
            }
        }
    }
    
    
    //MARK: - validate uploaded file with server
    
    fileprivate func validateUpload(){
        print("validating upload")
        
        ApiFileStore(onSuccess: { [weak self] in
            self?.onCompleted?()
        }, onError: { [weak self] (message) in
            self?.onCompleted?()
        }, onConnectionError: {[weak self] in
            self?.onCompleted?()
        }, onInvalidToken: {[weak self] in
            self?.onCompleted?()
        }).call(
            name: tempFileName,
            folder_id: tempFolderId,
            key: tempKey,
            fileExtension: tempFileExtension
        )
    }
    
    
    
    //MARK:- Cancel Upload
    public func cancelUpload(uploadId: Int) {
        if let request = requests?[uploadId] as? DataRequest {
            request.cancel()
            onCompleted?()
        }
    }
}

func getDocumentsDirectory() -> URL {
    // find all possible documents directories for this user
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    
    // just send back the first one, which ought to be the only one
    return paths[0]
}

func secureCopyItem(at srcURL: URL) -> URL? {
    let dstURL = getDocumentsDirectory().appendingPathComponent(srcURL.lastPathComponent)
    do {
        if FileManager.default.fileExists(atPath: dstURL.path) {
            try FileManager.default.removeItem(at: dstURL)
        }
        try FileManager.default.copyItem(at: srcURL, to: dstURL)
    } catch (let error) {
        print("Cannot copy item at \(srcURL): \(error)")
        return nil
    }
    return dstURL
}

