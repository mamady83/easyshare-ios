//
//  FolderTO.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct FolderTO: Codable {

    public let folderName: String
    public let folderId: Int
    public var filesCount: Int
    public let hasShared: Bool
    public let modifiedAt: Int64

    public init(folderName: String,
                folderId: Int,
                filesCount: Int,
                hasShared: Bool,
                modifiedAt: Int64) {
        self.folderName = folderName
        self.folderId = folderId
        self.filesCount = filesCount
        self.hasShared = hasShared
        self.modifiedAt = modifiedAt
    }
}

extension FolderTO: Equatable {
    public static func == (lhs: FolderTO, rhs: FolderTO) -> Bool {
        return lhs.folderName == rhs.folderName &&
            lhs.folderId == rhs.folderId &&
            lhs.filesCount == rhs.filesCount
    }
}
