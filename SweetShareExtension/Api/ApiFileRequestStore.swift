//
//  ApiFileRequestStore.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Alamofire
import SwiftyJSON

final public class ApiFileRequestStore{
    
    let onSuccess: ((String, String)->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: ((String, String)->Void)?,
                onError: ((String)->Void)?,
                onConnectionError: (()->Void)?,
                onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        filename: String,
        folderId: Int,
        fileExtension: String
    ){

        print("extension = \(fileExtension)")
        AF.request(
            MyURLScheme.filesRequestStore.url,
            method: .post,
            parameters: [
                "name": filename,
                "key": "pijaspoidfijasopdijfa",
                "folder_id": folderId,
                "extension": fileExtension
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": UserDefaults(suiteName: "group.sweetshare.app")?.string(forKey: "TOKEN") ?? "",
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: {response in
                
                switch(response.result) {
                case .success(let value):
                    let json = JSON(value)
                    if json["status"].bool ?? false {
                        let data = json["data"]
                        
                        
                        self.onSuccess(data["url"].stringValue, data["key"].stringValue)
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                        
                    }
                    break
                case .failure(_):
                    self.onConnectionError()
                    break
                }
        })
        
    }
    
}
