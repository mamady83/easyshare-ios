//
//  MyUrlSchecme.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire

fileprivate let API_URL = "https://sweetsha.re/api/v1/"

enum MyURLScheme: URLConvertible{
    func asURL() throws -> URL {
        return URL(string: API_URL)!
    }
    
    
    
    //MARK: - Folders
    case foldersGet
    //MARK: - Files
    case filesStore
    case filesRequestStore
    
    var url : URL  {
        switch self {
        //MARK: - Folders
        case .foldersGet:
            let url = URL(string: API_URL + "folders")
            return url!
            
        //MARK: - Files
        case .filesRequestStore:
            return  URL(string: API_URL + "files/requestStore")!
        case .filesStore:
            return  URL(string: API_URL + "files/store")!
            
        }
    }
}
