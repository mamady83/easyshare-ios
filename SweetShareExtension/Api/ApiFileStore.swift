//
//  ApiFileStore.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final public class ApiFileStore{
    
    let onSuccess: (()->Void)!
    let onError: ((String)->Void)!
    let onConnectionError: (()->Void)!
    let onInvalidToken: (()->Void)!
    
    public init(onSuccess: (()->Void)?,
         onError: ((String)->Void)?,
         onConnectionError: (()->Void)?,
         onInvalidToken: (()->Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onConnectionError = onConnectionError
        self.onInvalidToken = onInvalidToken
    }
    
    
    public func call(
        name: String,
        folder_id: Int,
        key: String,
        fileExtension: String
    ){
        print("extension = \(fileExtension)")

        AF.request(
            MyURLScheme.filesStore.url,
            method: .post,
            parameters: [
                "name": name,
                "folder_id": folder_id,
                "key": key,
                "extension": fileExtension
            ],
            encoding: URLEncoding(),
            headers: [
                "Authorization": UserDefaults(suiteName: "group.sweetshare.app")?.string(forKey: "TOKEN") ?? "",
                "Accept": "application/json"
            ]
        ).responseJSON(
            completionHandler: { response in
                
                switch(response.result) {
                case .success(let value):
                    let json = JSON(value)

                    if json["status"].bool ?? false {
                        self.onSuccess()
                    }else {
                        if response.response?.statusCode == 401 {
                            self.onInvalidToken()
                        }else {
                            self.onError(json["message"].stringValue)
                        }
                    }
                    
                    break
                case .failure(_):
                    
                    self.onConnectionError()
                    break
                }
        })
    }
    
}
