//
//  ShareViewController.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import MobileCoreServices

@objc(ShareExtensionViewController)
class ShareExtensionViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    
    var items : [FolderTO] = Array()
    var uploadId: Int = 0
    let loading : UIActivityIndicatorView = UIActivityIndicatorView(style: .white)
    let percent = UILabel()
    let progress = UIProgressView()

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = 20
        flowLayout.minimumLineSpacing = 24
        flowLayout.scrollDirection = .vertical
        
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
        
        loading.color = UIColor(named: "colorTextPrimary")
        getFolders()
    }
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loading.frame.origin = CGPoint(x: view.bounds.width / 2, y: (view.bounds.height / 2) - 100)
    }
    @IBAction func onCancelClicked(_ sender: Any) {
        setShareCompleted()
    }
    
    
    func getFolders(){
        collectionView.addSubview(loading)
        loading.startAnimating()
        ApiFolderList(onSuccess: {[weak self] (folders) in
            self?.loading.stopAnimating()
            self?.loading.removeFromSuperview()
            self?.items = folders
            self?.items.append(FolderTO(folderName: "Sweet Share", folderId: 0, filesCount: 0, hasShared: false, modifiedAt: 0))
            print("recieved folders \(self?.items.count ?? 0)")
            self?.collectionView.reloadData()
            }, onError: { [weak self] (message) in
                self?.loading.stopAnimating()
                self?.loading.removeFromSuperview()
                print(message)

            }, onConnectionError: { [weak self] in
                self?.loading.stopAnimating()
                print("onConnectionError")
            }, onInvalidToken: { [weak self] in
                self?.loading.stopAnimating()
                print("invalid token")
        }).call(page: 1, count: 50, sort: "date")
    }
    
    @IBAction func dismissClicked(_ sender: Any) {
        self.setShareCompleted()
    }
    
    private func uploadFile(folderId: Int) {
        //dump(self.extensionContext?.inputItems.first)
        // extracting the path to the URL that is being shared
        let attachments = (self.extensionContext?.inputItems.first as? NSExtensionItem)?.attachments ?? []

        for provider in attachments {
            let type = provider.registeredTypeIdentifiers
            let uti = type.first!
            // Check if the content type is the same as we expected
            if provider.hasItemConformingToTypeIdentifier(uti) {
                provider.loadItem(forTypeIdentifier: uti, options: nil) { (data, error) in
                    guard error == nil else {
                        print(error ?? "")
                        return
                    }
                    dump(data)
                    if let url = data as? URL {
                        self.startUpload(folderId: folderId, url: url)
                    }
                }
          
            }
        }
    }
    
    private func setShareCompleted(){
        self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
    }
    
    private func showLoadingView(
        
    ){
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.5
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)

        
        let box = UIView(frame: CGRect(x: 40, y: view.center.y - 80, width: view.frame.width - 80, height: 160))
        box.backgroundColor = UIColor(named: "colorBackground")
        box.layer.cornerRadius = 16
        box.layer.shadowColor = UIColor.black.cgColor
        box.layer.shadowOffset = CGSize(width: 0, height: 2)
        box.layer.shadowRadius = 18
        box.layer.shadowOpacity = 0.1
        box.clipsToBounds = false
        view.addSubview(box)
        
        let title = UILabel(frame: CGRect(x: 0, y: 20, width: box.frame.width, height: 30))
        title.textAlignment = .center
        title.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
        title.textColor = UIColor(named: "colorTextPrimary")
        title.text = "Uploading"
        box.addSubview(title)
        
        progress.frame = CGRect(x: 20, y: box.frame.height/2, width: box.frame.width - 40, height: 4)
        progress.backgroundColor = UIColor(named: "colorUploadCellBG")
        progress.progressTintColor = UIColor(named: "colorBlue")
        progress.progressViewStyle = .bar
        box.addSubview(progress)
        
        percent.frame = CGRect(x: 0, y: box.frame.height - 50, width: box.frame.width, height: 20)
        percent.text = "0%"
        percent.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        percent.textColor = UIColor(named: "colorTextSecondary")
        percent.textAlignment = .center
        box.addSubview(percent)
        
        
    }
    
    
    private func startUpload(folderId: Int, url: URL){
        DispatchQueue.main.async {
            self.showLoadingView()
        }
        UploadService.shared().addToUploadQueue(filename: url.lastPathComponent, folderId: folderId, filepath: url, fileExtension: url.pathExtension)
        UploadService.shared().onCompleted = {[weak self] in
            self?.setShareCompleted()
        }
        UploadService.shared().onProgressUpadated = { [weak self] fractionCompleted in
            print("progress = \(fractionCompleted)")
            self?.percent.text = "\(Int(fractionCompleted * 100))%"
            self?.progress.setProgress(Float(fractionCompleted), animated: true)
        }
    }
}


extension ShareExtensionViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return items.count
       }
       func numberOfSections(in collectionView: UICollectionView) -> Int {
           return 1
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShareCollectionCell", for: indexPath) as! ShareFolderCollectionCell
           cell.titleLable.text = items[indexPath.row].folderName
           cell.subtitleLable.text = "\(items[indexPath.row].filesCount) files"
           return cell
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           uploadFile(folderId: items[indexPath.row].folderId)
       }
       
       
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
           return CGSize(width: collectionView.bounds.width, height: 40)
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let totalWidth = collectionView.bounds.width - 60
           let itemWidth = totalWidth / 2
           return CGSize(width: itemWidth, height: itemWidth * 1.3)
       }
}
