//
//  FolderCollectionCell.swift
//  SweetShareExtension
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Foundation


class ShareFolderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var folderImage: UIImageView!
    @IBOutlet weak var titleLable: RoundedLable!
    @IBOutlet weak var subtitleLable: RoundedLable!
    
    var onEllipsisClickedClosure : (()->())?
    
    override func awakeFromNib() {
        folderImage.layer.cornerRadius = 10
    }
}

@IBDesignable
public class RoundedLable : UILabel {
    
 
    @IBInspectable
    public var roundFontSize: CGFloat = 2.0 {
        didSet {
              commonInit()
        }
    }
    

    @IBInspectable
    var light: Bool = false
    
    @IBInspectable
    var medium: Bool = false
    
    @IBInspectable
    var heavy: Bool = false
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        var mWeight = UIFont.Weight.bold
        if medium {
            mWeight = UIFont.Weight.medium
        }else if light {
            mWeight = UIFont.Weight.light
        }else if heavy {
            mWeight = UIFont.Weight.heavy
        }
        self.font = FontKit.roundedFont(ofSize: roundFontSize, weight: mWeight)
    }

}

class FontKit {

 static func roundedFont(ofSize fontSize: CGFloat, weight: UIFont.Weight) -> UIFont {
    let systemFont = UIFont.systemFont(ofSize: fontSize, weight: weight)
    let font: UIFont

    if #available(iOS 13.0, *) {
        if let descriptor = systemFont.fontDescriptor.withDesign(.rounded) {
            font = UIFont(descriptor: descriptor, size: fontSize)
        } else {
            font = systemFont
        }
    } else {
        font = systemFont
    }

    return font
    }
}

