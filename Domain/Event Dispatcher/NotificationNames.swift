//
//  NotificationNames.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation


public struct NotificationNames {
    
    public static let updateUpload = Notification.Name("UploadUpdate")
    public static let globalUpdate = Notification.Name("GlobalUpdate")

}
