//
//  EventDispatcher.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public class EventDispatcher {
    
    
    private static var sharedEventDispatcher: EventDispatcher = {
        return EventDispatcher()
    }()
    
    public class func shared() -> EventDispatcher {
        return sharedEventDispatcher
    }
    
    public func sendProgressUpdateEvent(){
       NotificationCenter.default.post(
        name: NotificationNames.updateUpload,
        object: nil
        )
    }
    
    public func sendGlobalUpdateEvent(){
       NotificationCenter.default.post(
        name: NotificationNames.globalUpdate,
        object: nil
        )
    }
}
