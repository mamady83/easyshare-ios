//
//  FileParser.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import SwiftyJSON

public func jsonToFile(json: JSON) -> FileTO {
    var type = 0
    switch json["meta"]["type"].stringValue {
    case "photo":
        type = FileType.PHOTO
        break
    case "video":
        type = FileType.VIDEO
        break
    case "document":
        type = FileType.DOCUMENT
        break
    case "music":
        type = FileType.MUSIC
        break
    default:
        type = FileType.DOCUMENT
    }
    return FileTO(
        fileId: json["id"].intValue,
        folderId: json["folder_id"].intValue,
        fileName: json["name"].stringValue,
        fileSize: json["meta"]["size"].doubleValue,
        filePathOnline: json["path"].stringValue,
        filePathOffline: "",
        fileType: type,
        thumbnailUrl: "",
        fileExtension: json["meta"]["orginal_extension"].stringValue,
        hasShared: json["has_shared"].intValue == 1,
        modifiedAt: json["modified_at"].int64Value
    )
}
