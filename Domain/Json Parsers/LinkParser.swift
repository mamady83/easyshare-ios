//
//  LinkParser.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import SwiftyJSON

public func jsonToLink(json: JSON) -> LinkTO {
    return LinkTO(
        linkId: json["id"].intValue,
        folderId: json["folder_id"].int ?? 0,
        fileId: json["file_id"].int ?? 0,
        url: json["url"].stringValue,
        expires_at: json["expired_at"].int64Value,
        isPasswordProtected: json["is_password_protected"].boolValue
    )
}
