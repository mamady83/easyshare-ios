//
//  ParseClientResponse.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import SwiftyJSON



public func parseClientResponse(json: JSON){
    let package = json["package"]
    let storageDetail = json["storage"]

    let storage = LocalStorage.shared()
    
    storage.clientID = json["id"].intValue
    storage.firstName = json["first_name"].stringValue
    storage.lastName = json["last_name"].stringValue
    storage.email = json["email"].stringValue
    storage.avatar = json["avatar"].stringValue
    
    storage.packageID = package["id"].intValue
    storage.packageStartDate = package["start_date"].doubleValue
    storage.packageEndDate = package["finish_date"].doubleValue
    storage.isVIP = package["is_vip"].boolValue
    print("is vip = \(package["is_vip"].boolValue)")
    
    storage.storageUsed = Double(storageDetail["used"].stringValue) ?? 0
    storage.totalStorage = Double(storageDetail["total"].stringValue) ?? 0
    let storageItems = storageDetail["used_by_type"]
    let storageDAO = StorageDAO()
    storageDAO.removeAll()
    for i in 0..<storageItems.count {
        let obj = storageItems[i]
        let size = Int64(obj["size"].stringValue) ?? 0
        
        var type = FileType.DOCUMENT
        
        switch obj["file_type_name"].stringValue {
        case "photo":
            type = FileType.PHOTO
            break
        case "video":
            type = FileType.VIDEO
            break
        case "music":
            type = FileType.MUSIC
            break
        case "document":
            type = FileType.DOCUMENT
            break
        default:
            type = FileType.DOCUMENT
            break
        }
        let s = StorageTO(title: "", color: UIColor(named: "colorBlue")!, space: "", value: 0, type: type, size: size)
        storageDAO.publicCreate(storage: s)
    }
}
