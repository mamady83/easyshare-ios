//
//  ProductParser.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/30/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import SwiftyJSON

public func jsonToProduct(json: JSON) -> ProductTO {
    return ProductTO(
        productId: json["id"].intValue,
        name: json["name"].stringValue,
        picture: json["picture"].stringValue,
        apple_product_id: json["apple_product_id"].stringValue,
        duration: json["duration"].intValue,
        storage: json["storage"].intValue,
        files_count: json["files_count"].intValue,
        info: json["info"].stringValue,
        price: json["price"].floatValue
    )
}
