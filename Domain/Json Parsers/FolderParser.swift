//
//  FolderParser.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import SwiftyJSON

public func jsonToFolder(json: JSON) -> FolderTO {
    return FolderTO(
        folderName: json["name"].stringValue,
        folderId: json["id"].intValue,
        filesCount: json["file_count"].int ?? 0,
        hasShared: json["has_shared"].intValue == 1,
        modifiedAt: json["modified_at"].int64Value
    )
}

