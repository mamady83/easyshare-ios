//
//  Utils.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
public struct Units {
  
  public let bytes: Int64
  
  public var kilobytes: Double {
    return Double(bytes) / 1_024
  }
  
  public var megabytes: Double {
    return kilobytes / 1_024
  }
  
  public var gigabytes: Double {
    return megabytes / 1_024
  }
  
  public init(bytes: Int64) {
    self.bytes = bytes
  }
  
  public func getReadableUnit() -> String {
    
    switch bytes {
    case 0..<1_024:
      return "\(bytes) bytes"
    case 1_024..<(1_024 * 1_024):
      return "\(String(format: "%.0f", kilobytes)) KB"
    case 1_024..<(1_024 * 1_024 * 1_024):
      return "\(String(format: "%.0f", megabytes)) MB"
    case (1_024 * 1_024 * 1_024)...Int64.max:
      return "\(String(format: "%.0f", gigabytes)) GB"
    default:
      return "\(bytes) bytes"
    }
  }
}


extension Date {
    func today() -> Date{
         return Calendar.current.startOfDay(for: self)
     }
     func yesterday() -> Date {
         let gregorian = Calendar(identifier: .gregorian)
        let components = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .month, .day], from: self)) ?? Date()
        return gregorian.date(byAdding: .day, value: -1, to: components) ?? Date()
     }
     func startOfWeek() -> Date {
         let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) ?? Date()
        return gregorian.date(byAdding: .day, value: 1, to: sunday) ?? Date()
     }
     func startOfMonth() -> Date {
         let calendar = Calendar(identifier: .gregorian)
         let components = calendar.dateComponents([.year, .month], from: self)
         return  calendar.date(from: components)!
     }
     func startOfYear() -> Date {
         let calendar = Calendar(identifier: .gregorian)
         let components = calendar.dateComponents([.year], from: self)
         return  calendar.date(from: components)!
     }
}
