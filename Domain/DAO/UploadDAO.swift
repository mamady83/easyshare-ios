//
//  UploadDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/15/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class UploadDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = UploadTable
    public typealias TO = UploadTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertUploads(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func removeSingle(uploadId: Int){
        if let upload = findSingle(uploadId: uploadId) {
            do {
                try realm.write {
                    realm.delete(upload)
                }
            }catch {
                print(error)
            }
        }
    }
    
    public func notUploadedItems() -> [TO] {
        let items = Array(realm.objects(DB.self).filter("status == \(UploadStatus.FAILED)"))
        return convertUploads(dbItms: items)
    }
    
    public func activeUpload() -> TO? {
        if let activeUpload = realm.objects(DB.self).filter("uploadId = \(LocalStorage.shared().activeUploadId)").first {
            return convertUpload(db: activeUpload)
        }
        if let activeUpload = realm.objects(DB.self).filter("status = \(UploadStatus.FAILED)").first {
            return convertUpload(db: activeUpload)
        }
        return nil
    }
    
    fileprivate func findSingle(uploadId : Int) -> DB? {
        return realm.objects(DB.self).filter("uploadId = \(uploadId)").first
    }
    
    public func updateUploadKey(uploadId: Int, key: String) {
        if let upload = realm.objects(DB.self).filter("uploadId = \(uploadId)").first {
            do{
                try realm.write {
                    upload.uploadKey = key
                }
            }catch{
                print(error)
            }
        }
    }
    public func getSingleUpload(uploadId: Int) -> TO? {
        if let db = findSingle(uploadId: uploadId) {
            return convertUpload(db: db)
        }
        return nil
    }
    
    public func createNewUpload(
        uploadId: Int,
        folderId: Int,
        filename: String,
        filepath: String,
        filesize: Int64,
        filetype: Int,
        fileExtension: String,
        uploadKey: String
        ){
        
        let db = DB()
        db.uploadId = uploadId
        db.percent = 0
        db.folderId = folderId
        db.status = UploadStatus.UPLOADING
        db.filename = filename
        db.filepath = filepath
        db.filesize = filesize
        db.filetype = filetype
        db.fileExtension = fileExtension
        
        
        db.uploadKey = uploadKey
        
        try! realm.write {
            realm.add(db)
        }
    }
    
    public func setUploadStatus(uploadId: Int, status: Int) {
        if let upload = findSingle(uploadId: uploadId) {
            try! realm.write{
                upload.status = status
            }
        }
    }
    
    public func setUploadPercent(uploadId: Int, percent: Int) {
        if let upload = findSingle(uploadId: uploadId) {
            try! realm.write{
                upload.percent = percent
            }
        }
    }
}
