//
//  StorageDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//
//
import RealmSwift

public class StorageDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = StorageTable
    public typealias TO = StorageTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertStorages(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func removeSingle(type: Int){
        if let db = realm.objects(DB.self).filter("type = \(type)").first {
            do{
                try realm.write {
                    realm.delete(db)
                }
            }catch {
                print(error)
            }
        }
    }
    
    public func getSingle(type: Int) -> TO?{
        if let db = realm.objects(DB.self).filter("type = \(type)").first {
            return convertStorage(db: db)
        }
        return nil
    }
    
    public func publicCreate(storage: TO) {
        _ = create(storage: storage)
    }
    
    func create(storage: TO) -> DB {
        let db = convertStorage(to: storage)
        removeSingle(type: storage.type)
        try! realm.write() {
            realm.add(db)
        }
        return db
    }
}
