//
//  FileDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class FileDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = FileTable
    public typealias TO = FileTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertFiles(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func removeSingle(fileId: Int){
        if let db = realm.objects(DB.self).filter("fileId = \(fileId)").first {
            if let url = URL(string: db.filePathOffline), FileManager.default.fileExists(atPath: url.path) {
                do {
                    try FileManager.default.removeItem(at: url)
                }catch {
                    
                }
            }
            do{
                try realm.write {
                    realm.delete(db)
                }
            }catch {
                print(error)
            }
        }
    }
    
    
    
    public func getSingle(fileId: Int) -> FileTO?{
        if let db = realm.objects(DB.self).filter("fileId = \(fileId)").first {
            return convertFile(db: db)
        }
        return nil
    }
    
    public func update(fileId: Int, fileName: String, folderId: Int){
        if let db = realm.objects(DB.self).filter("fileId = \(fileId)").first {
            do{
                try realm.write {
                    db.fileName = fileName
                    db.folderId = folderId
                }
            }catch {
                print(error)
            }
        }
    }
    public func publicCreateOrUpdate(file: TO) {
        _ = createOrUpdate(file: file)
    }
    
    func createOrUpdate(file: TO) -> DB {
        if let temp = realm.objects(DB.self).filter("fileId = \(file.fileId)").first {
            try! realm.write() {
                temp.folderId = file.folderId
                temp.fileName = file.fileName
                temp.filePathOnline = file.filePathOnline
                temp.modifiedAt = file.modifiedAt
                temp.hasShared = file.hasShared
                temp.fileExtension = file.fileExtension
                if file.filePathOffline != "" {
                    temp.filePathOffline = file.filePathOffline
                }
            }
            return temp
        }else {
            let db = convertFile(to: file)
            try! realm.write() {
                realm.add(db)
            }
            return db
        }
    }
}
