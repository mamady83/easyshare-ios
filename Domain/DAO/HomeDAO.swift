//
//  HomeDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class HomeDAO : BaseDAO {
    var realm: Realm
    
        
    typealias DB = HomeTable
    public typealias TO = HomeTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func allHomeFiles() -> [HomeTO] {
        let items = Array(realm.objects(DB.self).filter("type = \(HomeCellType.FILE)"))
        return convertHomes(dbItms: items)
    }
    
    public func allHomeFolders() -> [HomeTO] {
        let items = Array(realm.objects(DB.self).filter("type = \(HomeCellType.FOLDER)"))
        return convertHomes(dbItms: items)
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertHomes(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            print(error)
        }
    }
    
    public func getAllSorted(sort: String) -> [TO]{
        if sort == "alphabet" {
            return all().sorted { (homeOne, homeTwo) -> Bool in
                var titleOne = ""
                var titleTwo = ""
                if homeOne.item is FileTO {
                    titleOne = (homeOne.item as! FileTO).fileName
                }else if homeOne.item is FolderTO {
                    titleOne = (homeOne.item as! FolderTO).folderName
                }
                if homeTwo.item is FileTO {
                    titleTwo = (homeTwo.item as! FileTO).fileName
                }else if homeTwo.item is FolderTO {
                    titleTwo = (homeTwo.item as! FolderTO).folderName
                }
                return titleOne < titleTwo
            }
            
        }else {
            var resultItems: [TO] = Array()

            var todayItems: [TO] = Array()
            var yesterdayItems: [TO] = Array()
            var thisWeekItems: [TO] = Array()
            var thisMonthItems: [TO] = Array()
            var thisYearItems: [TO] = Array()
            var olderItems: [TO] = Array()

            let items = all().sorted { (homeOne, homeTwo) -> Bool in
                var dateOne : Int64 = 0
                var dateTwo : Int64 = 0
                if homeOne.item is FileTO {
                    dateOne = (homeOne.item as! FileTO).modifiedAt
                }else if homeOne.item is FolderTO {
                    dateOne = (homeOne.item as! FolderTO).modifiedAt
                }
                if homeTwo.item is FileTO {
                    dateTwo = (homeTwo.item as! FileTO).modifiedAt
                }else if homeTwo.item is FolderTO {
                    dateTwo = (homeTwo.item as! FolderTO).modifiedAt
                }
                
                let d1 = Date(timeIntervalSince1970: TimeInterval(dateOne))
                let d2 = Date(timeIntervalSince1970: TimeInterval(dateTwo))

                return d1.compare(d2) == .orderedDescending
            }
            
            for item in items {
                var dateTimeStamp : Int64 = 0
                if item.item is FileTO {
                    dateTimeStamp = (item.item as! FileTO).modifiedAt
                }else if item.item is FolderTO {
                    dateTimeStamp = (item.item as! FolderTO).modifiedAt
                }
                
                let diff = dayDifference(from: TimeInterval(dateTimeStamp))
                
                if diff == "Today"{
                    todayItems.append(item)
                }else if diff == "Yesterday" {
                    yesterdayItems.append(item)
                }else if diff == "This Week" {
                    thisWeekItems.append(item)
                }else if diff == "This Month"{
                    thisMonthItems.append(item)
                }else if diff == "This Year" {
                    thisYearItems.append(item)
                }else {
                    olderItems.append(item)
                }
            }
            
            
            if !todayItems.isEmpty {
                resultItems.append(TO(type: HomeCellType.HEADER, item: "Today"))
                resultItems.append(contentsOf: todayItems)
            }
            
            if !yesterdayItems.isEmpty {
                resultItems.append(TO(type: HomeCellType.HEADER, item: "Yesterday"))
                resultItems.append(contentsOf: yesterdayItems)
            }
            
            if !thisWeekItems.isEmpty {
                resultItems.append(TO(type: HomeCellType.HEADER, item: "This Week"))
                resultItems.append(contentsOf: thisWeekItems)
            }
            
            if !thisMonthItems.isEmpty {
                resultItems.append(TO(type: HomeCellType.HEADER, item: "This Month"))
                resultItems.append(contentsOf: thisMonthItems)
            }
            
            if !thisYearItems.isEmpty {
                resultItems.append(TO(type: HomeCellType.HEADER, item: "This Year"))
                resultItems.append(contentsOf: thisYearItems)
            }
            resultItems.append(contentsOf: olderItems)

            return resultItems
        }
    }
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day > 8 {
                return "This Week"
            }else if day > 32 {
                return "This Month"
            }else if day > 366 {
                return "This Year"
            }else {
                return ""
            }
        }
    }
    public func saveItems(items: [Any]) {
        removeAll()
        for item in items {
            let db = HomeTable()
            if let folder = item as? FolderTO {
                let folderDB = FolderDAO().createOrUpdate(folder: folder)
                db.folder = folderDB
                db.type = HomeCellType.FOLDER
            }else if let file = item as? FileTO {
                let fileDB = FileDAO().createOrUpdate(file: file)
                db.file = fileDB
                db.type = HomeCellType.FILE
            }
            do {
                try realm.write {
                    realm.add(db)
                }
            }catch{
                print(error)
            }
        }
    }
    
}
