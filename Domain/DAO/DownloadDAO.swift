//
//  DownloadDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class DownloadDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = DownloadTable
    public typealias TO = DownloadTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertDownloads(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func removeSingle(downloadId: Int){
        if let upload = findSingle(downloadId: downloadId) {
            do {
                try realm.write {
                    realm.delete(upload)
                }
            }catch {
                print(error)
            }
        }
    }
    
    
    fileprivate func findSingle(downloadId : Int) -> DB? {
        return realm.objects(DB.self).filter("downloadId = \(downloadId)").first
    }
    
    public func getSingleDownload(downloadId: Int) -> TO? {
        if let db = findSingle(downloadId: downloadId) {
            return convertDownload(db: db)
        }
        return nil
    }
    
  
    public func setDownloadStatus(downloadId: Int, status: Int) {
        if let upload = findSingle(downloadId: downloadId) {
            try! realm.write{
                upload.status = status
            }
        }
    }
    
    public func setDownloadPercent(downloadId: Int, percent: Int) {
        if let upload = findSingle(downloadId: downloadId) {
            try! realm.write{
                upload.percent = percent
            }
        }
    }
}
