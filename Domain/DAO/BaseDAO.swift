//
//  BaseDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/16/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import RealmSwift

protocol BaseDAO {
    
    var realm: Realm { get }
    
    associatedtype DB: Any
    associatedtype TO: Any
    
    func all() -> [TO]
    func removeAll() -> Void
    
}
