//
//  FolderDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class FolderDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = FolderTable
    public typealias TO = FolderTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertFolders(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func increaseFileCount(folderId: Int) {
        if let folder = realm.objects(DB.self).filter("folderId = \(folderId)").first {
            do {
                try realm.write() {
                    folder.filesCount += 1
                }
            }catch {
                
            }
        }
    }
    public func getSingle(folderId: Int) -> TO? {
        if let db = realm.objects(DB.self).filter("folderId = \(folderId)").first {
            return convertFolder(db: db)
        }
        return nil
    }
    
    public func publicCreateOrUpdate(folder: TO) {
        _ = createOrUpdate(folder: folder)
    }
    public func removeSingle(folderId: Int){
        if let db = realm.objects(DB.self).filter("folderId = \(folderId)").first {
            let files = getFiles(folderId: folderId)
            for f in files {
                FileDAO().removeSingle(fileId: f.fileId)
            }
            do{
                try realm.write {
                    realm.delete(db)
                }
            }catch {
                print(error)
            }
        }
    }
    public func setFiles(folderId: Int, files: [FileTO]) {
        do {
            try realm.write {
                realm.delete(
                    realm.objects(FolderFilesTable.self).filter("folderId = \(folderId)")
                )
            }
            for  file in files {
                let relation = FolderFilesTable()
                relation.folderId = folderId
                relation.fileId = file.fileId
                
                try realm.write {
                    realm.add(relation)
                }
            }
        }catch{
            print(error)
        }
    }
    
    public func getFiles(folderId: Int) -> [FileTO] {
        let filesDAO = FileDAO()
        var items: [FileTO] = Array()
        let fileIds = realm.objects(FolderFilesTable.self).filter("folderId = \(folderId)").map {$0.fileId}
        for id in fileIds {
            if let file = filesDAO.getSingle(fileId: id) {
                items.append(file)
            }
        }
        return items
    }
    
    func createOrUpdate(folder: TO) ->  DB {
        
        if let temp = realm.objects(DB.self).filter("folderId = \(folder.folderId)").first {
            try! realm.write() {
                temp.folderName = folder.folderName
                temp.filesCount = folder.filesCount
            }
            return temp
        }else {
            let db = convertFolder(to: folder)
            try! realm.write() {
                realm.add(db)
            }
            return db
        }
    }
}
