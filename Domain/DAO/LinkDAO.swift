//
//  LinkDAO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

public class LinkDAO : BaseDAO {
    
    var realm: Realm
    
    typealias DB = LinkTable
    public typealias TO = LinkTO
    
    
    public init() {
        realm = MyRealm.db
    }
    
    public func all() -> [TO] {
        let items = Array(realm.objects(DB.self))
        return convertLinks(dbItms: items)
    }
    
    public func removeAll() {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self))
            }
        }catch {
            
        }
    }
    
    public func removeFileLinks(fileId: Int) {
        do{
            try realm.write {
                realm.delete(realm.objects(DB.self).filter("fileId = \(fileId)"))
            }
        }catch {
            print(error)
        }
    }
    
    public func getFileLinks(fileId: Int) -> [LinkTO] {
        let items = Array(realm.objects(DB.self).filter("fileId = \(fileId)"))
        return convertLinks(dbItms: items)
    }
    
    
    public func removeSingle(linkId: Int){
        if let db = realm.objects(DB.self).filter("linkId = \(linkId)").first {
            do{
                try realm.write {
                    realm.delete(db)
                }
            }catch {
                print(error)
            }
        }
    }
    
    public func getSingle(linkId: Int) -> TO?{
        if let db = realm.objects(DB.self).filter("linkId = \(linkId)").first {
            return convertLink(db: db)
        }
        return nil
    }
    
    public func publicCreate(link: TO) {
        _ = create(link: link)
    }
    
    func create(link: TO) -> DB {
        let db = convertLink(to: link)
        removeSingle(linkId: link.linkId)
        try! realm.write() {
            realm.add(db)
        }
        return db
    }
}
