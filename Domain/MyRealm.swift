//
//  RealmConfig.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import RealmSwift

public class MyRealm {
    
    private static var sharedRealm: Realm = {
        var config = Realm.Configuration()
        
        let containerUrl = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.sweetshare.app")
        print(containerUrl!)
        
        let url = containerUrl?.appendingPathComponent("SweetShare.realm")
        print(url!)
        
        config.fileURL = url
        print(config.fileURL!)
        return try! Realm(configuration: config)
    }()

    public static var db : Realm {
        return sharedRealm
    }
    
}
