//
//  DownloadTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class DownloadTable: Object {
    @objc dynamic var downloadId: Int = 0
    @objc dynamic var percent: Int = 0
    @objc dynamic var fileId: Int = 0
    @objc dynamic var status: Int = 0
}
