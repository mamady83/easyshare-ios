//
//  ProductsTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/30/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import RealmSwift

class ProductsTable: Object {
    @objc dynamic var productId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var picture: String = ""
    @objc dynamic var apple_product_id: String = ""
    @objc dynamic var duration: Int = 0
    @objc dynamic var storage: Int = 0
    @objc dynamic var files_count: Int = 0
    @objc dynamic var info: String = ""
    @objc dynamic var price: Float = 0
}
