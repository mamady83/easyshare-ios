//
//  FolderTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class FolderTable: Object {
    @objc dynamic var folderName = ""
    @objc dynamic var folderId: Int = 0
    @objc dynamic var filesCount: Int = 0
    @objc dynamic var modifiedAt: Int64 = 0
    @objc dynamic var hasShared: Bool = false

}
