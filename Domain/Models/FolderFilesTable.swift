//
//  FolderFilesTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/17/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class FolderFilesTable: Object {
    
    @objc dynamic var fileId: Int = 0
    @objc dynamic var folderId: Int = 0
}
