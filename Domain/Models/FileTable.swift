//
//  FileTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class FileTable: Object {
    
    @objc dynamic var fileId: Int = 0
    @objc dynamic var folderId: Int = 0
    @objc dynamic var fileName: String = ""
    @objc dynamic var fileSize: Double = 0
    @objc dynamic var filePathOnline: String = ""
    @objc dynamic var filePathOffline: String = ""
    @objc dynamic var fileType: Int = 0
    @objc dynamic var fileExtension: String = ""
    @objc dynamic var thumbnailUrl: String = ""
    @objc dynamic var hasShared: Bool = false
    @objc dynamic var modifiedAt: Int64 = 0

}
