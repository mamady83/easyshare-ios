//
//  HomeTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class HomeTable: Object {
    @objc dynamic var type: Int = 0
    @objc dynamic var file: FileTable?
    @objc dynamic var folder: FolderTable?
}
