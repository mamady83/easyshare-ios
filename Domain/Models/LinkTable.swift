//
//  LinkTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class LinkTable: Object {
    @objc dynamic var linkId: Int = 0
    @objc dynamic var folderId: Int = 0
    @objc dynamic var fileId: Int = 0
    @objc dynamic var url: String = ""
    @objc dynamic var expires_at: Int64 = 0
    @objc dynamic var isPasswordProtected: Bool = false

}
