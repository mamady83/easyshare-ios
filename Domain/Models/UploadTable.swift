//
//  UploadTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class UploadTable: Object {
    @objc dynamic var uploadId: Int = 0
    @objc dynamic var percent: Int = 0
    @objc dynamic var folderId: Int = 0
    @objc dynamic var status: Int = 0
    @objc dynamic var uploadKey: String = ""
    @objc dynamic var filename: String = ""
    @objc dynamic var filepath: String = ""
    @objc dynamic var fileExtension: String = ""
    @objc dynamic var filesize: Int64 = 0
    @objc dynamic var filetype: Int = 0
}
