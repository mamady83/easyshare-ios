//
//  StorageTable.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import RealmSwift

class StorageTable: Object {
    @objc dynamic var size: Int64 = 0
    @objc dynamic var type: Int = 0
}
