//
//  LocalStorage.swift
//  Core
//
//  Created by mohamd yeganeh on 7/29/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation


public class LocalStorage {
    
    private static var sharedUserDefaults: UserDefaults = {
        return UserDefaults(suiteName: "group.sweetshare.app")!
    }()
    public static var myUserDefaults : UserDefaults {
        return sharedUserDefaults
    }
    
    
    public func removeAllData(){
        LocalStorage.myUserDefaults.removePersistentDomain(forName: "group.sweetshare.app")
        LocalStorage.myUserDefaults.synchronize()
    }
    private static var sharedLocalStorage: LocalStorage = {
        return LocalStorage()
    }()
    
    
    public class func shared() -> LocalStorage {
        return sharedLocalStorage
    }
    
    // MARK: - LOGGED IN
    public var isLoggedIn:Bool {
        get {
            return LocalStorage.myUserDefaults.bool(forKey: StorageKeys.LOGGED_IN)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.LOGGED_IN)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    
    // MARK: - IS VIP
    public var isVIP:Bool {
        get {
            return LocalStorage.myUserDefaults.bool(forKey: StorageKeys.IS_VIP)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.IS_VIP)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - PASSED ONBOARDING
    public var passedOnboarding:Bool {
        get {
            return LocalStorage.myUserDefaults.bool(forKey: StorageKeys.PASSED_ONBOARDING)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.PASSED_ONBOARDING)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - TOKEN
    public var token:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.TOKEN) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.TOKEN)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - PHONE
    public var phone:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.PHONE) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.PHONE)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - AVATAR
    public var avatar:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.AVATAR) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.AVATAR)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - CLIENT ID
    public var clientID:Int {
        get {
            return LocalStorage.myUserDefaults.integer(forKey: StorageKeys.CLIENT_ID)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.CLIENT_ID)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - FIRST NAME
    public var firstName:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.FIRST_NAME) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.FIRST_NAME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - LAST NAME
    public var lastName:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.LAST_NAME) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.LAST_NAME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - EMAIL
    public var email:String {
        get {
            return LocalStorage.myUserDefaults.string(forKey: StorageKeys.EMAIL) ?? ""
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.EMAIL)
            LocalStorage.myUserDefaults.synchronize()
            
        }
    }
    
    // MARK: - PACKAGE ID
    public var packageID:Int {
        get {
            return LocalStorage.myUserDefaults.integer(forKey: StorageKeys.PACKAGE_ID)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.PACKAGE_ID)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - PACKAGE START DATE
    public var packageStartDate:Double {
        get {
            return LocalStorage.myUserDefaults.double(forKey: StorageKeys.PACKAGE_START_DATE)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.PACKAGE_START_DATE)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - PACKAGE END DATE
    public var packageEndDate:Double {
        get {
            return LocalStorage.myUserDefaults.double(forKey: StorageKeys.PACKAGE_END_DATE)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.PACKAGE_END_DATE)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - ACTIVE UPLOAD ID
    public var activeUploadId:Int {
        get {
            return LocalStorage.myUserDefaults.integer(forKey: StorageKeys.ACTIVE_UPLOAD_ID)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.ACTIVE_UPLOAD_ID)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    // MARK: - STORAGE USED
    public var storageUsed:Double {
        get {
            return LocalStorage.myUserDefaults.double(forKey: StorageKeys.STORAGE_USED)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.STORAGE_USED)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - TOTAL STORAGE
    public var totalStorage:Double {
        get {
            return LocalStorage.myUserDefaults.double(forKey: StorageKeys.STORAGE_TOTAL)
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.STORAGE_TOTAL)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - HOME FIRST TIME
    public var homeFirstTime:Bool {
        get {
            return LocalStorage.myUserDefaults.optionalBool(forKey: StorageKeys.HOME_FIRST_TIME) ?? true
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.HOME_FIRST_TIME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - FILES FIRST TIME
    public var filesFirstTime:Bool {
        get {
            return LocalStorage.myUserDefaults.optionalBool(forKey: StorageKeys.FILES_FIRST_TIME) ?? true
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.FILES_FIRST_TIME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    
    // MARK: - LINKS FIRST TIME
    public var linksFirstTime:Bool {
        get {
            return LocalStorage.myUserDefaults.optionalBool(forKey: StorageKeys.LINKS_FIRST_TIME) ?? true
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.LINKS_FIRST_TIME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
    // MARK: - FILES SHEET FIRST TIME
    public var filesSheetFirstTime:Bool {
        get {
            return LocalStorage.myUserDefaults.optionalBool(forKey: StorageKeys.FILE_SHEET_FIRST_TIME) ?? true
        }
        set {
            LocalStorage.myUserDefaults.set(newValue, forKey: StorageKeys.FILE_SHEET_FIRST_TIME)
            LocalStorage.myUserDefaults.synchronize()
        }
    }
}

extension UserDefaults{
    public func optionalBool(forKey defaultName: String) -> Bool? {
        let defaults = self
        if let value = defaults.value(forKey: defaultName) {
            return value as? Bool
        }
        return nil
    }
}
