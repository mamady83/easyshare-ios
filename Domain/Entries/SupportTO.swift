//
//  SupportTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct SupportTO: Codable {
    
    public let isAdmin: Bool
    public let date: String
    public let message: String

    
    public init(message: String,
                isAdmin: Bool,
                date: String) {
        
        self.message = message
        self.isAdmin = isAdmin
        self.date = date
        
    }
}

extension SupportTO: Equatable {
    public static func == (lhs: SupportTO, rhs: SupportTO) -> Bool {
        return lhs.message == rhs.message &&
            lhs.date == rhs.date
    }
}
