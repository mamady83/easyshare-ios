//
//  DownloadTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct DownloadTO: Codable {
    
    public let downloadId: Int
    public let percent: Int
    public let fileId: Int
    public let status: Int
    
    public init(downloadId: Int,
                percent: Int,
                fileId: Int,
                status: Int) {
        
        self.downloadId = downloadId
        self.percent = percent
        self.status = status
        self.fileId = fileId
        
    }
}

extension DownloadTO: Equatable {
    public static func == (lhs: DownloadTO, rhs: DownloadTO) -> Bool {
        return lhs.downloadId == rhs.downloadId &&
            lhs.percent == rhs.percent &&
            lhs.status == rhs.status &&
            lhs.fileId == rhs.fileId
    }
}
