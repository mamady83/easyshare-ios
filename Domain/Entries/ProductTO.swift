//
//  ProductTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/30/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct ProductTO: Codable {
    
    public let productId: Int
    public let name: String
    public let picture: String
    public let apple_product_id: String
    public let duration: Int
    public let storage: Int
    public let files_count: Int
    public let info: String
    public let price: Float
    
    
    public init(
        productId: Int,
        name: String,
        picture: String,
        apple_product_id: String,
        duration: Int,
        storage: Int,
        files_count: Int,
        info: String,
        price: Float
    ) {
        self.productId = productId
        self.name = name
        self.picture = picture
        self.apple_product_id = apple_product_id
        self.duration = duration
        self.storage = storage
        self.files_count = files_count
        self.info = info
        self.price = price
    }
}

extension ProductTO: Equatable {
    public static func == (lhs: ProductTO, rhs: ProductTO) -> Bool {
        return lhs.productId == rhs.productId &&
            lhs.apple_product_id == rhs.apple_product_id
    }
}
