//
//  OnboardingTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct OnboardingTO: Codable {

    public let title: String
    public let subtitle: String

    public init(title: String,
                subtitle: String) {
        self.title = title
        self.subtitle = subtitle
    }
}

extension OnboardingTO: Equatable {
    public static func == (lhs: OnboardingTO, rhs: OnboardingTO) -> Bool {
        return lhs.subtitle == rhs.subtitle &&
            lhs.title == rhs.title
    }
}
