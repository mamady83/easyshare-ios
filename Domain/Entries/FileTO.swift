//
//  FileTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct FileTO: Codable {
    
    public let fileId: Int
    public let folderId: Int
    public let fileName : String
    public let fileSize: Double
    public let filePathOnline: String
    public var filePathOffline: String
    public let fileType: Int
    public let fileExtension: String
    public let thumbnailUrl: String
    public let hasShared: Bool
    public let modifiedAt: Int64
    
    public init(fileId: Int,
                folderId: Int,
                fileName : String,
                fileSize: Double,
                filePathOnline: String,
                filePathOffline: String,
                fileType: Int,
                thumbnailUrl: String,
                fileExtension: String,
                hasShared: Bool,
                modifiedAt: Int64) {
        
        self.fileId = fileId
        self.folderId = folderId
        self.fileName = fileName
        self.fileSize = fileSize
        self.filePathOnline = filePathOnline
        self.filePathOffline = filePathOffline
        self.fileType = fileType
        self.fileExtension = fileExtension
        self.thumbnailUrl = thumbnailUrl
        self.hasShared = hasShared
        self.modifiedAt = modifiedAt

    }
    
}
extension FileTO: Equatable {
    public static func == (lhs: FileTO, rhs: FileTO) -> Bool {
        return lhs.fileId == rhs.fileId &&
            lhs.folderId == rhs.folderId &&
            lhs.fileName == rhs.fileName &&
            lhs.fileSize == rhs.fileSize &&
            lhs.filePathOnline == rhs.filePathOnline &&
            lhs.filePathOffline == rhs.filePathOffline &&
            lhs.fileType == rhs.fileType &&
            lhs.fileExtension == rhs.fileExtension &&
            lhs.thumbnailUrl == rhs.thumbnailUrl
    }
}
