//
//  Home.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct HomeTO{
    
    public let type: Int
    public let item: Any?
    
    public init(type: Int, item: Any?) {
        self.type = type
        self.item = item
    }
    public init(type: Int) {
        self.type = type
        self.item = nil
    }
}
