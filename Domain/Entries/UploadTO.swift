//
//  UploadTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct UploadTO: Codable {
    
    public let uploadId: Int
    public let percent: Int
    public let folderId: Int
    public let status: Int
    public let uploadKey: String
    public let filename: String
    public let filepath: String
    public let fileExtension: String
    public let filesize: Int64
    public let filetype: Int
    
    public init(uploadId: Int,
                percent: Int,
                status: Int,
                uploadKey: String,
                folderId: Int,
                filename: String,
                filepath: String,
                fileExtension: String,
                filesize: Int64,
                filetype: Int) {
        
        self.uploadId = uploadId
        self.percent = percent
        self.status = status
        self.uploadKey = uploadKey
        self.folderId = folderId
        self.filename = filename
        self.filepath = filepath
        self.filesize = filesize
        self.fileExtension = fileExtension
        self.filetype = filetype
        
    }
}

extension UploadTO: Equatable {
    public static func == (lhs: UploadTO, rhs: UploadTO) -> Bool {
        return lhs.uploadId == rhs.uploadId &&
            lhs.percent == rhs.percent &&
            lhs.status == rhs.status &&
            lhs.folderId == rhs.folderId &&
            lhs.uploadKey == rhs.uploadKey &&
            lhs.filename == rhs.filename &&
            lhs.filepath == rhs.filepath &&
            lhs.filesize == rhs.filesize &&
            lhs.filetype == rhs.filetype
    }
}
