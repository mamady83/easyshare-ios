//
//  LinkTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct LinkTO: Codable {
    
    public let linkId: Int
    public let folderId: Int
    public var fileId: Int
    public let url: String
    public let expires_at: Int64
    public let isPasswordProtected: Bool

    
    public init(linkId: Int,
                folderId: Int,
                fileId: Int,
                url: String,
                expires_at: Int64,
                isPasswordProtected: Bool) {
        
        self.linkId = linkId
        self.folderId = folderId
        self.fileId = fileId
        self.url = url
        self.expires_at = expires_at
        self.isPasswordProtected = isPasswordProtected
    }
}

extension LinkTO: Equatable {
    public static func == (lhs: LinkTO, rhs: LinkTO) -> Bool {
        return lhs.linkId == rhs.linkId &&
            lhs.url == rhs.url
    }
}
