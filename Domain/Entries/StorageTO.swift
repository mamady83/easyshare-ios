//
//  StorageTO.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit

public struct StorageTO {

    public let title : String
    public let color: UIColor
    public let space: String
    public let value: Double
    public let type: Int
    public let size: Int64

    public init(title: String,
                color: UIColor,
                space: String,
                value: Double,
                type: Int,
                size: Int64) {
        self.title = title
        self.color = color
        self.space = space
        self.value = value
        self.type = type
        self.size = size

    }
}

extension StorageTO: Equatable {
    public static func == (lhs: StorageTO, rhs: StorageTO) -> Bool {
        return lhs.title == rhs.title &&
            lhs.color == rhs.color &&
            lhs.space == rhs.space &&
            lhs.value == rhs.value &&
            lhs.type == rhs.type
    }
}
