//
//  MenuTabs.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


public struct MenuTab {
    public static let RECENT_FILES = 1
    public static let SHARED_FILES = 2
}
