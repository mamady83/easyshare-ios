//
//  HomeCellType.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

public struct HomeCellType {
    
    public static let MENU = 1
    public static let UPLOAD = 2
    public static let HEADER = 3
    public static let FILE = 4
    public static let FOLDER = 5
    
}
