//
//  FileType.swift
//  Core
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

public struct FileType {
    public static let VIDEO = 1
    public static let PHOTO = 2
    public static let DOCUMENT = 4
    public static let MUSIC = 3
}
