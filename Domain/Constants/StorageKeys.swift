//
//  Keys.swift
//  Core
//
//  Created by mohamd yeganeh on 7/29/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

struct StorageKeys {
    static let LOGGED_IN = "LOGGED_IN"
    static let PASSED_ONBOARDING = "PASSED_ONBOARDING"

    static let TOKEN = "TOKEN"
    static let CLIENT_ID = "CLIENT_ID"
    static let FIRST_NAME = "FIRST_NAME"
    static let LAST_NAME = "LAST_NAME"
    static let EMAIL = "EMAIL"
    static let PHONE = "PHONE"
    static let AVATAR = "AVATAR"

    static let PACKAGE_ID = "PACKAGE_ID"
    static let PACKAGE_START_DATE = "PACKAGE_START_DATE"
    static let PACKAGE_END_DATE = "PACKAGE_END_DATE"
    static let IS_VIP = "IS_VIP"

    static let ACTIVE_UPLOAD_ID = "ACTIVE_UPLOAD_ID"

    static let STORAGE_USED = "STORAGE_USED"
    static let STORAGE_TOTAL = "STORAGE_TOTAL"

    static let HOME_FIRST_TIME = "HOME_FIRST_TIME"
    static let FILES_FIRST_TIME = "FILES_FIRST_TIME"
    static let LINKS_FIRST_TIME = "LINKS_FIRST_TIME"
    static let FILE_SHEET_FIRST_TIME = "FILE_SHEET_FIRST_TIME"

}
