//
//  DonwloadStatuss.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

public struct DownloadStatus {
    public static let DOWNLOADING = 2
    public static let FAILED = 3
    public static let COMPLETED = 4
}
