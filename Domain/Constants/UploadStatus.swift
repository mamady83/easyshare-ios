//
//  UploadStatus.swift
//  Core
//
//  Created by mohamd yeganeh on 7/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

public struct UploadStatus {
    public static let UPLOADING = 2
    public static let FAILED = 3
    public static let COMPLETED = 4
}
