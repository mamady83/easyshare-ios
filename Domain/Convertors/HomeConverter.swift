//
//  ConvertHome.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertHome(db: HomeTable) -> HomeTO{
    var item : Any? = nil
    if db.type == HomeCellType.FILE, let file = db.file {
        item = convertFile(db: file)
    }else if db.type == HomeCellType.FOLDER, let folder = db.folder {
        item = convertFolder(db: folder)
    }
    return HomeTO(
        type: db.type,
        item: item
    )
}

func convertHome(to: HomeTO) -> HomeTable{
    let db = HomeTable()
    db.type = to.type
    if db.type == HomeCellType.FILE, let file = to.item as? FileTO {
        db.file = convertFile(to: file)
    }else if db.type == HomeCellType.FOLDER, let folder = to.item as? FolderTO {
        db.folder = convertFolder(to: folder)
    }
    return db
}

func convertHomes(dbItms: [HomeTable]) -> [HomeTO]{
    var items: [HomeTO] = Array()
    for item in dbItms {
        items.append(convertHome(db: item))
    }
    return items
}
