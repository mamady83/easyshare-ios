//
//  FolderConvertor.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertFolder(db: FolderTable) -> FolderTO{
    return FolderTO(
        folderName: db.folderName,
        folderId: db.folderId,
        filesCount: db.filesCount,
        hasShared: db.hasShared,
        modifiedAt: db.modifiedAt
    )
    
}

func convertFolder(to: FolderTO) -> FolderTable{
    let db = FolderTable()
    db.folderId = to.folderId
    db.folderName = to.folderName
    db.filesCount = to.filesCount
    db.modifiedAt = to.modifiedAt
    db.hasShared = to.hasShared
    return db
}

func convertFolders(dbItms: [FolderTable]) -> [FolderTO]{
    var items: [FolderTO] = Array()
    for item in dbItms {
        items.append(convertFolder(db: item))
    }
    return items
}
