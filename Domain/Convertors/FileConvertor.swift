//
//  FileConvertr.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertFile(db: FileTable) -> FileTO{
    return FileTO(
        fileId: db.fileId,
        folderId: db.folderId,
        fileName: db.fileName,
        fileSize: db.fileSize,
        filePathOnline: db.filePathOnline,
        filePathOffline: db.filePathOffline,
        fileType: db.fileType,
        thumbnailUrl: db.thumbnailUrl,
        fileExtension: db.fileExtension,
        hasShared: db.hasShared,
        modifiedAt: db.modifiedAt
    )
    
}
func convertFile(to: FileTO) -> FileTable{
    let db = FileTable()
    db.fileId = to.fileId
    db.folderId = to.folderId
    db.fileName = to.fileName
    db.fileSize = to.fileSize
    db.filePathOnline = to.filePathOnline
    db.filePathOffline = to.filePathOffline
    db.fileType = to.fileType
    db.thumbnailUrl = to.thumbnailUrl
    db.fileExtension = to.fileExtension
    db.modifiedAt = to.modifiedAt
    db.hasShared = to.hasShared
    return db
}
func convertFiles(dbItms: [FileTable]) -> [FileTO]{
    var items: [FileTO] = Array()
    for item in dbItms {
        items.append(convertFile(db: item))
    }
    return items
}
