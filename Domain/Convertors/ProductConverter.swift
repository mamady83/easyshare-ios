//
//  ConverProduct.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/30/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertProduct(db: ProductsTable) -> ProductTO{
    return ProductTO(
        productId: db.productId,
        name: db.name,
        picture: db.picture,
        apple_product_id: db.apple_product_id,
        duration: db.duration,
        storage: db.storage,
        files_count: db.files_count,
        info: db.info,
        price: db.price
    )
}
func convertProduct(to: ProductTO) -> ProductsTable{
    let db = ProductsTable()
    db.productId = to.productId
    db.name = to.name
    db.picture = to.picture
    db.apple_product_id = to.apple_product_id
    db.duration = to.duration
    db.storage = to.storage
    db.files_count = to.files_count
    db.info = to.info
    db.price = to.price
    return db
}
func convertProducts(dbItms: [ProductsTable]) -> [ProductTO]{
    var items: [ProductTO] = Array()
    for item in dbItms {
        items.append(convertProduct(db: item))
    }
    return items
}
