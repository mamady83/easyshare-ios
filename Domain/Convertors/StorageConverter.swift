//
//  StorageConverter.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import Foundation
import UIKit


func convertStorage(db: StorageTable) -> StorageTO{
    var title = ""
    var color = UIColor(named: "colorBlue")!
    let total = LocalStorage.shared().totalStorage
    let space = Units(bytes: db.size).getReadableUnit()
    
    
    switch db.type {
    case FileType.PHOTO:
        title = "Photo"
        color = UIColor(named: "colorImage")!
        break
    case FileType.MUSIC:
        title = "Music"
        color = UIColor(named: "colorMusic")!
        break
    case FileType.VIDEO:
        title = "Video"
        color = UIColor(named: "colorVideo")!
        break
    default:
        title = "Document"
        color = UIColor(named: "colorFile")!
    }
    return StorageTO(
        title: title,
        color: color,
        space: space,
        value: Double(db.size)/total,
        type: db.type,
        size: db.size
    )
}
func convertStorage(to: StorageTO) -> StorageTable{
    let db = StorageTable()
    db.type = to.type
    db.size = to.size
    return db
}
func convertStorages(dbItms: [StorageTable]) -> [StorageTO]{
    var items: [StorageTO] = Array()
    for item in dbItms {
        items.append(convertStorage(db: item))
    }
    return items
}
