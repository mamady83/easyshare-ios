//
//  UploadConvertor.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertUpload(db: UploadTable) -> UploadTO{
    return UploadTO(
        uploadId: db.uploadId,
        percent: db.percent,
        status: db.status,
        uploadKey: db.uploadKey,
        folderId: db.folderId,
        filename: db.filename,
        filepath: db.filepath,
        fileExtension: db.fileExtension,
        filesize: db.filesize,
        filetype: db.filetype
    )
}
func convertUpload(to: UploadTO) -> UploadTable{
    let db = UploadTable()
    db.uploadId = to.uploadId
    db.percent = to.percent
    db.status = to.status
    db.uploadKey = to.uploadKey
    db.folderId = to.folderId
    db.filename = to.filename
    db.filepath = to.filepath
    db.filesize = to.filesize
    db.fileExtension = to.fileExtension
    db.filetype = to.filetype
    return db
}
func convertUploads(dbItms: [UploadTable]) -> [UploadTO]{
    var items: [UploadTO] = Array()
    for item in dbItms {
        items.append(convertUpload(db: item))
    }
    return items
}
