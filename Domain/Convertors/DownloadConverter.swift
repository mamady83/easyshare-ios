//
//  DownloadConverter.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertDownload(db: DownloadTable) -> DownloadTO{
    return DownloadTO(
        downloadId: db.downloadId, percent: db.percent, fileId: db.fileId, status: db.status
    )
}
func convertDownload(to: DownloadTO) -> DownloadTable{
    let db = DownloadTable()
    db.downloadId = to.downloadId
    db.percent = to.percent
    db.status = to.status
    db.fileId = to.fileId
    return db
}
func convertDownloads(dbItms: [DownloadTable]) -> [DownloadTO]{
    var items: [DownloadTO] = Array()
    for item in dbItms {
        items.append(convertDownload(db: item))
    }
    return items
}
