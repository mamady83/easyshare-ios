//
//  LinkConverter.swift
//  Domain
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation

func convertLink(db: LinkTable) -> LinkTO{
    return LinkTO(
        linkId: db.linkId,
        folderId: db.folderId,
        fileId: db.fileId,
        url: db.url,
        expires_at: db.expires_at,
        isPasswordProtected: db.isPasswordProtected
    )
}
func convertLink(to: LinkTO) -> LinkTable{
    let db = LinkTable()
    db.linkId = to.linkId
    db.folderId = to.folderId
    db.fileId = to.fileId
    db.url = to.url
    db.expires_at = to.expires_at
    db.isPasswordProtected = to.isPasswordProtected
    return db
}
func convertLinks(dbItms: [LinkTable]) -> [LinkTO]{
    var items: [LinkTO] = Array()
    for item in dbItms {
        items.append(convertLink(db: item))
    }
    return items
}
