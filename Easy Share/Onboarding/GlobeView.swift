//
//  GlobeView.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/22/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class GlobeView : UIView {
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        backgroundColor = UIColor.clear
        isUserInteractionEnabled = false
        
        addSubview(cloud_bg)
        addSubview(globe)
        addSubview(globe_left)
        addSubview(globe_top)
        addSubview(globe_right)
        addSubview(globe_center_left)
        addSubview(globe_center_right)
        addSubview(cloud_left)
        addSubview(folder_back)
        addSubview(file)
        addSubview(image_back)
        addSubview(image)
        addSubview(folder_front)
        addSubview(cloud_right)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    private let globe: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe")
        return image
    }()
    
    
    private let globe_top: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe_top")
        return image
    }()
    
    
    private let globe_left: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe_left")
        return image
    }()
    
    
    private let globe_right: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe_right")
        return image
    }()
    
    private let globe_center_left: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe_center")
        return image
    }()
    
    private let globe_center_right: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_globe_center")
        return image
    }()
    
    private let cloud_bg: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_cloud_one")
        return image
    }()

    private let cloud_left: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_cloud_one")
        return image
    }()
    
    private let cloud_right: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_cloud_two")
        return image
    }()
    
    private let folder_front: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_folder")
        return image
    }()
    
    private let folder_back: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_folder_back")
        return image
    }()
    
    private let file: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_file")
        return image
    }()
    
    private let image_back: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_image_two")
        return image
    }()
    
    private let image: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_image")
        return image
    }()
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let centerY = frame.size.height/2
        let centerX = frame.size.width/2
        
        
        globe.frame = CGRect(
            x: centerX - 56,
            y: centerY - 100,
            width: 198,
            height: 200
        )
        
        
        globe_top.frame = CGRect(
            x: centerX + 22,
            y: centerY - 100,
            width: 112,
            height: 91
        )
        
        globe_right.frame = CGRect(
            x: centerX + 86,
            y: centerY + 25,
            width: 54,
            height: 49
        )
        
        globe_left.frame = CGRect(
            x: centerX - 54,
            y: centerY - 16,
            width: 114,
            height: 103
        )
        
        globe_center_left.frame = CGRect(
            x: centerX - 39,
            y: centerY - 48,
            width: 25,
            height: 25
        )
        
        globe_center_right.frame = CGRect(
            x: centerX + 58,
            y: centerY - 28,
            width: 30,
            height: 30
        )
        
        cloud_bg.frame = CGRect(
            x: centerX + 55,
            y: centerY - 121,
            width: 98,
            height: 39
        )
        
        cloud_left.frame = CGRect(
            x: centerX - 155,
            y: centerY + 19,
            width: 168,
            height: 67
        )
        
        cloud_right.frame = CGRect(
            x: centerX - 111,
            y: centerY + 42,
            width: 199,
            height: 79
        )
        
        folder_front.frame = CGRect(
            x: centerX - 116,
            y: centerY - 12,
            width: 134,
            height: 118
        )
        
        folder_back.frame = CGRect(
            x: centerX - 100,
            y: centerY - 6,
            width: 130,
            height: 89
        )
        
        file.frame = CGRect(
            x: centerX - 131,
            y: centerY - 72,
            width: 102,
            height: 113
        )
        
        image.frame = CGRect(
            x: centerX - 92,
            y: centerY - 76,
            width: 106,
            height: 125
        )
        
        image_back.frame = CGRect(
            x: centerX - 82,
            y: centerY - 53,
            width: 106,
            height: 125
        )
    }
    
    
    func animateElements(){
        let offscreenAmount = frame.width - frame.width/3.4
        // MARK: - cloud right
        let cloudRightOrigin = cloud_right.transform
        cloud_right.transform = cloudRightOrigin.translatedBy(x: -offscreenAmount, y: 0)

        UIView.animate(withDuration: 0.7, delay: 00, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.cloud_right.transform = cloudRightOrigin
        })
        
        // MARK: - cloud left
        let cloudLeftOrigin = cloud_left.transform
        cloud_left.transform = cloudLeftOrigin.translatedBy(x: -offscreenAmount, y: 0)
        
        UIView.animate(withDuration: 1.0, delay: 0.4, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.cloud_left.transform = cloudLeftOrigin
        })
        
        // MARK: - cloud bg
        let cloudBGOrigin = cloud_bg.transform
        cloud_bg.transform = cloudBGOrigin.translatedBy(x: offscreenAmount, y: 0)
        
        UIView.animate(withDuration: 1.0, delay: 0.6, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.cloud_bg.transform = cloudLeftOrigin
        })
        
        // MARK: - globe top
        let globeTopOrigin = globe_top.transform
        globe_top.transform = globeTopOrigin.scaledBy(x: 0, y: 0)
        
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.globe_top.transform = globeTopOrigin
        })
        
        // MARK: - globe right
        let globeRightOrigin = globe_right.transform
        globe_right.transform = globeRightOrigin.scaledBy(x: 0, y: 0)
        
        UIView.animate(withDuration: 1.2, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.globe_right.transform = globeRightOrigin
        })
        
        // MARK: - globe left
        let globeLeftOrigin = globe_left.transform
        globe_left.transform = globeLeftOrigin.scaledBy(x: 0, y: 0)
        
        UIView.animate(withDuration: 0.7, delay: 0.2, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.globe_left.transform = globeLeftOrigin
        })
        
        // MARK: - globe center left
        let globeCenterLeftOrigin = globe_center_left.transform
        globe_center_left.transform = globeCenterLeftOrigin.scaledBy(x: 0, y: 0)
        
        UIView.animate(withDuration: 1.1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.globe_center_left.transform = globeCenterLeftOrigin
        })
        // MARK: - globe center right
        let globeCenterRightOrigin = globe_center_right.transform
        globe_center_right.transform = globeCenterRightOrigin.scaledBy(x: 0, y: 0)
        
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.globe_center_right.transform = globeCenterRightOrigin
        })
        // MARK: - folder front
        let folderFrontOrigin = folder_front.transform
        folder_front.transform = folderFrontOrigin.translatedBy(x: 0, y: 300)
        
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.folder_front.transform = folderFrontOrigin
        })
        // MARK: - folder back
        let folderBackOrigin = folder_back.transform
        folder_back.transform = folderBackOrigin.translatedBy(x: 0, y: 300)
        
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.folder_back.transform = folderBackOrigin
        })
        // MARK: - file
        let fileOrigin = file.transform
        file.transform = fileOrigin.translatedBy(x: 0, y: 300)
        
        UIView.animate(withDuration: 0.8, delay: 0.5, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.file.transform = fileOrigin
        })
        // MARK: - image
        let imageOrigin = image.transform
        image.transform = imageOrigin.translatedBy(x: 0, y: 300)
        
        UIView.animate(withDuration: 0.8, delay: 0.7, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.image.transform = imageOrigin
        })
        // MARK: - image
        let imageBackOrigin = image_back.transform
        image_back.transform = imageBackOrigin.translatedBy(x: 0, y: 300)
        
        UIView.animate(withDuration: 0.8, delay: 0.6, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.image_back.transform = imageBackOrigin
        })
    }
}
