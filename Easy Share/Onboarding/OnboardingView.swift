//
//  StepOneView.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/22/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import TweenKit

public class OnboardingView: UIView {
    
    // MARK: - Internal
    
    func updateStepOne(t: Double) {
        stepOneScrubber.update(t: t)
    }
    func updateStepTwo(t: Double) {
        stepTwoScrubber.update(t: t)
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        
        backgroundColor = UIColor.clear
        isUserInteractionEnabled = false
        
        // Add subviews
        addSubview(file)
        addSubview(file2)
        addSubview(file3)
        addSubview(folder)
        addSubview(image)
        addSubview(image2)
        addSubview(image3)
        addSubview(folderOverlay)
        addSubview(cloud)
        addSubview(cloud2)
        addSubview(calendar_icon)
        addSubview(security_icon)
        addSubview(cloud_icon)
        
    }
    
    // MARK: - Properties
    
    private var fileScaleFactor = CGFloat(1)
    private var imageScaleFactor = CGFloat(1)
    private var fileMoveRightAmount = CGFloat(0)
    private var imagesBottomInset = CGFloat(0)
    
    private var filesOffset = CGFloat(0)
    private var folderOffset = CGFloat(0)
    private var stepOneScrubber: ActionScrubber!
    private var stepTwoScrubber: ActionScrubber!
    
    private var cloudOffset = CGFloat(-1)
    
    private let cloud_icon: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_icon_security")
        return image
    }()
    private let security_icon: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_icon_calendar")
        return image
    }()
    private let calendar_icon: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_icon_cloud")
        return image
    }()
    
    private let folder: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_folder")
        return image
    }()
    
    private let folderOverlay: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.alpha = 0
        image.image = UIImage(named: "o_folder")
        return image
    }()
    
    private let image: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_image")
        return image
    }()
    private let image2: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_image")
        return image
    }()
    private let image3: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_image")
        return image
    }()
    
    private let file: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_file")
        return image
    }()
    
    
    private let file2: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_file")
        return image
    }()
    
    
    private let file3: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_file")
        return image
    }()
    
    
    private let cloud: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_cloud_one")
        return image
    }()
    private let cloud2: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "o_cloud_two")
        return image
    }()
    
    
    // MARK: - UIView
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initStepOneAction(){
        let stepOneIconAction = InterpolationAction(
            from: CGFloat(1.0),
            to: CGFloat(0.0),
            duration: 1.0,
            easing: .linear) {
                [unowned self] in
                self.cloud_icon.alpha = $0
                
        }
        let stepOnMoveAction = InterpolationAction(
            from: filesOffset,
            to: 12,
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.filesOffset = $0
                self.setNeedsLayout()
        })
        let stepOnRotateRightAction = InterpolationAction(
            from: CGFloat(0),
            to: CGFloat(6.0),
            duration: 0.5,
            easing: .linear, update: { [unowned self] in
                self.folderOffset = $0
                self.setNeedsLayout()
        })
        let stepOnRotateLeftAction = InterpolationAction(
            from: CGFloat(6.0),
            to: CGFloat(0),
            duration: 0.5,
            easing: .linear, update: { [unowned self] in
                self.folderOffset = $0
                self.setNeedsLayout()
        })
        
        
        let stepOneGroup = ActionGroup(actions: stepOneIconAction, stepOnMoveAction, ActionSequence(actions: stepOnRotateRightAction, stepOnRotateLeftAction))
        stepOneScrubber = ActionScrubber(action: stepOneGroup)
    }
    
    private func initStepTwoScrubber(){
        let stepTwoIconAction = InterpolationAction(
            from: CGFloat(1.0),
            to: CGFloat(0.0),
            duration: 1.0,
            easing: .linear) {
                [unowned self] in
                self.security_icon.alpha = $0
                
        }
        
        let stepTwoMoveAction = InterpolationAction(
            from: cloudOffset,
            to: 0,
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.cloudOffset = $0
                self.setNeedsLayout()
        })
        
        
        let stepTwoFolderMoveAction = InterpolationAction(
            from: folderOffset,
            to: 10,
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.folderOffset = $0
                self.setNeedsLayout()
        })
        

        let stepTwoFileOffsetAction = InterpolationAction(
            from: CGFloat(12),
            to: 0,
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.filesOffset = $0
                self.setNeedsLayout()
        })
        
        let stepTwoFilesMoveRight = InterpolationAction(
            from: CGFloat(0),
            to: CGFloat(12),
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.fileMoveRightAmount = $0
                self.setNeedsLayout()
        })
        
        
        
        let stepTwoFilesScale = InterpolationAction(
            from: CGFloat(1),
            to: CGFloat(1.2),
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.fileScaleFactor = $0
                self.setNeedsLayout()
        })
        
        
        
        let stepTwoImagesScale = InterpolationAction(
            from: CGFloat(1),
            to: CGFloat(0.7),
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.imageScaleFactor = $0
                self.setNeedsLayout()
        })
        
        
        let stepTwoImagesMoveUp = InterpolationAction(
            from: CGFloat(0),
            to: CGFloat(20),
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.imagesBottomInset = $0
                self.setNeedsLayout()
        })
        
        let stepTwoFolderOverlay = InterpolationAction(
            from: CGFloat(0),
            to: CGFloat(1),
            duration: 1.0,
            easing: .backOut, update: { [unowned self] in
                self.folderOverlay.alpha = $0
        })
        
        let stepTwoGroup = ActionGroup(
            actions: stepTwoIconAction,
            stepTwoMoveAction,
            stepTwoFolderMoveAction,
            stepTwoFileOffsetAction,
            stepTwoFilesMoveRight,
            stepTwoFilesScale,
            stepTwoImagesScale,
            stepTwoImagesMoveUp,
            stepTwoFolderOverlay
        )
        
        stepTwoScrubber = ActionScrubber(action: stepTwoGroup)
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if cloudOffset == -1 {
            cloudOffset = self.frame.width - self.frame.width/3.3
            initStepOneAction()
            initStepTwoScrubber()
        }
        let bottomInset = CGFloat(50)
        let centerY = frame.size.height/2
        let centerX = frame.size.width/2
        
        
        file.frame = CGRect(x: centerX - 75 + fileMoveRightAmount,
                            y: centerY - bottomInset - 82,
                            width: 71 * fileScaleFactor,
                            height: 78 * fileScaleFactor)
        
        file2.frame = CGRect(x: centerX - 75 + filesOffset + fileMoveRightAmount,
                             y: centerY - bottomInset - 82 - (filesOffset * 0.8),
                             width: 71 * fileScaleFactor,
                             height: 78 * fileScaleFactor)
        
        file3.frame = CGRect(x: centerX - 75 + (filesOffset * 2) + fileMoveRightAmount,
                             y: centerY - bottomInset - 82 - (filesOffset * 1.6),
                             width: 71 * fileScaleFactor,
                             height: 78 * fileScaleFactor)
        
        folder.frame = CGRect(x: centerX - 66 + folderOffset,
                              y: centerY - bottomInset - 40,
                              width: 105,
                              height: 89)
        folderOverlay.frame = CGRect(x: centerX - 66 + folderOffset,
                                     y: centerY - bottomInset - 40,
                                     width: 105,
                                     height: 89)
        
        image.frame = CGRect(x: centerX - 20 + (filesOffset * 3.2),
                             y: centerY - bottomInset - 74 - (filesOffset * 0.6) - imagesBottomInset,
                             width: 89 * imageScaleFactor,
                             height: 106 * imageScaleFactor)
        
        image2.frame = CGRect(x: centerX - 20 + (filesOffset * 1.6),
                              y: centerY - bottomInset - 74 - (filesOffset * 0.3) - imagesBottomInset,
                              width: 89 * imageScaleFactor,
                              height: 106 * imageScaleFactor)
        
        image3.frame = CGRect(x: centerX - 20,
                              y: centerY - bottomInset - 74 - imagesBottomInset,
                              width: 89 * imageScaleFactor,
                              height: 106 * imageScaleFactor)
        
        
        cloud_icon.frame = CGRect(x: centerX + 8,
                                  y: centerY - bottomInset,
                                  width: 65,
                                  height: 65)
        
        
        security_icon.frame = CGRect(x: centerX + 8,
                                     y: centerY - bottomInset,
                                     width: 65,
                                     height: 65)
        
        calendar_icon.frame = CGRect(x: centerX + 8,
                                     y: centerY - bottomInset,
                                     width: 65,
                                     height: 65)
        
        cloud.frame = CGRect(x: centerX - 110 - cloudOffset,
                             y: centerY - bottomInset + 8,
                             width: 129,
                             height: 51)
        
        cloud2.frame = CGRect(x: centerX - 70 + cloudOffset,
                              y: centerY - bottomInset + 20,
                              width: 129,
                              height: 51)
        
    }
    
}

