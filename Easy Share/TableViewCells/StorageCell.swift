//
//  StorageCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class StorageCell : UITableViewCell {
    
    @IBOutlet weak var badge: UIImageView!
    @IBOutlet weak var title: RoundedLable!
    @IBOutlet weak var space: RoundedLable!

    func initWithItem(item: StorageTO) {
        title.text = item.title
        badge.backgroundColor = item.color
        space.text = "   \(item.space)   "
    }
}
