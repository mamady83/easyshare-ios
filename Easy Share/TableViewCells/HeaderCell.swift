//
//  HeaderCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLable.font = FontKit.roundedFont(ofSize: 10, weight: .bold)
        selectionStyle = .none
        backgroundColor = UIColor(named: "colorHeaderBackground")
    }
    
    func initWithItem(item: Any?) {
        if let title = item as? String {
            titleLable.text = title
        }
    }
}
