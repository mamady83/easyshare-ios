//
//  FolderHeader.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class FolderHeader: UITableViewHeaderFooterView {
    
    var onSortClicked: (()->())?
    @IBOutlet weak var folderNameLable: RoundedLable!
    @IBOutlet weak var sortLable: RoundedLable!
    
    @IBAction func sortBtnClicked(_ sender: Any) {
        onSortClicked?()
    }
    
}
