//
//  FolderCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class FolderCell : UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var subtitleLable: UILabel!
    @IBOutlet weak var ellipsisBtn: UIButton!
    
    public var onEllipsisClicked: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLable.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        subtitleLable.font = FontKit.roundedFont(ofSize: 10, weight: .semibold)
        selectionStyle = .none
        contentView.backgroundColor = UIColor(named: "colorUploadCellBG")
    }
    
    @IBAction func onEllipsisClicked(_ sender: Any) {
        onEllipsisClicked?()
    }
    
    func initWithItem(vc: UIViewController, item: Any?) {
        if let file = item as? FolderTO {
            titleLable.text = file.folderName
            subtitleLable.text = "\(file.filesCount) Files"
            onEllipsisClicked = {
                showFolderSheet(vc: vc, folder: file)
            }
        }
    }
}
