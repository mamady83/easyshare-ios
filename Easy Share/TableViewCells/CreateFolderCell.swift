//
//  CreateFolderCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/24/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class CreateFolderCell : UITableViewCell {
    
    @IBOutlet weak var createFolderBtn: RoundedButton!
}
