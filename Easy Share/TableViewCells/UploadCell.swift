//
//  UploadCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class UploadCell : UITableViewCell {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressPercentLable: UILabel!
    @IBOutlet weak var filesCountLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        filesCountLable.layer.borderColor = UIColor(named: "colorUploadBadgeBorder")?.cgColor
        selectionStyle = .none
        backgroundColor = UIColor(named: "colorUploadCellBG")
        clipsToBounds = false
        layer.masksToBounds = false
        layer.shadowColor = UIColor(named: "colorBlue")?.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 0.5
        layer.shadowOpacity = 0.25
        
    }
    
    func initWithItem(item: Any?, isSingle: Bool) {
        if let upload = item as? UploadTO {
            titleLable.text = upload.filename
            
            if isSingle {
                filesCountLable.isHidden = true
                arrow.isHidden = true
            }else {
                filesCountLable.text = "\(UploadDAO().notUploadedItems().count)"
            }
            
            progressView.setProgress(Float(upload.percent) / 100, animated: true)

            if upload.status == UploadStatus.UPLOADING {
                if upload.percent == 100 {
                    progressPercentLable.text = "Uploaded"
                    progressPercentLable.textColor = UIColor(named: "colorImage")
                    progressView.progressTintColor = UIColor(named: "colorImage")
                }else {
                    progressPercentLable.text = "\(upload.percent)%"
                    progressPercentLable.textColor = UIColor(named: "colorTextPrimary")
                    progressView.progressTintColor = UIColor(named: "colorBlue")
                }
            }else {
                progressPercentLable.text = "Failed"
                progressPercentLable.textColor = UIColor(named: "colorAccent")
                progressView.progressTintColor = UIColor(named: "colorAccent")
            }
        }
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *) {
            if (traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)) {
                filesCountLable.layer.borderColor = UIColor(named: "colorUploadBadgeBorder")?.cgColor
                filesCountLable.clipsToBounds = true
            }
        }
    }
}
