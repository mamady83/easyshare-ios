//
//  ChooseFolderCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/15/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import UIKit
import Domain

class ChooseFolderCell : UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var subtitleLable: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLable.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        subtitleLable.font = FontKit.roundedFont(ofSize: 10, weight: .semibold)
        selectionStyle = .none
        contentView.backgroundColor = UIColor(named: "colorUploadCellBG")
    }
    

    
    func initWithItem(vc: UIViewController, item: Any?) {
        if let file = item as? FolderTO {
            titleLable.text = file.folderName
            subtitleLable.text = "\(file.filesCount) Files"
            
        }
    }
}
