//
//  LinkCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/24/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class LinkCell:  UITableViewCell {
    
    @IBOutlet weak var titleLable: RoundedLable!
    @IBOutlet weak var subtitleLable: RoundedLable!
    var copyClicked: (()->())?
    
    func initWithItem(link: LinkTO) {
        selectionStyle = .none
        if link.expires_at == 0 {
            titleLable.text = "Expires at: Never"
        }else {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd MMM yyyy"
            let exp = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(link.expires_at)))
            titleLable.text = "Expires at: \(exp)"
        }
        if link.isPasswordProtected {
            subtitleLable.text = "Password Protected"
        }else {
            subtitleLable.text = "Public Link"
        }
    }
    
    @IBAction func copyBtnClicked(_ sender: Any) {
        copyClicked?()
    }
}
