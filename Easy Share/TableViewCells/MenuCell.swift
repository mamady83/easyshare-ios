//
//  MenuCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class MenuCell : UITableViewCell {
   
    @IBOutlet weak var sharedFilesBtn: UIButton!
    @IBOutlet weak var recentFilesBtn: UIButton!
    @IBOutlet weak var recentFilesBadge: UIView!
    @IBOutlet weak var sharedFilesBadge: UIView!
    
    public var onMoreClicked: (()->())?
    public var onTabChanged : ((Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        recentFilesBtn.titleLabel?.font = FontKit.roundedFont(ofSize: 14, weight: .medium)
        sharedFilesBtn.titleLabel?.font = FontKit.roundedFont(ofSize: 14, weight: .medium)
        selectionStyle = .none
    }
    
    public func selectRecent() {
        recentFilesBtn.setTitleColor(UIColor(named: "colorTextPrimary"), for: .normal)
        sharedFilesBtn.setTitleColor(UIColor(named: "colorTextInactive"), for: .normal)
        
        recentFilesBadge.isHidden = false
        sharedFilesBadge.isHidden = true
    }
    
    public func selectShared() {
        sharedFilesBtn.setTitleColor(UIColor(named: "colorTextPrimary"), for: .normal)
        recentFilesBtn.setTitleColor(UIColor(named: "colorTextInactive"), for: .normal)
        
        sharedFilesBadge.isHidden = false
        recentFilesBadge.isHidden = true
    }
    
    
    @IBAction func recentFilesClicked(_ sender: Any) {
        selectRecent()
        onTabChanged?(MenuTab.RECENT_FILES)
    }
    @IBAction func sharedFilesClicked(_ sender: Any) {
        selectShared()
        onTabChanged?(MenuTab.SHARED_FILES)
    }
    @IBAction func menuClicked(_ sender: Any) {
        onMoreClicked?()
    }
}
