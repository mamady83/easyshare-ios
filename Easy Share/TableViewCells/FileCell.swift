//
//  FileCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class FileCell : UITableViewCell {
    
    @IBOutlet weak var fileImage: UIImageView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var subtitleLable: UILabel!
    @IBOutlet weak var ellipsisBtn: UIButton!
    
    public var onEllipsisClicked: (()->())?
    let dateFormatter = DateFormatter()

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLable.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        subtitleLable.font = FontKit.roundedFont(ofSize: 10, weight: .semibold)
        selectionStyle = .none
        
        
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMM yyyy"

    }
    
    @IBAction func onEllipsisClicked(_ sender: Any) {
        onEllipsisClicked?()
    }
    
    func initWithItem(vc: UIViewController, item: Any?) {
        if let file = item as? FileTO {
            titleLable.text = file.fileName
            
            let folder = FolderDAO().getSingle(folderId: file.folderId)
            let fileSize = Units(bytes: Int64(file.fileSize)).getReadableUnit()
            let folerName = folder?.folderName ?? "SweetShare"
            let date = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(file.modifiedAt)))
            subtitleLable.text = "\(fileSize) - \(date) \(folerName)"
            subtitleLable.highlight(textToHightlight: folerName, color: UIColor(named: "colorBlue")!)
            
            switch file.fileType {
            case FileType.PHOTO:
                fileImage.image = UIImage(named: "m_image_placeholder")
                fileImage.backgroundColor = UIColor(named: "colorImageOpaque")
            case FileType.MUSIC:
                fileImage.image = UIImage(named: "m_music_placeholder")
                fileImage.backgroundColor = UIColor(named: "colorMusicOpaque")
            case FileType.VIDEO:
                fileImage.image = UIImage(named: "m_video_placeholder")
                fileImage.backgroundColor = UIColor(named: "colorVideoOpaque")
            case FileType.DOCUMENT:
                fileImage.image = UIImage(named: "m_file_placeholder")
                fileImage.backgroundColor = UIColor(named: "colorFileOpaque")
            default:
                fileImage.image = UIImage(named: "m_file_placeholder")
                fileImage.backgroundColor = UIColor(named: "colorFileOpaque")
            }
            
            onEllipsisClicked = {
                showFileSheet(vc: vc, file: file)
            }
        }
        
    }
}
