//
//  DatePickerDialog.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/31/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit


class DatePickerDialog: UIViewController {

    
    let datePicker = UIDatePicker()
    var onDatePicked: ((String)->())?
    var date = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.5
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)
        
        
        let container = UIView(frame: CGRect(x: 40, y: view.center.y - 130, width: view.frame.width - 80, height: 260))
        container.layer.cornerRadius = 14
        container.backgroundColor = UIColor(named: "colorBackground")
        container.layer.shadowColor = UIColor(named: "colorTextPrimary")?.cgColor
        container.layer.shadowOffset = CGSize(width: 0, height: 2)
        container.layer.shadowRadius = 18
        container.layer.shadowOpacity = 0.1
        
        let title = UILabel(frame: CGRect(x: 0, y: 12, width: container.frame.width, height: 28))
        title.text = "Expire Date"
        title.textColor = UIColor(named: "colorTextPrimary")
        title.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
        title.textAlignment = .center
        container.addSubview(title)
        
        datePicker.frame = CGRect(x: 0, y: 50, width: container.frame.width, height: 180)
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerTarget), for: .valueChanged)
        container.addSubview(datePicker)
        
        let cancelButton = UIButton(frame: CGRect(x: 0, y: container.frame.height - 50, width: container.frame.width / 2, height: 50))
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.titleLabel?.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        cancelButton.setTitleColor(UIColor(named: "colorTextInactive"), for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        container.addSubview(cancelButton)
        
        let okBtn = UIButton(frame: CGRect(x: container.frame.width / 2, y: container.frame.height - 50, width: container.frame.width / 2, height: 50))
        okBtn.titleLabel?.textAlignment = .center
        okBtn.titleLabel?.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
        okBtn.setTitle("Done", for: .normal)
        okBtn.setTitleColor(UIColor(named: "colorBlue"), for: .normal)
        okBtn.addTarget(self, action: #selector(doneClicked), for: .touchUpInside)
        container.addSubview(okBtn)
        
        view.addSubview(container)
    }
    
    @objc func datePickerTarget(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        date = formatter.string(from: datePicker.date)
    }
    
    @objc func cancelClicked(){
        dismiss(animated: true, completion: nil)
    }
  
    @objc func doneClicked(){
        onDatePicked?(date)
        dismiss(animated: true, completion: nil)
    }
  
}
