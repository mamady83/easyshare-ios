//
//  BackButton.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

@IBDesignable class BackButton : UIButton {
    
    override func awakeFromNib() {
        addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
    }
    
    @objc func backButtonClick(sender : UIButton) {
        if let parent = sender.parentViewController {
            if let nav = parent.navigationController {
                nav.popViewController(animated: true)
            }else {
                parent.dismiss(animated: true, completion: nil)
            }
        }
    }
}
