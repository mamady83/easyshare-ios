//
//  RoundedFontLable.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

@IBDesignable
public class RoundedLable : UILabel {
    
 
    @IBInspectable
    public var roundFontSize: CGFloat = 2.0 {
        didSet {
              commonInit()
        }
    }
    

    @IBInspectable
    var light: Bool = false
    
    @IBInspectable
    var medium: Bool = false
    
    @IBInspectable
    var heavy: Bool = false
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        var mWeight = UIFont.Weight.bold
        if medium {
            mWeight = UIFont.Weight.medium
        }else if light {
            mWeight = UIFont.Weight.light
        }else if heavy {
            mWeight = UIFont.Weight.heavy
        }
        self.font = FontKit.roundedFont(ofSize: roundFontSize, weight: mWeight)
    }

}


@IBDesignable
public class RoundedButton : UIButton {
    
 
    @IBInspectable
    public var roundFontSize: CGFloat = 2.0 {
        didSet {
              commonInit()
        }
    }
    

    @IBInspectable
    var light: Bool = false
    
    @IBInspectable
    var medium: Bool = false
    
    @IBInspectable
    var heavy: Bool = false
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        var mWeight = UIFont.Weight.bold
        if medium {
            mWeight = UIFont.Weight.medium
        }else if light {
            mWeight = UIFont.Weight.light
        }else if heavy {
            mWeight = UIFont.Weight.heavy
        }
        self.titleLabel?.font = FontKit.roundedFont(ofSize: roundFontSize, weight: mWeight)
    }

}
