//
//  ClickActions.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import RealmSwift

func onFileClicked(vc: UIViewController, fileId: Int){
    guard let file = FileDAO().getSingle(fileId: fileId) else {
        return
    }
    
    let url = URL(fileURLWithPath: file.filePathOffline)
    print("checking to see if file exists \(url.path)")
    if file.filePathOffline != "", FileManager.default.fileExists(atPath: url.path) {
        if let split = vc.splitViewController {
            if split.viewControllers.count >= 2 {
                if let detail = split.viewControllers[1] as? DetailViewController {
                    detail.fileId = file.fileId
                }
            }else if let detail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as? DetailViewController {
                detail.fileId = file.fileId
                DispatchQueue.main.async(execute: { () -> Void in
                    vc.present(detail, animated: true, completion: nil)
                })
            }
        }
    }else {
        if let dlVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "downloadVC") as? DownloadViewController {
            if #available(iOS 13.0, *) {
                dlVC.isModalInPresentation = false
            }
            dlVC.file = file
            dlVC.isModalInPopover = false
            dlVC.modalTransitionStyle = .crossDissolve
            dlVC.modalPresentationStyle = .overFullScreen
            dlVC.onDownloadFinished = {
                onFileClicked(vc: vc, fileId: fileId)
            }
            vc.present(dlVC, animated: true, completion: nil)
        }
    }

}

func onFolderClicked(vc: UIViewController, item: FolderTO){
    if let folderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "folderVC") as? FolderViewController {
        folderVC.folder = item
        if let nav = vc.navigationController {
            nav.pushViewController(folderVC, animated: true)
        }else {
            vc.present(folderVC, animated: true, completion: nil)
        }
    }
}

func onRenameClicked(vc: UIViewController, item: Any){
    if let renameVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "renameVC") as? RenameViewController {
        renameVC.item = item
        vc.present(renameVC, animated: true, completion: nil)
    }
}

func onCreateLinkClicked(vc: UIViewController, file: FileTO){
    if let createLinkVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createLinkVC") as? CreateLinkViewController {
        createLinkVC.file = file
        vc.present(createLinkVC, animated: true, completion: nil)
    }
}

func onLinkClicked(vc: UIViewController, link: LinkTO){
    if let linkVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createLinkVC") as? CreateLinkViewController {
        linkVC.link = link
        vc.present(linkVC, animated: true, completion: nil)
    }
}


func onFileLinksClicked(vc: UIViewController, file: FileTO){
    if let linksVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "linkSettingsVC") as? LinksViewController {
        linksVC.fileId = file.fileId
        vc.present(linksVC, animated: true, completion: nil)
    }
}

func onCreateFolderClicked(vc: UIViewController){
    if let createFolderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createFolderVC") as? CreateFolderViewController {
        vc.present(createFolderVC, animated: true, completion: nil)
    }
}

func onMoveClicked(vc: UIViewController, item: FileTO){
    if let moveVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "moveVC") as? MoveViewController {
        moveVC.moveType = MOVE_TYPE_MOVE
        moveVC.file = item
        vc.present(moveVC, animated: true, completion: nil)
    }
}


func logoutUser(vc: UIViewController){
    
    LocalStorage.shared().removeAllData()
    let realm = MyRealm.db
    try! realm.write {
        realm.deleteAll()
    }
    restartApplication()
    
}

func goToHome(source: UIViewController){
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let initialViewController = storyboard.instantiateViewController(withIdentifier: "splitVC")
    UIApplication.shared.setRootVC(initialViewController)
    
}

func restartApplication() {
    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
    
    guard
        let window = UIApplication.shared.keyWindow,
        let rootViewController = window.rootViewController
        else {
            return
    }
    
    viewController.view.frame = rootViewController.view.frame
    viewController.view.layoutIfNeeded()
    
    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
        window.rootViewController = viewController
    })
}
