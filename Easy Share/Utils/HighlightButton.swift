//
//  HighlightButton.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/31/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class HighlightButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor(named: "colorBorder") : UIColor(named: "colorBackground")
        }
    }
}
