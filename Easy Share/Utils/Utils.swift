//
//  Utils.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import SwiftMessages

public func showSwiftyMessage(title: String, content: String, theme: Theme){
    let message = MessageView.viewFromNib(layout: .cardView)
    message.configureTheme(theme)
    message.configureDropShadow()
    message.titleLabel?.textAlignment = .left
    message.bodyLabel?.textAlignment = .left
    message.titleLabel?.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
    message.bodyLabel?.font = FontKit.roundedFont(ofSize: 14, weight: .regular)
    message.configureContent(title: title, body: content)
    message.button?.isHidden = true
    var config = SwiftMessages.defaultConfig
    config.presentationStyle = .top
    config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
    config.dimMode = .blur(style: .regular, alpha: 0.8, interactive: true)
    config.duration = .seconds(seconds: 3)
    SwiftMessages.show(config: config, view: message)
    
}

public func showNoInternetMessage(){
    showSwiftyMessage(title: "You’re Offline", content: "Turn off Airplane Mode or connect to Wi-Fi", theme: .error)
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}

public func shareFile(vc: UIViewController, path: String){
    print("sharing \(path)")
    let fileURL = URL(fileURLWithPath: path)
    
    var filesToShare = [Any]()

    filesToShare.append(fileURL)
    
    let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
    activityViewController.excludedActivityTypes = nil

    vc.present(activityViewController, animated: true, completion: nil)
}

