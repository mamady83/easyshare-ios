//
//  Extensions.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SnapKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    class func fromNib(named: String? = nil) -> Self {
        let name = named ?? "\(Self.self)"
        guard
            let nib = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
            else { fatalError("missing expected nib named: \(name)") }
        guard
            let view = nib.first as? Self
            else { fatalError("view of type \(Self.self) not found in \(nib)") }
        return view
    }
    
    func showNib(nibName: String, tag: Int){
        hideTag(tag: tag)
        let view = UIView.fromNib(named: nibName)
        view.tag = tag
        view.frame = self.frame
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = false
        addSubview(view)
        bringSubviewToFront(view)
    }
    
    func hideTag(tag: Int){
        for f in self.subviews {
            if f.tag == tag {
                f.removeFromSuperview()
            }
        }
    }
    
    func setViewBGbyTag(tag: Int, bgColor: UIColor?) {
        for f in self.subviews {
            if f.tag == tag {
                f.backgroundColor = bgColor
            }
        }
    }
}

extension UILabel {
    func highlight(textToHightlight: String, color: UIColor) {
                
        let subtitle_attributed_string = NSMutableAttributedString(string: self.text ?? "")
        if let range = self.text?.range(of: textToHightlight) {
            subtitle_attributed_string.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(range, in: self.text ?? ""))
        }
       
        self.attributedText = subtitle_attributed_string
    }
}
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
}
extension UIButton {
    
    func showLoading(color: UIColor, bg: UIColor) {
        let loading = NVActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: self.bounds.width - 10, height: self.bounds.height - 10), type: .ballPulse, color: color, padding: 2)
        loading.backgroundColor = bg
        self.addSubview(loading)
        self.bringSubviewToFront(loading)
        loading.startAnimating()
        self.isUserInteractionEnabled = false
    }
    func showLoading() {
        let loading = NVActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: self.bounds.width - 10, height: self.bounds.height - 10), type: .ballPulse, color: .white, padding: 2)
        loading.backgroundColor = UIColor(named: "colorAccent")
        self.addSubview(loading)
        self.bringSubviewToFront(loading)
        loading.startAnimating()
        self.isUserInteractionEnabled = false
    }
    func hideLoading(){
        for f in self.subviews {
            if let loading = f as? NVActivityIndicatorView {
                loading.removeFromSuperview()
            }
        }
        self.isUserInteractionEnabled = true
    }
}

extension UIApplication {
    
    func setRootVC(_ vc : UIViewController){
        
        self.windows.first?.rootViewController = vc
        self.windows.first?.makeKeyAndVisible()
        
    }
}

extension UITableView {
    
    func showLoading() {
        let loading = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40), type: .ballTrianglePath, color: UIColor(named: "colorTextPrimary"), padding: 0)
        loading.backgroundColor = .clear
        self.addSubview(loading)
        loading.snp.makeConstraints { make -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY).offset(-50)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        self.bringSubviewToFront(loading)
        loading.startAnimating()
        self.isUserInteractionEnabled = false
    }
    
    func hideLoading(){
        for f in self.subviews {
            if let loading = f as? NVActivityIndicatorView {
                loading.removeFromSuperview()
            }
        }
        self.isUserInteractionEnabled = true
    }
    
    
}

extension UICollectionView {
    
    func showLoading() {
        let loading = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40), type: .ballTrianglePath, color: UIColor(named: "colorTextPrimary"), padding: 0)
        loading.backgroundColor = .clear
        self.addSubview(loading)
        loading.snp.makeConstraints { make -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY).offset(-50)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        self.bringSubviewToFront(loading)
        loading.startAnimating()
        self.isUserInteractionEnabled = false
    }
    func hideLoading(){
        for f in self.subviews {
            if let loading = f as? NVActivityIndicatorView {
                loading.removeFromSuperview()
            }
        }
        self.isUserInteractionEnabled = true
    }
    
}
extension Date {
    
 
    var yearsFromNow: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
    var monthsFromNow: Int {
        return Calendar.current.dateComponents([.month], from: self, to: Date()).month!
    }
    var weeksFromNow: Int {
        return Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear!
    }
    var daysFromNow: Int {
        return Calendar.current.dateComponents([.day], from: self, to: Date()).day!
    }
    var isInYesterday: Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    var hoursFromNow: Int {
        return Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
    }
    var minutesFromNow: Int {
        return Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
    }
    var secondsFromNow: Int {
        return Calendar.current.dateComponents([.second], from: self, to: Date()).second!
    }
    var relativeTime: String {
        if yearsFromNow > 0 { return "\(yearsFromNow) year" + (yearsFromNow > 1 ? "s" : "") + " ago" }
        if monthsFromNow > 0 { return "\(monthsFromNow) month" + (monthsFromNow > 1 ? "s" : "") + " ago" }
        if weeksFromNow > 0 { return "\(weeksFromNow) week" + (weeksFromNow > 1 ? "s" : "") + " ago" }
        if isInYesterday { return "Yesterday" }
        if daysFromNow > 0 { return "\(daysFromNow) day" + (daysFromNow > 1 ? "s" : "") + " ago" }
        if hoursFromNow > 0 { return "\(hoursFromNow) hour" + (hoursFromNow > 1 ? "s" : "") + " ago" }
        if minutesFromNow > 0 { return "\(minutesFromNow) minute" + (minutesFromNow > 1 ? "s" : "") + " ago" }
        if secondsFromNow > 0 { return secondsFromNow < 15 ? "Just now"
            : "\(secondsFromNow) second" + (secondsFromNow > 1 ? "s" : "") + " ago" }
        return ""
    }
}

