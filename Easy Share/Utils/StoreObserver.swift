//
//  StoreObserver.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/29/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import StoreKit
import NetworkPlatform

class StoreObserver: NSObject, SKPaymentTransactionObserver {

    //Initialize the store observer.
    override init() {
        super.init()
        //Other initialization here.
    }


    //Observe transaction updates.
    func paymentQueue(_ queue: SKPaymentQueue,updatedTransactions transactions: [SKPaymentTransaction]) {
        var shouldValidate = false
        
        for transaction in transactions {
            
            switch transaction.transactionState {
            // Call the appropriate custom method for the transaction state.
            case .purchasing: print("purchasing")
            case .deferred: print("deferred")
            case .failed: print("failed")
            case .purchased: shouldValidate = true
            case .restored: print("restored")
            // For debugging purposes.
            @unknown default: print("Unexpected transaction state \(transaction.transactionState)")
            }
        }
        if shouldValidate {
            validatePurchase()
        }
    }
    func validatePurchase(){
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {


            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
              //  print(receiptData)


                let receiptString = receiptData.base64EncodedString(options: [])


                ApiVerifyApplePurchase(onSuccess: {
                    
                }, onError: { (message) in
                    
                }, onConnectionError: {
                    
                }, onInvalidToken: {
                    
                }).call(reciept: receiptString)
                // Read receiptData
            }
            catch { print("Couldn't read receipt data with error: " + error.localizedDescription) }
        }else {
            print("appStoreReceiptURL empty")
        }
    }
}
