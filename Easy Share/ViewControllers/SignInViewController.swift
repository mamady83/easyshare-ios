//
//  SignInViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateForm() {
            login()
        }
        
    }
    
    fileprivate func login(){
        
        submitBtn.showLoading()
        ApiLogin(onSuccess: { [weak self] in
            self?.submitBtn.hideLoading()
            if let this = self {
                goToHome(source: this)
            }
        }, onError: { [weak self] (message) in
            self?.submitBtn.hideLoading()
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: { [weak self] in
            self?.submitBtn.hideLoading()
            showNoInternetMessage()
        }).call(
            email: email.text ?? "",
            password: password.text ?? ""
        )
    }
    
    fileprivate func validateForm() -> Bool{
        
        if email.text == "" {
            showSwiftyMessage(title: "Email is empty!", content: "Please fill in your email", theme: .warning)
            return false
        }
        
        if !(email.text ?? "").isValidEmail() {
            showSwiftyMessage(title: "Email is invalid!", content: "Please enter a valid a email address", theme: .warning)
            return false
        }
        
        if password.text == "" {
            showSwiftyMessage(title: "Password is empty!", content: "Please fill in your password", theme: .warning)
            return false
        }
        
        return true
    }
}
