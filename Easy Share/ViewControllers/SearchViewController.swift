//
//  SearchTableViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/17/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform


class SearchViewController: UITableViewController, UISearchResultsUpdating {
    
    private let EMPTY_LAYOUT_TAG = 749203
    private let EMPTY_LAYOUT_NAME = "SearchEmptyLayout"
    private let INTERNET_LAYOUT_TAG = 10467
    private let INTERNET_LAYOUT_NAME = "InternetLayout"
    
    var tempQuery = ""
    
    
    var searchRequest : ApiSearch? = nil
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text else {
            return
        }
        if query == "" {
            return
        }
        searchFor(query: query)
    }
    
    var items: [HomeTO] = Array()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "FileCell", bundle: nil), forCellReuseIdentifier: "FileCell")
        tableView.register(UINib(nibName: "FolderCell", bundle: nil), forCellReuseIdentifier: "FolderCell")
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(globalUpdate), name: NotificationNames.globalUpdate, object: nil)
    }
    
    
    @objc func globalUpdate(){
        searchFor(query: tempQuery)
    }
    
    fileprivate func searchFor(query: String) {
        searchRequest?.cancel()
        items.removeAll()
        
        tableView.showLoading()
        tempQuery = query
        tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        tableView.hideTag(tag: INTERNET_LAYOUT_TAG)
        searchRequest = ApiSearch(onSuccess: {[weak self] (newItem) in
            self?.tableView?.hideLoading()
            if newItem.isEmpty {
                self?.checkIfEmpty()
            }else {
                self?.items.append(HomeTO(type: HomeCellType.HEADER, item: "Results"))
                self?.items.append(contentsOf: newItem)
            }
            self?.tableView.reloadData()
            }, onError: { [weak self] (message) in
                self?.tableView?.hideLoading()
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.tableView?.hideLoading()
                self?.showInternetLayout()
            }, onInvalidToken: {[weak self] in
                self?.tableView?.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }
        )
        searchRequest?.call(
            query: query
        )
    }
    
    
    fileprivate func showInternetLayout(){
        
        if items.count == 0 {
            tableView.showNib(nibName: INTERNET_LAYOUT_NAME, tag: INTERNET_LAYOUT_TAG)
        }
    }
    fileprivate func checkIfEmpty(){
        if items.count == 0 {
            tableView.showNib(nibName: EMPTY_LAYOUT_NAME, tag: EMPTY_LAYOUT_TAG)
        }else {
            tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        }
    }
    
}

//MARK:- Table View

extension SearchViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        switch item.type {
        case HomeCellType.HEADER:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            cell.initWithItem(item: item.item)
            return cell
        case HomeCellType.FILE:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as! FileCell
            cell.initWithItem(vc: self, item: item.item)
            return cell
        case HomeCellType.FOLDER:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath) as! FolderCell
            cell.initWithItem(vc: self, item: item.item)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch items[indexPath.row].type {
        case HomeCellType.FILE:
            if let file = items[indexPath.row].item as? FileTO {
                if let vc = self.presentingViewController {
                    onFileClicked(vc: vc, fileId: file.fileId)
                }
            }
        case HomeCellType.FOLDER:
            if let folder = items[indexPath.row].item as? FolderTO {
                if let vc = self.presentingViewController {
                    onFolderClicked(vc: vc, item: folder)
                }
            }
        default:
            print("Cell Type not recognized")
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.row]
        
        switch item.type {
        case HomeCellType.FOLDER:
            return 72
        case HomeCellType.HEADER:
            return 28
        case HomeCellType.FILE:
            return 72
        default:
            return 50
        }
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if items[indexPath.row].type == HomeCellType.FILE {
            return true
        }else {
            return false
        }
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(
            style: .destructive,
            title: "",
            handler: {[weak self] (action, view, completion) in
                if let this = self {
                    showDeleteSheet(vc: this, item: this.items[indexPath.row].item)
                }
                completion(false)
                
        })
        if let cgImageX =  UIImage(named: "m_row_delete")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor(named: "colorBackground")
        
        let configuration = UISwipeActionsConfiguration(actions: [action])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}
