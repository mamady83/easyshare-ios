//
//  DownloadViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/22/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform
import Alamofire
import Foundation

class DownloadViewController: BaseViewController {
    
    var dlRequest : DownloadRequest? = nil
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var dialogTitle: RoundedLable!
    
    public var onDownloadFinished: (()->())?
    var file: FileTO? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.5
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.sendSubviewToBack(blurEffectView)
        
        getFile()
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        dlRequest?.cancel()
        dismiss(animated: true, completion: nil)
    }
    
    
    fileprivate func getFile(){
        guard let file = self.file else {
            return
        }
        
        ApiFileSingle(onSuccess: {[weak self] (newFile) in
            self?.file = newFile
            self?.downloadFile()
            }, onError: {[weak self] (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
                if let this = self {
                    this.dismiss(animated: true, completion: nil)
                }
            }, onConnectionError: {[weak self] in
                showNoInternetMessage()
                if let this = self {
                    this.dismiss(animated: true, completion: nil)
                }
            }, onInvalidToken: {[weak self] in
                if let this = self {
                    logoutUser(vc: this)
                }
        }).call(
            fileId: file.fileId
        )
    }
    
    fileprivate func downloadFile(){
        
        guard var file = self.file else {
            return
        }
        
        guard let downloadUrl = URL(string: file.filePathOnline) else {
            return
        }
        let docuements = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

        let filename = URL(fileURLWithPath: file.fileName).deletingPathExtension().lastPathComponent
        let fileUrl = docuements.appendingPathComponent("\(filename)\(Int.random(in: 0..<1000))").appendingPathExtension(file.fileExtension)

       
        let destination : DownloadRequest.Destination = { _, _ in
            return (fileUrl, [DownloadRequest.Options.removePreviousFile])
        }

        dlRequest = AF.download(
            downloadUrl,
            to: destination
        ).downloadProgress(closure: {[weak self] (progress) in
            self?.progressView.setProgress(Float(progress.fractionCompleted), animated: true)
        }).response(completionHandler: { [weak self] (response) in
            
            if response.error == nil, let filePath = response.fileURL {
                print("file downloaded \(filePath.path)")
                
                file.filePathOffline = filePath.path
                FileDAO().publicCreateOrUpdate(file: file)
                
                self?.onDownloadFinished?()
                self?.dismiss(animated: true, completion: nil)
            }else {
                print("file download error \(String(describing: response.error))")
                if let this = self {
                    this.dismiss(animated: true, completion: nil)
                }
            }
        })
    }
}
