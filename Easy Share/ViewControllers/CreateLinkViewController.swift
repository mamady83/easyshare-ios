//
//  CreateLinkViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import NVActivityIndicatorView
import Domain
import MaterialShowcase

class CreateLinkViewController: UIViewController, MaterialShowcaseDelegate {
    
    @IBOutlet weak var pageTitle: RoundedLable!
    @IBOutlet weak var backBtn: BackButton!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var expireDateBtn: UIButton!
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var passwordLable: RoundedLable!
    @IBOutlet weak var expireDateLable: RoundedLable!
    @IBOutlet weak var linkLable: RoundedLable!
    
    
    var loadingContainer = UIView()
    var loading : NVActivityIndicatorView?
    
    var file : FileTO?
    var link : LinkTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoadingView()
        if link == nil {
            createLink()
        }else {
            pageTitle.text = "Edit Link"
            loading?.stopAnimating()
            loadingContainer.isHidden = true
            initData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[weak self] in
            self?.showTapTarget()
        }
    }
    func showCaseDidDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
        showExpireTapTarget()
    }
    func showTapTarget(){
        if !LocalStorage.shared().linksFirstTime {
            return
        }
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: passwordBtn)
        showcase.primaryText = "Protect your links!"
        showcase.secondaryText = "Set a password on your links to protect them against unintended visits"
        showcase.backgroundPromptColor = UIColor(named: "colorAccent")
        showcase.backgroundPromptColorAlpha = 0.9
        showcase.show(completion: {
        })
        showcase.delegate = self
    }
    func showExpireTapTarget(){
      
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: expireDateBtn)
        showcase.primaryText = "Set a Time Limit"
        showcase.secondaryText = "You can specify a date, at which this link will expire and no longer available"
        showcase.backgroundPromptColor = UIColor(named: "colorBlue")
        showcase.backgroundPromptColorAlpha = 0.9
        showcase.show(completion: {
            LocalStorage.shared().linksFirstTime = false
        })
    }
    
    fileprivate func setupLoadingView() {
        loading = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40), type: .ballTrianglePath, color: UIColor(named: "colorTextPrimary"), padding: 0)
        loadingContainer = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        loadingContainer.backgroundColor = UIColor(named: "colorBackground")
        loadingContainer.addSubview(loading!)
        loading?.snp.makeConstraints { make -> Void in
            make.center.equalTo(loadingContainer.snp.center)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        view.addSubview(loadingContainer)
        loadingContainer.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(view.snp.left)
            make.bottom.equalTo(view.snp.bottom)
            make.right.equalTo(view.snp.right)
            make.top.equalTo(backBtn.snp.bottom).offset(12)
        }
    }
    
    fileprivate func createLink(){
        loadingContainer.isHidden = false
        loading?.startAnimating()

        ApiShareFilesStore(onSuccess: { [weak self] (link) in
            self?.link = link
            self?.initData()
            self?.loading?.stopAnimating()
            self?.loadingContainer.isHidden = true
            }, onError: { [weak self] (message) in
                self?.loading?.stopAnimating()
                self?.loadingContainer.isHidden = true
                if let this = self {
                    this.dismiss(animated: true, completion: {
                        showSwiftyMessage(title: "There was an error", content: message, theme: .error)
                    })
                }
        }, onConnectionError: { [weak self] in
            self?.loading?.stopAnimating()
            self?.loadingContainer.isHidden = true
            if let this = self {
                this.dismiss(animated: true, completion: {
                    showNoInternetMessage()
                })
            }
        }, onInvalidToken: { [weak self] in
            self?.loading?.stopAnimating()
            self?.loadingContainer.isHidden = true
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            fileId: file?.fileId ?? 0,
            password: "",
            expires_at: ""
        )
    }
    
    fileprivate func initData(){
        linkLable.text = link?.url ?? ""
        
        if link?.isPasswordProtected == true {
            passwordLable.text = "Password Protected"
            passwordBtn.setTitle("Delete", for: .normal)
        }else {
            passwordLable.text = "None"
            passwordBtn.setTitle("Set", for: .normal)
        }
        if link?.expires_at != 0 {
            let date = Date(timeIntervalSince1970: TimeInterval(link?.expires_at ?? 0))
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: date)
            expireDateLable.text = "Expires at \(strDate)"
            expireDateBtn.setTitle("Delete", for: .normal)
        }else{
            expireDateLable.text = "None"
            expireDateBtn.setTitle("Set", for: .normal)
        }
    }
    
    @IBAction func copyClicked(_ sender: Any) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = link?.url ?? ""
        showSwiftyMessage(title: "Link Copied!", content: "Share link copied to clipboard", theme: .success)
    }
    @IBAction func expireDateClicked(_ sender: Any) {
        if link?.expires_at == 0 {
            showDatePicker()
        }else {
            removeExpireDate()
        }
    }
    @IBAction func passwordClicked(_ sender: Any) {
        if link?.isPasswordProtected == true {
            removePassword()
        }else {
            showSetPasswordPage()
        }
    }
    
    fileprivate func showDatePicker() {
    
        let datePicker = DatePickerDialog()
        if #available(iOS 13.0, *) {
            datePicker.isModalInPresentation = false
        }
        datePicker.onDatePicked = { [weak self] date in
            self?.setExpireDate(date: date)
        }
        datePicker.isModalInPopover = false
        datePicker.modalTransitionStyle = .crossDissolve
        datePicker.modalPresentationStyle = .overFullScreen
        present(datePicker, animated: true, completion: nil)

    }
    fileprivate func removeExpireDate() {
        ApiShareFilesUpdate(onSuccess: {[weak self] in
            self?.link = LinkDAO().getSingle(linkId: self?.link?.linkId ?? 0)
            self?.initData()
        }, onError: { (message) in
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            linkId: link?.linkId ?? 0,
            password: "",
            expire_date: "",
            remove_password: false,
            remove_expire: true
        )
    }
    fileprivate func showSetPasswordPage(){
        if let vc = storyboard?.instantiateViewController(withIdentifier: "linkPasswordVC") as? LinkPasswordViewController {
            vc.link = link
            vc.onPasswordSet = {[weak self] in
                self?.link = LinkDAO().getSingle(linkId: self?.link?.linkId ?? 0)
                self?.initData()
            }
            present(vc, animated: true, completion: nil)
        }
    }
    fileprivate func removePassword(){
        ApiShareFilesUpdate(onSuccess: {[weak self] in
            self?.link = LinkDAO().getSingle(linkId: self?.link?.linkId ?? 0)
            self?.initData()
        }, onError: { (message) in
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            linkId: link?.linkId ?? 0,
            password: "",
            expire_date: "",
            remove_password: true,
            remove_expire: false
        )
    }
    fileprivate func setExpireDate(date: String){
        ApiShareFilesUpdate(onSuccess: {[weak self] in
            self?.link = LinkDAO().getSingle(linkId: self?.link?.linkId ?? 0)
            self?.initData()
        }, onError: { (message) in
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            linkId: link?.linkId ?? 0,
            password: "",
            expire_date: date,
            remove_password: false,
            remove_expire: false
        )
    }
    
}
