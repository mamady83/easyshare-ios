//
//  PreviewViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import AVFoundation


class PreviewViewController: UIViewController {
    
    var player: AVAudioPlayer?
    var playerProgress: UIProgressView?
    var file: FileTO?  {
        didSet {
            fileSet()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if file == nil {
            initEmptyView()
        }
        
    }
    fileprivate func initEmptyView(){
        for v in view.subviews {
            v.removeFromSuperview()
        }
        let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        image.image = UIImage(named: "expired_image")
        view.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.width.equalTo(206)
            make.height.equalTo(248)
            make.centerX.equalTo(view.snp.centerX)
            make.centerY.equalTo(view.snp.centerY)
        }
        
        let lable = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        lable.text = "Nothing to Preview"
        lable.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
        lable.textColor = UIColor(named: "colorTextInactive")
        lable.textAlignment = .center
        view.addSubview(lable)
        lable.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.top.equalTo(image.snp.bottom).offset(12)
        }
    }
    fileprivate func fileSet(){
        if file == nil {
            initEmptyView()
            return
        }
        for v in view.subviews {
            v.removeFromSuperview()
        }
        DispatchQueue.main.async {
            switch self.file?.fileType ?? 0 {
            case FileType.PHOTO:
                self.initImagePreview()
                break
            case FileType.MUSIC:
                self.initMusicPreview()
                break
            case FileType.VIDEO:
                self.initVieoPreview()
                break
            default:
                self.initDocumentPreview()
            }
        }
        
       
    }
    
    fileprivate func initDocumentPreview(){
        
        let container = UIView(frame: CGRect(x: 60, y: 200, width: view.frame.width - 120, height: view.frame.width - 120))
        container.backgroundColor = UIColor(named: "colorFileOpaque")
        container.layer.cornerRadius = 16
        view.addSubview(container)
        container.snp.makeConstraints { (make) in
            make.center.equalTo(view.snp.center)
            make.width.equalTo(container.frame.width)
            make.height.equalTo(container.frame.width)
        }
        
        let fileImage = UIImageView(frame: container.frame)
        fileImage.image = UIImage(named: "m_file_placeholder")
        container.addSubview(fileImage)
        fileImage.snp.makeConstraints { (make) in
            make.width.equalTo(container.frame.width)
            make.height.equalTo(container.frame.width)
            make.edges.equalTo(container.snp.edges)
        }
        
        
    }
    
    fileprivate func initImagePreview(){
        
        let imageURL = URL(fileURLWithPath: file?.filePathOffline ?? "")
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        imageView.contentMode = .scaleAspectFit
        imageView.kf.setImage(with: imageURL)
        imageView.backgroundColor = UIColor(named: "colorBackground")
        view.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.snp.edges)
        }
    }
    
    fileprivate func initVieoPreview(){
        
        let centerX = view.frame.width / 2
        let centerY = view.frame.height / 2
        
        let playBtn = UIButton(frame: CGRect(x: centerX - 30, y: centerY - 100, width: 60, height: 68))
        playBtn.setImage(UIImage(named: "p_play"), for: .normal)
        playBtn.addTarget(self, action: #selector(videoPlayClicked(sender:)), for: .touchUpInside)
        view.addSubview(playBtn)
        
    }
    
    @objc func videoPlayClicked(sender: UIButton){
        let vc = VideoPlayerViewController()
        vc.file = file
        present(vc, animated: true, completion: nil)
    }
    
    fileprivate func initMusicPreview(){

        for v in view.subviews {
            v.removeFromSuperview()
        }
        
        let mp3URL = URL(fileURLWithPath: file?.filePathOffline ?? "")
        print("openning mp3 \(file?.filePathOffline ?? "")")
        let centerX = view.frame.width / 2
        let centerY = view.frame.height / 2
        
        let playBtn = UIButton(frame: CGRect(x: centerX - 20, y: centerY - 100, width: 40, height: 50))
        playBtn.setImage(UIImage(named: "p_play"), for: .normal)
        view.addSubview(playBtn)
        
        let fastForwardBtn = UIButton(frame: CGRect(x: centerX + 75, y: centerY - 85, width: 25, height: 15))
        fastForwardBtn.setImage(UIImage(named: "p_fast_forward"), for: .normal)
        view.addSubview(fastForwardBtn)
        
        let rewindBtn = UIButton(frame: CGRect(x: centerX - 100, y: centerY - 85, width: 25, height: 15))
        rewindBtn.setImage(UIImage(named: "p_rewind"), for: .normal)
        view.addSubview(rewindBtn)
        
        playerProgress = UIProgressView(frame: CGRect(x: 40, y: centerY, width: view.frame.width - 80, height: 2))
        playerProgress?.backgroundColor = UIColor(named: "colorUploadCellBG")
        playerProgress?.progressTintColor = UIColor(named: "colorBlue")
        playerProgress?.progressViewStyle = .bar
        view.addSubview(playerProgress!)

        
    
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: mp3URL, fileTypeHint: AVFileType.mp3.rawValue)
            let updater = CADisplayLink(target: self, selector: #selector(musicProgress))
            updater.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        } catch let error {
            print(error.localizedDescription)
        }
        
        playBtn.addTarget(self, action: #selector(playPauseClicked(sender:)), for: .touchUpInside)
        rewindBtn.addTarget(self, action: #selector(rewindClicked), for: .touchUpInside)
        fastForwardBtn.addTarget(self, action: #selector(fastForwardClicked), for: .touchUpInside)

    }
    
    @objc func musicProgress()  {

        guard let currenttime = player?.currentTime else {
            return
        }
        guard let duration = player?.duration else {
            return
        }
        let normalizedTime = Float(currenttime / duration )
        playerProgress?.setProgress(normalizedTime, animated: true)
    }
    
    @objc func playPauseClicked(sender: UIButton){
        if player?.isPlaying == true{
            player?.pause()
            sender.setImage(UIImage(named: "p_play"), for: .normal)
        }else {
            player?.play()
            sender.setImage(UIImage(named: "p_pause"), for: .normal)
        }

    }
    @objc func rewindClicked(){
        guard let duration = player?.duration else {
            return
        }
        if player?.currentTime ?? 0 - 15 > duration {
            player?.currentTime -= 15
        }
    }
    @objc func fastForwardClicked(){
        guard let duration = player?.duration else {
            return
        }
        if player?.currentTime ?? 0 + 15 < duration {
            player?.currentTime += 15
        }

    }
    
}
