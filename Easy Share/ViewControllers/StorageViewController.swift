//
//  StorageViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import Charts

class StorageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ChartViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "storageCell", for: indexPath) as! StorageCell
        cell.initWithItem(item: items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableHeightConstraint.constant = CGFloat(items.count) * CGFloat(50)
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chartView.highlightValue(x: Double(indexPath.row), dataSetIndex: 0)
        
    }
    var hasAddedCircleView = false
    
    var items: [StorageTO] = Array()
    
    @IBOutlet weak var chartView: PieChartView!
    @IBOutlet weak var premiumButton: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initChart()
        addCenterView()
        initData()
    }
    
    fileprivate func initData(){

        items = StorageDAO().all()
        
        let totlaStorage = LocalStorage.shared().totalStorage
        let availableBytes = totlaStorage - LocalStorage.shared().storageUsed
        let freeSpace = Units(bytes: Int64(availableBytes)).getReadableUnit()
        
        items.append(
            StorageTO(title: "Free",
                      color: UIColor(named: "colorFree")!,
                      space: freeSpace,
                      value: availableBytes / totlaStorage,
                      type: 0,
                      size: 0
            )
        )
        
        setChartData()
    }
    fileprivate func initChart(){
        
        chartView.delegate = self
        
        chartView.legend.enabled = false
        
        // entry label styling
        chartView.entryLabelColor = .white
        chartView.entryLabelFont = .systemFont(ofSize: 12, weight: .light)
        
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.5
        chartView.transparentCircleRadiusPercent = 0.60
        chartView.transparentCircleColor = UIColor.black.withAlphaComponent(0.15)
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        chartView.drawEntryLabelsEnabled = false
        chartView.drawCenterTextEnabled = false
        chartView.drawHoleEnabled = true
        chartView.rotationEnabled = false
        chartView.highlightPerTapEnabled = true
        
        
                
    }

    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        for i in 0..<items.count {
            if let lable = entry.data as? String {
                if items[i].title == lable {
                    tableView.selectRow(at: IndexPath(row: i, section: 0), animated: true, scrollPosition: .top)
                    break
                }
            }
        }
    }
    fileprivate func addCenterView(){

        let totlaStorage = Units(bytes: Int64(LocalStorage.shared().totalStorage)).getReadableUnit()
        let availableBytes = LocalStorage.shared().totalStorage - LocalStorage.shared().storageUsed
        let availableStorage = Units(bytes: Int64(availableBytes)).getReadableUnit()
        
        let circleWidth = ( UIScreen.main.bounds.width - 80 ) / 2
        let container = UIView(frame: CGRect(x: circleWidth/2, y: circleWidth/2, width: circleWidth, height: circleWidth))
        container.backgroundColor = UIColor(named: "colorBackground")
        container.clipsToBounds = true
        container.layer.cornerRadius = circleWidth / 2
        chartView.addSubview(container)
        
        let available = UILabel(frame: CGRect.zero)
        available.font = FontKit.roundedFont(ofSize: 12, weight: .bold)
        available.textColor = UIColor(named: "colorTextPrimary")
        available.text = "Available"
        available.textAlignment = .center
        
        
        let used = UILabel(frame: CGRect.zero)
        used.font = FontKit.roundedFont(ofSize: 22, weight: .bold)
        used.textColor = UIColor(named: "colorTextPrimary")
        used.text = availableStorage
        used.textAlignment = .center
        
        let total = UILabel(frame: CGRect.zero)
        total.font = FontKit.roundedFont(ofSize: 12, weight: .bold)
        total.textColor = UIColor(named: "colorTextPrimary")
        total.text = "Total \(totlaStorage)"
        total.textAlignment = .center
        
        container.addSubview(available)
        container.addSubview(used)
        container.addSubview(total)
        
        
        used.snp.makeConstraints { make -> Void in
            make.center.equalTo(container.snp.center)
        }
        total.snp.makeConstraints { make -> Void in
            make.top.equalTo(used.snp.bottom).offset(10)
            make.centerX.equalTo(container.snp.centerX)
        }
        available.snp.makeConstraints { make -> Void in
            make.bottom.equalTo(used.snp.top).offset(-4)
            make.centerX.equalTo(container.snp.centerX)
        }
        
        

    }
    
    func setChartData() {
        var entries : [PieChartDataEntry] = Array()
        var colors : [NSUIColor] = Array()
        for i in items {
            entries.append(PieChartDataEntry(value: i.value, data: i.title))
            colors.append(i.color)
        }
        
        let set = PieChartDataSet(entries: entries, label: "")
        set.sliceSpace = 4
        
        set.drawValuesEnabled = false
        set.colors = colors
        
        let data = PieChartData(dataSet: set)
        
        chartView.data = data
        chartView.highlightValues(nil)
        
        chartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)

    }
    
}
