//
//  FilesViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class FilesViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchResultsUpdating {
    
    private let EMPTY_LAYOUT_TAG = 14730
    private let EMPTY_LAYOUT_NAME = "FilesEmptyLayout"
    private let INTERNET_LAYOUT_TAG = 47229
    private let INTERNET_LAYOUT_NAME = "InternetLayout"
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let item = items[indexPath.row] as? FolderTO {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FolderCollectionCell", for: indexPath) as! FolderCollectionCell
            cell.initWithItem(vc: self, item: item)
            return cell
        }else if let item = items[indexPath.row] as? FileTO  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FileCollectionCell", for: indexPath) as! FileCollectionCell
            cell.initWithItem(vc: self, item: item)
            return cell
        }else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if items[indexPath.row] is FolderTO {
            if let folder = items[indexPath.row] as? FolderTO {
                onFolderClicked(vc: self, item: folder)
            }
        }else if items[indexPath.row] is FileTO  {
            if let file = items[indexPath.row] as? FileTO {
                onFileClicked(vc: self, fileId: file.fileId)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FilesMenuHeader", for: indexPath) as! FilesMenuHeader
            headerView.onMoreClickedClosure = {[weak self] in
                if let this = self {
                    showSortSheet(vc: this, onSortChangedListener: {[weak self] newSort in
                        self?.sort = newSort
                        self?.sortData()
                    })
                }
            }
            if sort == "alphabet" {
                headerView.sortTitle.text = "Name"
            }else if sort == "date" {
                headerView.sortTitle.text = "Date"
            }
            return headerView
        }else {
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.width - 60
        let itemWidth = totalWidth / 2
        return CGSize(width: itemWidth, height: itemWidth * 1.2)
    }
    
    var items: [Any] = Array()
    var sort = "date"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSearchController()
        initCollectionView()
        getOnlineData()
        NotificationCenter.default.addObserver(self, selector: #selector(getOnlineData), name: NotificationNames.globalUpdate, object: nil)
        
    }
    
    
    @objc private func getOnlineData(){
        if items.count == 0 {
            collectionView.showLoading()
        }
        collectionView.hideTag(tag: EMPTY_LAYOUT_TAG)
        collectionView.hideTag(tag: INTERNET_LAYOUT_TAG)
        ApiRootFilesAndFolders(onSuccess: {[weak self] (newItems) in
            self?.collectionView.hideLoading()
            self?.items.removeAll()
            self?.items.append(contentsOf: newItems)
            self?.collectionView.reloadData()
            self?.collectionView.refreshControl?.endRefreshing()
            self?.checkIfEmpty()
            }, onError: {[weak self] (message) in
                self?.collectionView.hideLoading()
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
                self?.collectionView.refreshControl?.endRefreshing()
            }, onConnectionError: {[weak self] in
                self?.collectionView.hideLoading()
                self?.collectionView.refreshControl?.endRefreshing()
                self?.showInternetLayout()
            }, onInvalidToken: {[weak self] in
                self?.collectionView.hideLoading()
                self?.collectionView.refreshControl?.endRefreshing()
                if let this = self {
                    logoutUser(vc: this)
                }
        }).call()
    }
    
    fileprivate func sortData(){
        items = sortFilesAndFoldersList(items: items, sort: sort)
        collectionView.reloadData()
    }
    fileprivate func showInternetLayout(){
          if items.count == 0 {
              collectionView.showNib(nibName: INTERNET_LAYOUT_NAME, tag: INTERNET_LAYOUT_TAG)
          }
      }
      fileprivate func checkIfEmpty(){
          if items.count == 0 {
              collectionView.showNib(nibName: EMPTY_LAYOUT_NAME, tag: EMPTY_LAYOUT_TAG)
          }else {
              collectionView.hideTag(tag: EMPTY_LAYOUT_TAG)
          }
      }
      
    
    private func initSearchController(){
        
        definesPresentationContext = true
        
        let resultsController = SearchViewController()
        
        
        let searchController = UISearchController(searchResultsController: resultsController)
        searchController.searchResultsUpdater = resultsController
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = true
        
        searchController.searchBar.placeholder = "Search your files"
        if #available(iOS 13.0, *) {
            searchController.searchBar.searchTextField.font = FontKit.roundedFont(ofSize: 14, weight: .medium)
        } else {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
        navigationItem.searchController = searchController
        
    }
    
    private func initCollectionView(){
        
        collectionView.register(UINib(nibName: "FolderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "FolderCollectionCell")
        collectionView.register(UINib(nibName: "FileCollectionCell", bundle: nil), forCellWithReuseIdentifier: "FileCollectionCell")
        collectionView.register(UINib(nibName: "FilesMenuHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FilesMenuHeader")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = 20
        flowLayout.minimumLineSpacing = 24
        flowLayout.scrollDirection = .vertical
        
        flowLayout.sectionInset = UIEdgeInsets(top: 12, left: 20, bottom: 20, right: 20)
        
        
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.tintColor = UIColor(named: "colorTextPrimary")
        collectionView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
    }
    
    @objc func onRefresh(){
        getOnlineData()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

func sortFilesAndFoldersList(items: [Any], sort: String) -> [Any]{
    if sort == "alphabet" {
        return items.sorted { (itemOne, itemTwo) -> Bool in
            var titleOne = ""
            var titleTwo = ""
            if itemOne is FileTO {
                titleOne = (itemOne as! FileTO).fileName
            }else if itemOne is FolderTO {
                titleOne = (itemOne as! FolderTO).folderName
            }
            if itemTwo is FileTO {
                titleTwo = (itemTwo as! FileTO).fileName
            }else if itemTwo is FolderTO {
                titleTwo = (itemTwo as! FolderTO).folderName
            }
            return titleOne < titleTwo
        }
        
    }else {
    
        return items.sorted { (itemOne, itemTwo) -> Bool in
            var dateOne : Int64 = 0
            var dateTwo : Int64 = 0
            if itemOne is FileTO {
                dateOne = (itemOne as! FileTO).modifiedAt
            }else if itemOne is FolderTO {
                dateOne = (itemOne as! FolderTO).modifiedAt
            }
            if itemTwo is FileTO {
                dateTwo = (itemTwo as! FileTO).modifiedAt
            }else if itemTwo is FolderTO {
                dateTwo = (itemTwo as! FolderTO).modifiedAt
            }
            
            let d1 = Date(timeIntervalSince1970: TimeInterval(dateOne))
            let d2 = Date(timeIntervalSince1970: TimeInterval(dateTwo))

            return d1.compare(d2) == .orderedDescending
        }
    }
}
