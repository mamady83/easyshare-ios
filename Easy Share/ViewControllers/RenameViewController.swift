//
//  RenameViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class RenameViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    var item: Any?
    @IBOutlet weak var edittext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let file = item as? FileTO {
            edittext.text = file.fileName
        }else if let folder = item as? FolderTO {
            edittext.text = folder.folderName
        }
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if edittext.text == "" {
            return
        }
        edittext.endEditing(true)
        if let folder = item as? FolderTO {
            submitBtn.showLoading()
            ApiFolderUpdate(onSuccess: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    this.dismiss(animated: true) {
                        showSwiftyMessage(
                            title: "Updated Successfully",
                            content: "Your folder has been renamed",
                            theme: .success
                        )
                    }
                }
            }, onError: {[weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(
                    title: "There was an error!",
                    content: message,
                    theme: .error
                )
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }).call(
                folderId: folder.folderId,
                folderName: edittext.text ?? ""
            )
        }else if let file = item as? FileTO {
            
            submitBtn.showLoading()
            ApiFileUpdate(onSuccess: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    this.dismiss(animated: true) {
                        showSwiftyMessage(
                            title: "Updated Successfully",
                            content: "Your file has been renamed",
                            theme: .success
                        )
                    }
                }
            }, onError: {[weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(
                    title: "There was an error!",
                    content: message,
                    theme: .error
                )
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }).call(
                fileId: file.fileId,
                fileName: edittext.text ?? "",
                folderId: file.folderId
            )
        }
        
    }
    
    
}
