//
//  ForgetVerifyViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/22/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform

class ForgetVerifyViewController: BaseViewController {
    
    var email = ""
    @IBOutlet weak var verifyET: UITextField!
    @IBOutlet weak var passwordET: UITextField!
    @IBOutlet weak var passwordRepeatET: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    @IBAction func submitClicked(_ sender: Any) {
        if !validateForm() {
            return
        }
        submitBtn.showLoading()
        ApiDoForgetPassword(onSuccess: {[weak self] in
            self?.submitBtn.hideLoading()
            if let this = self {
                goToHome(source: this)
            }
            }, onError: { [weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
        }).call(
            email: email,
            token: verifyET.text ?? "",
            password: passwordET.text ?? "",
            password_confirmation: passwordRepeatET.text ?? ""
        )
    }
    
    fileprivate func validateForm() -> Bool {
        if verifyET.text == "" {
            showSwiftyMessage(title: "Verify Code Empty", content: "Fill in the verify code sent to your email", theme: .warning)
            return false
        }
        if passwordET.text == "" {
            showSwiftyMessage(title: "Password empty", content: "Please enter your new password", theme: .warning)
            return false
        }
        if passwordRepeatET.text == "" {
            showSwiftyMessage(title: "Password empty", content: "Please re-enter your password", theme: .warning)
            return false
        }
        if passwordET.text != passwordRepeatET.text {
            showSwiftyMessage(title: "Passwords mismatch", content: "The entered passwords do not match", theme: .warning)
            return false
        }
        return true
    }
}

