//
//  LoginViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/22/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform
import GoogleSignIn
import AuthenticationServices

class LoginViewController: BaseViewController, ASAuthorizationControllerDelegate {
    
    @IBOutlet weak var titleToGoogleConstraint: NSLayoutConstraint!
    @IBOutlet weak var appleIdContainer: UIView!
    @IBOutlet weak var globeContainer: UIView!
    private var isScaled = false
    fileprivate let globeView = {
        return GlobeView()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globeContainer.addSubview(globeView)
        globeView.frame = globeContainer.frame
        globeView.snp.makeConstraints { make -> Void in
            make.edges.equalTo(globeContainer.snp.edges)
        }
        
        if #available(iOS 13.0, *) {
            
        }else {
            appleIdContainer.isHidden = true
            titleToGoogleConstraint.constant = 12
        }
        GIDSignIn.sharedInstance()?.presentingViewController = self
        NotificationCenter.default.addObserver(self, selector: #selector(loggedInUsingGoogle(notification:)), name: Notification.Name("LoggedInGoogle"), object: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !isScaled {
            let sourceWidth = CGFloat(414)
            let targetWidth = globeContainer.bounds.width
            
            let scale = targetWidth / sourceWidth
            
            
            print("scale = \(scale)")
            globeContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            globeContainer.transform = globeContainer.transform.scaledBy(x: scale, y: scale)
            
            globeView.animateElements()
            
            
        }
        
    }
    @IBAction func googleSigninClicked(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func singInWithAppleClicked(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
            
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
    }
    
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            if let userIdentifier = String(data: appleIDCredential.identityToken ?? Data(), encoding: .utf8) {
                
                ApiAppleLogin(onSuccess: {[weak self] in
                    if let this = self {
                        goToHome(source: this)
                    }
                    }, onError: { (message) in
                        showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
                }, onConnectionError: {
                    showNoInternetMessage()
                }).call(
                    tokenId: userIdentifier,
                    firstname: appleIDCredential.fullName?.givenName ?? "",
                    lastname: appleIDCredential.fullName?.familyName ?? ""
                )
                
            }
            
        }
    }
    
    @objc func loggedInUsingGoogle(notification: NSNotification){
        var token = ""
        if let tokenStr = notification.userInfo?["token"] as? String {
            token = tokenStr
        }
        ApiGoogleLogin(onSuccess: {[weak self] in
            if let this = self {
                goToHome(source: this)
            }
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }).call(
            tokenId: token
        )
    }
    
    
}
