//
//  FolderViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class FolderViewController: BaseTableViewController {
    
    private let EMPTY_LAYOUT_TAG = 24872
    private let EMPTY_LAYOUT_NAME = "FolderEmptyLayout"
    private let INTERNET_LAYOUT_TAG = 82378
    private let INTERNET_LAYOUT_NAME = "InternetLayout"
    
    
    
    var folder: FolderTO?
    var files: [FileTO] = Array()
    var sort = "date"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "FolderHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "header")
        tableView.register(UINib(nibName: "FileCell", bundle: nil), forCellReuseIdentifier: "FileCell")
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor(named: "colorTextPrimary")
        tableView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
        onRefresh()
        initData()
        NotificationCenter.default.addObserver(self, selector: #selector(initData), name: NotificationNames.globalUpdate, object: nil)
    }
    
    @objc func initData(){
        folder = FolderDAO().getSingle(folderId: folder?.folderId ?? 0)
        files = FolderDAO().getFiles(folderId: folder?.folderId ?? 0)
        tableView.reloadData()
    }
    
    @objc func onRefresh(){
        if files.count == 0 {
            tableView.showLoading()
        }
        tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        tableView.hideTag(tag: INTERNET_LAYOUT_TAG)
        ApiFileList(
            onSuccess: {[weak self] (newFiles) in
                self?.tableView?.hideLoading()
                self?.tableView?.refreshControl?.endRefreshing()
                self?.files = newFiles
                self?.tableView.reloadData()
                self?.checkIfEmpty()
            }, onError: {[weak self] (message) in
                self?.tableView?.hideLoading()
                self?.tableView?.refreshControl?.endRefreshing()
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.tableView?.hideLoading()
                self?.tableView?.refreshControl?.endRefreshing()
                self?.showInternetLayout()
            }, onInvalidToken: {[weak self] in
                self?.tableView?.hideLoading()
                self?.tableView?.refreshControl?.endRefreshing()
                if let this = self {
                    logoutUser(vc: this)
                }
            }
        ).call(
            folderId: folder?.folderId ?? 0,
            page: 1,
            count: 50,
            sort: sort
        )
    }
    
    fileprivate func showInternetLayout(){
        if files.count == 0 {
            tableView.showNib(nibName: INTERNET_LAYOUT_NAME, tag: INTERNET_LAYOUT_TAG)
            tableView.setViewBGbyTag(tag: INTERNET_LAYOUT_TAG, bgColor: UIColor(named: "colorBackground"))
        }
    }
    fileprivate func checkIfEmpty(){
        if files.count == 0 {
            tableView.showNib(nibName: EMPTY_LAYOUT_NAME, tag: EMPTY_LAYOUT_TAG)
            tableView.setViewBGbyTag(tag: EMPTY_LAYOUT_TAG, bgColor: UIColor(named: "colorBackground"))
        }else {
            tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        }
    }
    
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        if let f = folder {
            showFolderSheet(vc: self, folder: f)
        }
    }
}



extension FolderViewController {
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as! FileCell
        cell.initWithItem(vc: self, item: files[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onFileClicked(vc: self, fileId: files[indexPath.row].fileId)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! FolderHeader
        cell.folderNameLable.text = folder?.folderName ?? ""
        if sort == "date" {
            cell.sortLable.text = "Date"
        }else if sort == "alphabet" {
            cell.sortLable.text = "Name"
        }
        cell.onSortClicked = {[weak self] in
            if let this = self {
                showSortSheet(vc: this) { (newSort) in
                    self?.sort = newSort
                    self?.tableView?.reloadData()
                    self?.onRefresh()
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(
            style: .destructive,
            title: "",
            handler: {[weak self] (action, view, completion) in
                
                if let this = self {
                    showDeleteSheet(vc: this, item: this.files[indexPath.row])
                }
                completion(false)
                
        })
        if let cgImageX =  UIImage(named: "m_row_delete")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor(named: "colorBackground")
        
        let configuration = UISwipeActionsConfiguration(actions: [action])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}
