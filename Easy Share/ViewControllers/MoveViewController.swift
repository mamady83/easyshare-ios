//
//  MoveViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/15/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

public let MOVE_TYPE_MOVE = 1
public let MOVE_TYPE_CHOOSE_FOLDER = 2

class MoveViewController: BaseViewController {
    
    @IBOutlet weak var moveBtn: RoundedButton!
    public var moveType = 0
    var items : [Any] = Array()
    var onFolderSelected: ((FolderTO)->())? = nil
    var selectedItem : FolderTO? = nil
    var file: FileTO? = nil
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "CreateFolderCell", bundle: nil), forCellReuseIdentifier: "CreateFolderCell")
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "ChooseFolderCell", bundle: nil), forCellReuseIdentifier: "ChooseFolderCell")
        
        
        foldersUpdated()
        NotificationCenter.default.addObserver(self, selector: #selector(foldersUpdated), name: NotificationNames.globalUpdate, object: nil)
        
    }
    
    @objc func foldersUpdated(){
        items.removeAll()
        items = FolderDAO().all()
        print("folders \(items.count)")
        if moveType == MOVE_TYPE_CHOOSE_FOLDER {
            items.insert(HomeTO(type: HomeCellType.HEADER, item: "Choose Folder"), at: 0)
            moveBtn.isHidden = true
        }else if moveType == MOVE_TYPE_MOVE {
            items.insert(HomeTO(type: HomeCellType.HEADER, item: "Move to"), at: 0)
            moveBtn.setTitle("Move", for: .normal)
        }
        let rootItemsCont = FolderDAO().getFiles(folderId: 0).count
        items.append(FolderTO(folderName: "SweetShare", folderId: 0, filesCount: rootItemsCont, hasShared: false, modifiedAt: 0))

        items.append("Create Folder")
        tableView.reloadData()
        
    }
    
    @IBAction func moveBtnClicked(_ sender: Any) {
        if let item = selectedItem {
            moveFile(folder: item)
        }
        
        
    }
    
    fileprivate func moveFile(folder: FolderTO){
        guard let mFile = file else {
            return
        }
        ApiFileUpdate(onSuccess: { [weak self] in
            self?.dismiss(animated: true, completion: {
                showSwiftyMessage(
                    title: "Moved Successfully",
                    content: "your file has been moved",
                    theme: .success
                )
            })
            EventDispatcher.shared().sendGlobalUpdateEvent()
        }, onError: {(message) in
            showSwiftyMessage(
                title: "There was an error!",
                content: message,
                theme: .error
            )
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            fileId: mFile.fileId,
            fileName: mFile.fileName,
            folderId: folder.folderId
        )
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension MoveViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let item = items[indexPath.row] as? HomeTO {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            cell.initWithItem(item: item.item)
            return cell
        }else if let item = items[indexPath.row] as? FolderTO {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseFolderCell", for: indexPath) as! ChooseFolderCell
            cell.initWithItem(vc: self, item: item)
            if selectedItem == item {
                cell.checkIcon.isHidden = false
            }else {
                cell.checkIcon.isHidden = true
            }
            return cell
        }else if let item = items[indexPath.row] as? String, item == "Create Folder" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateFolderCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = items[indexPath.row] as? FolderTO {
            selectedItem = item
            tableView.reloadData()
            if let item = selectedItem, moveType == MOVE_TYPE_CHOOSE_FOLDER {
                onFolderSelected?(item)
                dismiss(animated: true, completion: nil)
            }
        }else if let item = items[indexPath.row] as? String, item == "Create Folder" {
            onCreateFolderClicked(vc: self)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if items[indexPath.row] is HomeTO {
            return 28
        }else if items[indexPath.row] is FolderTO {
            return 72
        }else if let item = items[indexPath.row] as? String, item == "Create Folder" {
            return 70
        }else {
            return 100
        }
        
    }
    
    
}
