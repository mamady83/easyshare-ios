//
//  OnboardingViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import TweenKit
import Domain

class OnboardingViewController: UIViewController {

    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var collectionViewContainer: UIView!
    fileprivate var actionScrubber: ActionScrubber?
    fileprivate var hasAppeared = false
    let pageCount = 3
    fileprivate let onboardingView: OnboardingView = {
        return OnboardingView()
    }()

    fileprivate var items :[OnboardingTO] = Array()
    
    
    fileprivate let collectionView: UICollectionView = {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        collectionView.register(OnboardingCollectionCell.self, forCellWithReuseIdentifier: "OnboardingCollectionCell")
        
        collectionViewContainer.addSubview(collectionView)
    
        collectionViewContainer.addSubview(onboardingView)

        items.append(OnboardingTO(title: "Password Protection", subtitle: "only those with a password\nare worthy to see your files"))
        items.append(OnboardingTO(title: "Expiration Date", subtitle: "equip your files\nwith a self-destruct button"))
        items.append(OnboardingTO(title: "Easy Sharing", subtitle: "share anything, fast!"))

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.bounces = false
        collectionView.reloadData()
    }
    
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        goToLogin()
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if nextBtn.titleLabel?.text == "Start" {
            goToLogin()
        }else {
            print("current page \(pageControll.currentPage)")
            collectionView.isPagingEnabled = false
            collectionView.scrollToItem(at: IndexPath(row: pageControll.currentPage + 1, section: 0), at: .centeredHorizontally, animated: true)
            collectionView.isPagingEnabled = true
        }
    }
    
    fileprivate func goToLogin(){
        LocalStorage.shared().passedOnboarding = true
        if let vc = storyboard?.instantiateViewController(withIdentifier: "loginVC") as? LoginViewController {
            if #available(iOS 13.0, *) {
                vc.isModalInPresentation = false
            }
            vc.isModalInPopover = false
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionView.frame = collectionViewContainer.bounds
        onboardingView.frame = collectionViewContainer.bounds

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !hasAppeared {
            actionScrubber = ActionScrubber(action: buildAction())
            actionScrubber?.clampTValuesAboveOne = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hasAppeared = true
    }
    
    // MARK: - Build Action
    
    fileprivate func buildAction() -> FiniteTimeAction {
        
        let stepOneAction = InterpolationAction(from: 0.0, to: 1.0, duration: 1.0, easing: .linear) {
            [unowned self] in
            self.onboardingView.updateStepOne(t: $0)
        }
        let stepTwoAction = InterpolationAction(from: 0.0, to: 1.0, duration: 1.0, easing: .linear) {
            [unowned self] in
            self.onboardingView.updateStepTwo(t: $0)
        }
 
        
        return ActionGroup(actions:
            [stepOneAction,
             ActionSequence(actions: DelayAction(duration: 1.0), stepTwoAction)]
        )
    }
    
}



extension OnboardingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.x
        
        let pageOffset = offset / view.bounds.size.width
        actionScrubber?.update(elapsedTime: Double(pageOffset))
        
        var currentPage = Int(pageOffset + 0.5)
        currentPage = max(currentPage, 0)
        currentPage = min(currentPage, pageCount-1)
        pageControll.currentPage = currentPage
        
        
        if currentPage == 2 {
            nextBtn.setTitle("Start", for: .normal)
        }else {
            nextBtn.setTitle("Next", for: .normal)
        }
    }
}

extension OnboardingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pageCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCollectionCell", for: indexPath) as! OnboardingCollectionCell
        cell.initWithItem(item: items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return view.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
