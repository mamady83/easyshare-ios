//
//  ShareViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NetworkPlatform
import Domain

class ShareViewController: UIViewController {

    
    var url: URL?
    let progress = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballTrianglePath, color: UIColor(named: "colorTextPrimary"), padding: 0)
    let textfield = UITextField(frame: CGRect(x: 20, y: 0, width: 100, height: 100))
    var file: FileTO?
    let button = UIButton(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
    let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        getFile(password: "")
    }
    
    
    fileprivate func initView(){
        
        for v in view.subviews {
            v.removeFromSuperview()
        }
        
        view.backgroundColor = UIColor(named: "colorBackground")
        
        let backBtn = UIButton(frame: CGRect(x: 20, y: 50, width: 54, height: 54))
        backBtn.setImage(UIImage(named: "m_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(onBackPressed), for: .touchUpInside)
        view.addSubview(backBtn)

        let line = UIView(frame: CGRect(x: 0, y: 112, width: view.frame.width, height: 1))
        line.backgroundColor = UIColor(named: "colorDivider")
        view.addSubview(line)
        
        view.addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.center.equalTo(view.snp.center)
        }
        
    }
    
    
    fileprivate func getFile(password: String){
        progress.startAnimating()
        ApiShareFilesShow(onSuccess: {[weak self]  (newfile) in
            self?.progress.stopAnimating()
            self?.file = FileDAO().getSingle(fileId: newfile.fileId)
            self?.initFileLayout()
        }, onError: {[weak self] (message) in
            self?.progress.stopAnimating()
            if message.lowercased().contains("password") {
                self?.showPasswordLayout()
            }else if message.lowercased().contains("expire") {
                self?.showExpiredLayout()
            }else {
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }
        }, onConnectionError: {[weak self] in
            self?.progress.stopAnimating()
            showNoInternetMessage()
        }, onInvalidToken: {
        }).call(
            hash: url?.lastPathComponent ?? "",
            password: password
        )
    }
    
    fileprivate func initFileLayout(){
        guard let file = self.file else {
            return
        }
        initView()
        
        switch file.fileType {
        case FileType.PHOTO:
            image.image = UIImage(named: "m_image_placeholder")
            image.backgroundColor = UIColor(named: "colorImageOpaque")
            
        case FileType.VIDEO:
            image.image = UIImage(named: "m_video_placeholder")
            image.backgroundColor = UIColor(named: "colorVideoOpaque")
            
        case FileType.MUSIC:
            image.image = UIImage(named: "m_music_placeholder")
            image.backgroundColor = UIColor(named: "colorMusicOpaque")
            
        default:
            image.image = UIImage(named: "m_file_placeholder")
            image.backgroundColor = UIColor(named: "colorFileOpaque")
        }
        image.layer.cornerRadius = 10
        view.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left).offset(40)
            make.right.equalTo(view.snp.right).offset(-40)
            make.top.equalTo(view.snp.top).offset(155)
            make.width.equalTo(view.frame.width - 80)
            make.height.equalTo(view.frame.width - 80)
        }
        let lable = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        lable.font = FontKit.roundedFont(ofSize: 20, weight: .bold)
        lable.textColor = UIColor(named: "colorTextPrimary")
        lable.text = file.fileName
        view.addSubview(lable)
        lable.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left).offset(40)
            make.top.equalTo(image.snp.bottom).offset(20)
            make.right.equalTo(view.snp.right).offset(-20)
            make.height.equalTo(30)
        }
        
        let subtitle = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        subtitle.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        subtitle.textColor = UIColor(named: "colorTextSecondary")
        let fileSize = Units(bytes: Int64(file.fileSize)).getReadableUnit()
        subtitle.text = fileSize
        view.addSubview(subtitle)
        subtitle.snp.makeConstraints { (make) in
            make.leading.equalTo(lable.snp.leading)
            make.trailing.equalTo(lable.snp.trailing)
            make.top.equalTo(lable.snp.bottom)
            make.height.equalTo(20)
        }
        button.setTitle("Download", for: .normal)
        button.titleLabel?.font = FontKit.roundedFont(ofSize: 18, weight: .bold)
        button.setTitleColor(UIColor(named: "colorBlue"), for: .normal)
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.equalTo(subtitle.snp.bottom).offset(12)
            make.width.equalTo(100)
            make.centerX.equalTo(view.snp.centerX)
            make.height.equalTo(40)
        }
        button.addTarget(self, action: #selector(downloadBtnClicked), for: .touchUpInside)
        let url = URL(fileURLWithPath: file.filePathOffline)
        if file.filePathOffline != "", FileManager.default.fileExists(atPath: url.path) {
            button.setTitle("Export", for: .normal)
            showPreview()
        }
    }
    
    @objc func downloadBtnClicked(){
        guard let file = FileDAO().getSingle(fileId: file?.fileId ?? 0) else {
            return
        }
        
        let url = URL(fileURLWithPath: file.filePathOffline)
        if file.filePathOffline != "", FileManager.default.fileExists(atPath: url.path) {
            shareFile(vc: self, path: file.filePathOffline)
        }else {
            if let dlVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "downloadVC") as? DownloadViewController {
                if #available(iOS 13.0, *) {
                    dlVC.isModalInPresentation = false
                }
                dlVC.file = file
                dlVC.isModalInPopover = false
                dlVC.modalTransitionStyle = .crossDissolve
                dlVC.modalPresentationStyle = .overFullScreen
                dlVC.onDownloadFinished = {[weak self] in
                    self?.button.setTitle("Export", for: .normal)
                    self?.showPreview()
                }
                present(dlVC, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func showPreview(){
        guard let file = FileDAO().getSingle(fileId: file?.fileId ?? 0) else {
            return
        }
        let previewVC = PreviewViewController()
        previewVC.file = file
        addChild(previewVC)
        previewVC.view.translatesAutoresizingMaskIntoConstraints = false
        image.addSubview(previewVC.view)
        previewVC.view.snp.makeConstraints { (make) in
            make.edges.equalTo(image.snp.edges)
        }
        previewVC.didMove(toParent: self)
    }
    
    fileprivate func showExpiredLayout(){
        progress.stopAnimating()
        let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 206, height: 259))
        image.image = UIImage(named: "expired_image")
        view.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.width.equalTo(206)
            make.height.equalTo(259)
            make.top.equalTo(view.snp.top).offset(200)
            make.centerX.equalTo(view.snp.centerX)
        }
        
        let lable = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        lable.font = FontKit.roundedFont(ofSize: 14, weight: .bold)
        lable.textColor = UIColor(named: "colorTextInactive")
        lable.text = "This link is expired\nand is no longer available"
        lable.textAlignment = .center
        lable.numberOfLines = 2
        view.addSubview(lable)
        lable.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(20)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
        }
    }
    
    
    
    fileprivate func showPasswordLayout(){
        progress.stopAnimating()
        
        let image = UIImageView(frame: CGRect(x: 0, y: 0, width: 136, height: 159))
        image.image = UIImage(named: "shared_files_empty_image")
        view.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.width.equalTo(136)
            make.height.equalTo(159)
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(view.snp.top).offset(140)
        }
        
        let title = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30))
        title.text = "This file is locked"
        title.font = FontKit.roundedFont(ofSize: 20, weight: .bold)
        title.textColor = UIColor(named: "colorTextPrimary")
        title.textAlignment = .center
        view.addSubview(title)
        title.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(12)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.height.equalTo(30)
        }
        
        let subtitle = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        subtitle.text = "Enter the file's password to preview it"
        subtitle.font = FontKit.roundedFont(ofSize: 14, weight: .medium)
        subtitle.textColor = UIColor(named: "colorTextPrimary")
        subtitle.textAlignment = .center
        view.addSubview(subtitle)
        subtitle.snp.makeConstraints { (make) in
            make.top.equalTo(title.snp.bottom)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.height.equalTo(20)
        }
        
        let container = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        container.backgroundColor = UIColor(named: "colorTextField")
        container.layer.cornerRadius = 10
        container.layer.borderColor = UIColor(named: "colorDivider")?.cgColor
        container.layer.borderWidth = 1
        view.addSubview(container)
        container.snp.makeConstraints { (make) in
            make.top.equalTo(subtitle.snp.bottom).offset(20)
            make.left.equalTo(view.snp.left).offset(20)
            make.right.equalTo(view.snp.right).offset(-20)
            make.height.equalTo(50)
        }
        
        textfield.placeholder = "Password"
        textfield.isSecureTextEntry = true
        textfield.textColor = UIColor(named: "colorTextPrimary")
        container.addSubview(textfield)
        textfield.snp.makeConstraints { (make) in
            make.left.equalTo(container.snp.left).offset(20)
            make.top.equalTo(container.snp.top)
            make.bottom.equalTo(container.snp.bottom)
            make.right.equalTo(container.snp.right).offset(-20)
        }
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        button.setTitle("Unlock", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(named: "colorAccent")
        button.titleLabel?.font = FontKit.roundedFont(ofSize: 16, weight: .bold)
        button.layer.cornerRadius = 10

        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left).offset(30)
            make.right.equalTo(view.snp.right).offset(-30)
            make.top.equalTo(container.snp.bottom).offset(10)
            make.height.equalTo(50)
        }
        button.addTarget(self, action: #selector(passwordSubmitClicked), for: .touchUpInside)
    }
    
    @objc func passwordSubmitClicked(){
        initView()
        getFile(password: textfield.text ?? "")
    }
    
    @objc func onBackPressed(){
        goToHome(source: self)
    }
}
