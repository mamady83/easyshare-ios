//
//  ChangePasswordViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/2/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var newPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateForm() {
            submitBtn.showLoading()
            ApiClientUpdate(onSuccess: {[weak self] in
                self?.submitBtn.hideLoading()
                self?.dismiss(animated: true) {
                    showSwiftyMessage(title: "Done!", content: "Password updated successfuly", theme: .success)
                }
            }, onError: { [weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an Error", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }).updatePassword(password: newPassword.text ?? "")
        }
    }
    fileprivate func validateForm() -> Bool{
        if newPassword.text == "" {
            showSwiftyMessage(title: "Password is empty!", content: "Please fill in your password", theme: .warning)
            return false
        }
        return true
    }


}
