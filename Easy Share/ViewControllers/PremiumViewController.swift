//
//  PremiumViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import StoreKit
import NVActivityIndicatorView
import Domain
import NetworkPlatform

class PremiumViewController: BaseTableViewController, SKProductsRequestDelegate {

    @IBOutlet weak var popularPlanTitle: RoundedLable!
    @IBOutlet weak var popularPlanSubtitle: RoundedLable!
    @IBOutlet weak var popularPlanGiftLable: RoundedLable!
    @IBOutlet weak var secondPlanTitle: RoundedLable!
    @IBOutlet weak var secondPlanSubtitle: RoundedLable!
    
  
    var request: SKProductsRequest!
    var products = [SKProduct]()
    var items: [ProductTO] = Array()
    
    var is_popular_selected = false
    
    @IBOutlet weak var popular_plan_container: UIView!
    @IBOutlet weak var popular_lable: RoundedLable!
    @IBOutlet weak var popular_check: UIImageView!
    @IBOutlet weak var gift_lable: RoundedLable!
    @IBOutlet weak var gift_icon: UIImageView!
    
    let loadingLayout = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
    let loading = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40), type: .ballTrianglePath, color: UIColor(named: "colorTextPrimary"), padding: 0)

    @IBOutlet weak var second_package_check: UIImageView!
    @IBOutlet weak var second_package_container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshData()
    }
    
    func initLoadingLayout(){
        view.addSubview(loadingLayout)
        loadingLayout.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left)
            make.width.equalTo(view.frame.width)
            make.height.equalTo(view.frame.height - 80)
            make.right.equalTo(view.snp.right)
            make.top.equalTo(view.snp.top).offset(80)
            make.bottom.equalTo(view.snp.bottom)
        }
        loadingLayout.backgroundColor = UIColor(named: "colorBackground")
        loadingLayout.isUserInteractionEnabled = true
        loadingLayout.addSubview(loading)
        loading.snp.makeConstraints { (make) in
            make.center.equalTo(loadingLayout.snp.center)
        }
        loading.startAnimating()
    }
    
    func refreshData(){
        
        initLoadingLayout()
        ApiProducts(onSuccess: {[weak self] in
            self?.items = ProductDAO().all()
            self?.getProductsFromApple()
        }, onError: { [weak self] (message) in
            self?.loading.stopAnimating()
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {[weak self] in
            self?.loading.stopAnimating()
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            self?.loading.stopAnimating()
            if let this = self {
                logoutUser(vc: this)
            }
        }).call()
    }
    
    func getProductsFromApple() {
        if items.isEmpty {
            dismiss(animated: true, completion: nil)
            return
        }
        var identifiers : [String] = Array()
        for i in items {
            identifiers.append(i.apple_product_id)
        }
        let productIdentifiers = Set(identifiers)
        request = SKProductsRequest(productIdentifiers: productIdentifiers)
        request.delegate = self
        request.start()
    }
    
    
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count == 2 && items.count == 2{
            DispatchQueue.main.async { [weak self] in
                self?.products = response.products
                self?.loadingLayout.isHidden = true
                self?.loading.stopAnimating()
                self?.loadProductsIntoLayout()
            }
            
        }else {
            DispatchQueue.main.async { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    
    var popular_product: SKProduct? = nil
    var popular_plan: ProductTO? = nil
    var second_product: SKProduct? = nil
    var second_plan: ProductTO? = nil
    
    func loadProductsIntoLayout(){
        
        
        if items[0].storage > items[1].storage {
            popular_plan = items[0]
            second_plan = items[1]
        }else {
            popular_plan = items[1]
            second_plan = items[0]
        }
        
        for p in products {
            if p.productIdentifier == popular_plan?.apple_product_id {
                popular_product = p
            }else if p.productIdentifier == second_plan?.apple_product_id {
                second_product = p
            }
        }
        
        let formatter = NumberFormatter()
        formatter.locale = popular_product?.priceLocale
        formatter.numberStyle = .currency
       
        let popularPrice = formatter.string(from: popular_product?.price ?? 0) ?? ""
        let secondPackagePrice = formatter.string(from: second_product?.price ?? 0) ?? ""
        popularPlanTitle.text =  "\(popularPrice) / Month"
        secondPlanTitle.text = "\(secondPackagePrice) / Month"

        let popualrGiftStorage = (popular_plan?.storage ?? 0) / 3
        let popularRealStorage = (popular_plan?.storage ?? 0) - popualrGiftStorage
        print("storage = \(popular_plan?.storage ?? 0)")
        print("gift storage = \(popualrGiftStorage)")
        print("real storage = \(popularRealStorage)")
        
        popularPlanSubtitle.text = "( For \(Units(bytes: Int64(popularRealStorage)).getReadableUnit()) Storage )"
        popularPlanGiftLable.text = "+\(Units(bytes: Int64(popualrGiftStorage)).getReadableUnit())"
        secondPlanSubtitle.text = "( For \(Units(bytes: Int64(second_plan?.storage ?? 0)).getReadableUnit()) Storage )"
        
        selectPopular()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 4:
            selectPopular()
        case 5:
            selectSecondPackage()
        case 6:
            continueButtonClicked()
        default:
            print("")
        }
    }
    
    
    fileprivate func selectPopular(){
        popular_plan_container.layer.borderColor = UIColor(named: "colorAccent")?.cgColor
        popular_check.image = UIImage(named: "m_check_accent")
        gift_icon.isHidden = false
        gift_lable.isHidden = false
        second_package_check.image = UIImage(named: "m_check_accent_inactive")
        second_package_container.layer.borderColor = UIColor.clear.cgColor
        popular_lable.backgroundColor = UIColor(named: "colorAccent")
        popular_lable.textColor = .white
        
        is_popular_selected = true
    }
    
    fileprivate func selectSecondPackage(){
        popular_plan_container.layer.borderColor = UIColor.clear.cgColor
        popular_lable.backgroundColor = UIColor(named: "colorHeaderBackground")
        popular_lable.textColor = UIColor(named: "colorTextInactive")
        popular_check.image = UIImage(named: "m_check_accent_inactive")
        gift_icon.isHidden = true
        gift_lable.isHidden = true
        second_package_check.image = UIImage(named: "m_check_accent")
        second_package_container.layer.borderColor = UIColor(named: "colorAccent")?.cgColor
        
        is_popular_selected = false
    }
    
    fileprivate func continueButtonClicked(){

        guard let popular_product = self.popular_product else {
            return
        }
        guard let second_product = self.second_product else {
            return
        }
        if is_popular_selected {
            startPurchase(product: popular_product)
        }else {
            startPurchase(product: second_product)
        }
        
    }
    
    @IBAction func onCloseBtnClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    fileprivate func startPurchase(product: SKProduct){
        let payment = SKMutablePayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    @IBAction func restorePurchaseClicked(_ sender: Any) {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {


            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
              //  print(receiptData)


                let receiptString = receiptData.base64EncodedString(options: [])

                ApiVerifyApplePurchase(onSuccess: {
                    showSwiftyMessage(title: "Purchase Restored Successfully", content: "", theme: .success)
                }, onError: { (message) in
                    showSwiftyMessage(title: "Purchase Restored Successfully", content: "", theme: .success)
                }, onConnectionError: {

                    showNoInternetMessage()
                }, onInvalidToken: {
                    showNoInternetMessage()

                }).call(reciept: receiptString)
                // Read receiptData
            }
            catch { print("Couldn't read receipt data with error: " + error.localizedDescription) }
        }else {
            print("appStoreReceiptURL empty")
        }
    }
    
    
    
}
