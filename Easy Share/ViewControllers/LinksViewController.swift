//
//  LinksViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/24/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform


class LinksViewController: BaseViewController {
    
    public var fileId: Int = 0
    
    var items : [LinkTO] = Array()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "LinkCell", bundle: nil), forCellReuseIdentifier: "LinkCell")
        tableView.register(UINib(nibName: "CreateFolderCell", bundle: nil), forCellReuseIdentifier: "CreateFolderCell")
        tableView.tableFooterView = UIView()
        
        initData()
        refreshData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(globalUpdate), name: NotificationNames.globalUpdate, object: nil)
    }
    
    @objc func globalUpdate(){
        initData()
    }
    fileprivate func initData(){
        items = LinkDAO().getFileLinks(fileId: fileId)
        items.append(LinkTO(linkId: 0, folderId: 0, fileId: 0, url: "", expires_at: 0, isPasswordProtected: false))
        tableView.reloadData()
    }
    
    fileprivate func refreshData(){
        ApiShareFilesList(onSuccess: { [weak self] (newLinks) in
            self?.items = newLinks
            self?.items.append(LinkTO(linkId: 0, folderId: 0, fileId: 0, url: "", expires_at: 0, isPasswordProtected: false))
            self?.tableView.reloadData()
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            file_id: fileId,
            page: 1,
            count: 50
        )
    }
    
    fileprivate func deleteLink(linkId: Int){
        ApiShareFilesDelete(onSuccess: {[weak self] in
            self?.initData()
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            linkId: linkId
        )
    }
}

extension LinksViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == items.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateFolderCell", for: indexPath) as! CreateFolderCell
            cell.createFolderBtn.setTitle("Create Link", for: .normal)
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "LinkCell", for: indexPath) as! LinkCell
        
        cell.initWithItem(link: items[indexPath.row])
        cell.copyClicked = {[weak self] in
            let pasteboard = UIPasteboard.general
            pasteboard.string = self?.items[indexPath.row].url ?? ""
            showSwiftyMessage(title: "Link Copied!", content: "Share link copied to clipboard", theme: .success)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1 {
            if let file = FileDAO().getSingle(fileId: fileId) {
                onCreateLinkClicked(vc: self, file: file)
            }
        }else {
            onLinkClicked(vc: self, link: items[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row != items.count - 1 {
            return true
        }else {
            return false
        }
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(
            style: .destructive,
            title: "",
            handler: {[weak self] (action, view, completion) in
                self?.deleteLink(linkId: self?.items[indexPath.row].linkId ?? 0)
                completion(false)
                
        })
        if let cgImageX =  UIImage(named: "m_row_delete")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor(named: "colorBackground")
        
        let configuration = UISwipeActionsConfiguration(actions: [action])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}
