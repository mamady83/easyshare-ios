//
//  CreateFolderViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/10/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import SwiftMessages
import Domain

class CreateFolderViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var folderNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if !validateForm() {
            return
        }
        view.endEditing(true)
        submitBtn.showLoading()
        ApiFolderCreate(
            onSuccess: { [weak self] in
                self?.submitBtn.hideLoading()
                EventDispatcher.shared().sendGlobalUpdateEvent()
                if let this = self {
                    this.dismiss(animated: true) {
                        showSwiftyMessage(
                            title: "Folder Created",
                            content: "You can now move your files to this folder",
                            theme: .success
                        )
                    }
                }
            }, onError: { [weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }
        ).call(folderName: folderNameTextField.text ?? "")
        
    }
    
    fileprivate func validateForm() -> Bool{
        if folderNameTextField.text == "" {
            return false
        }
        return true
    }
}
