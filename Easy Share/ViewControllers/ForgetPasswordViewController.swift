//
//  ForgetPasswordViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform

class ForgetPasswordViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func onSubmitClicked(_ sender: Any) {
        if !validateForm() {
            return
        }
        submitBtn.showLoading()
        ApiSendForgotPasswordToken(
            onSuccess: { [weak self] in
                self?.submitBtn.hideLoading()
                self?.goToVerify()
            }, onError: { [weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: { [weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }
        ).call(
            email: email.text ?? ""
        )
    }
    
    fileprivate func goToVerify(){
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "forgetVerifyVC") as? ForgetVerifyViewController {
            vc.email = email.text ?? ""
            present(vc, animated: true, completion: nil)
        }
    }
    
    fileprivate func validateForm()  -> Bool{
        if email.text == "" {
            showSwiftyMessage(title: "Please enter your email", content: "Email fields can not be empty", theme: .warning)
            return false
        }
        if email.text?.isValidEmail() == false {
            showSwiftyMessage(title: "Invalid Email", content: "The entered email is not valid", theme: .warning)
            return false
        }
        return true
    }
    
}
