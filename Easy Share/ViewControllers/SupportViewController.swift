//
//  SupportViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import UIKit
import Domain
import NetworkPlatform
import MessengerKit
import Kingfisher

struct User: MSGUser {
    
    var displayName: String
    
    var avatar: UIImage?
        
    var isSender: Bool
    
}


class SupportViewController: MSGMessengerViewController {
    
    
    // Users in the chat
    
    
    var me = User(displayName: LocalStorage.shared().firstName, avatar: #imageLiteral(resourceName: "m_avatar_placeholder"), isSender: true)
    
    let admin = User(displayName: "Admin", avatar: #imageLiteral(resourceName: "m_support"), isSender: false)
    
    // Messages
    
    lazy var messages: [[MSGMessage]] = {
        return [
            
        ]
    }()
    
    // Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        setNavBarAppearance()
        refreshData()
        
        shouldScrollToBottom = true
        collectionView.showsVerticalScrollIndicator = false
        if ImageCache.default.isCached(forKey:"avatar") {

             ImageCache.default.retrieveImage(forKey: "avatar") { [weak self] result in
            switch result {
            case .success(let value):

                self?.me.avatar = value.image
            case .failure(let error):
                print(error)
            }
        }
        }
    }
    
    override var style: MSGMessengerStyle {
        var style = MessengerKit.Styles.travamigos
        style.inputPlaceholder = "Send a message"
        style.backgroundColor = UIColor(named: "colorBackground")!
        style.inputViewBackgroundColor = UIColor(named: "colorBackground")!
        return style
    }
    
    func setNavBarAppearance() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]
            navigationController?.navigationBar.standardAppearance = appearance
        } else {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]
        }
    }
    
    override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {
        if inputView.message != "" {
            ApiSupportSend(onSuccess: { [weak self] (newItems) in
                self?.initData(newItems: newItems)
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {
                showNoInternetMessage()
            }, onInvalidToken: {
                
            }).call(message: inputView.message)
        }
    }
    
    func initData(newItems: [SupportTO]){
        messages.removeAll()
        messages.append(
            [
                MSGMessage(id: 0, body: .text("Hello \(LocalStorage.shared().firstName)\nHow can I help you?"), user: admin, sentAt: Date())
            ]
        )
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        for i in newItems {
            messages.append([
                MSGMessage(id: 0, body: .text(i.message), user: (i.isAdmin ? admin : me), sentAt: dateFormatter.date(from: i.date) ?? Date())
            ])
        }
        collectionView.reloadData()
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.scrollToBottom(animated: true)
        }
        
    }
    
    
    func refreshData(){
        ApiSupportList(onSuccess: { [weak self] (newItems) in
            self?.initData(newItems: newItems)
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {
            showNoInternetMessage()
        }, onInvalidToken: {
            
        }).call()
    }
}


// MARK: - MSGDataSource

extension SupportViewController: MSGDataSource {
    
    func numberOfSections() -> Int {
        return messages.count
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[section].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[indexPath.section][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
        
        return messages[section].first?.sentAt.relativeTime
    }
    
    func headerTitle(for section: Int) -> String? {
        return ""
    }
    
    

}
