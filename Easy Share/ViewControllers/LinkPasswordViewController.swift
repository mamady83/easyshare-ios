//
//  LinkPasswordViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class LinkPasswordViewController: BaseViewController {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var passwordET: UITextField!
    var link: LinkTO?
    var onPasswordSet: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func submitBtnClicked(_ sender: Any) {
        if passwordET.text ?? "" == "" {
            return
        }

        setPasswrod(password: passwordET.text ?? "")
        
    }
    fileprivate func setPasswrod(password: String){
        submitBtn.showLoading()
        ApiShareFilesUpdate(onSuccess: {[weak self] in
            self?.submitBtn.hideLoading()
            self?.link = LinkDAO().getSingle(linkId: self?.link?.linkId ?? 0)
            self?.onPasswordSet?()
            self?.dismiss(animated: true, completion: nil)
        }, onError: { [weak self] (message) in
            self?.submitBtn.hideLoading()
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: {[weak self] in
            self?.submitBtn.hideLoading()
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            self?.submitBtn.hideLoading()
            if let this = self {
                logoutUser(vc: this)
            }
        }).call(
            linkId: link?.linkId ?? 0,
            password: password,
            expire_date: "",
            remove_password: false,
            remove_expire: false
        )
    }

}
