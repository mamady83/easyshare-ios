//
//  VerifyViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import Domain

class VerifyViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var code: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateForm() {
            submitBtn.showLoading()
            ApiDoVerify(onSuccess: {[weak self] in
                self?.submitBtn.hideLoading()
                self?.goToRegister()
                }, onError: {[weak self] (message) in
                    self?.submitBtn.hideLoading()
                    showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
                }, onConnectionError: {[weak self] in
                    self?.submitBtn.hideLoading()
                    showNoInternetMessage()
            }).call(
                email: LocalStorage.shared().email,
                token: code.text ?? ""
            )
        }
    }
    
    fileprivate func goToRegister(){
        weak var presentingViewController = self.presentingViewController

        self.dismiss(animated: true, completion: {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "registerVC") {
                presentingViewController?.present(vc, animated: true, completion: nil)
            }
        })
        
    }
    
    fileprivate func validateForm() -> Bool{
        
        if code.text == "" {
            showSwiftyMessage(title: "Verification Code is empty!", content: "Please fill in your verification code", theme: .warning)
            return false
        }
        
        return true
    }
    
}
