//
//  DetailViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/18/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import Kingfisher

class DetailViewController: BaseViewController {

    private var file: FileTO? = nil
    var fileId: Int? {
        didSet {
            configureView()
        }
    }

    @IBOutlet weak var filenameLable: RoundedLable!

    private var previewVC: PreviewViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        NotificationCenter.default.addObserver(self, selector: #selector(globalUpdate), name: NotificationNames.globalUpdate, object: nil)
    }
    
    @objc func globalUpdate(){
        file = FileDAO().getSingle(fileId: fileId ?? 0)
    }

    func configureView() {
        guard let file = FileDAO().getSingle(fileId: fileId ?? 0) else {
            return
        }
        self.file = file
        
        if filenameLable == nil {
            return
        }
        
        // Update the user interface for the detail item.
        if file.filePathOffline == "" {
            //show download
            return
        }
        previewVC?.file = file
        filenameLable.text = file.fileName
        
    }
    
    @IBAction func LinkClikced(_ sender: Any) {
        if let file = self.file {
            onFileLinksClicked(vc: self, file: file)
        }
    }
    @IBAction func editBtnClicked(_ sender: Any) {
        if let file = self.file {
            onRenameClicked(vc: self, item: file)
        }
    }
    
    @IBAction func moreBtnClicked(_ sender: Any) {
        if let file = self.file {
            showFileSheet(vc: self, file: file)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PreviewViewController {
            destination.file = self.file
            self.previewVC = destination
        }
    }
}

