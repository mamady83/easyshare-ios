//
//  ChangeNameViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/2/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class ChangeNameViewController: BaseViewController {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let storage = LocalStorage.shared()
        firstName.text = storage.firstName
        lastName.text = storage.lastName
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateForm() {
            submitBtn.showLoading()
            ApiClientUpdate(onSuccess: { [weak self] in
                self?.submitBtn.hideLoading()
                self?.dismiss(animated: true) {
                    showSwiftyMessage(title: "Done!", content: "Your name has been updated!", theme: .success)
                }
            }, onError: {[weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
            }).updateName(first_name: firstName.text ?? "",
                          last_name: lastName.text ?? "")
        }
    }
    
    fileprivate func validateForm() -> Bool{
        if firstName.text == "" {
            showSwiftyMessage(title: "First name is empty!", content: "Please fill in your first name", theme: .warning)
            return false
        }
        if lastName.text == "" {
            showSwiftyMessage(title: "Last name is empty!", content: "Please fill in your last name", theme: .warning)
            return false
        }
        return true
    }
    
}
