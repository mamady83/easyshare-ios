//
//  SendVerifyViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/1/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import Domain

class SendVerifyViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passwordEt: UITextField!
    @IBOutlet weak var passwordRepeatEt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        if validateForm() {
            submitBtn.showLoading()
            LocalStorage.shared().email = email.text ?? ""
            ApiRegister(onSuccess: {[weak self] in
                self?.submitBtn?.hideLoading()
                self?.goToVerifyCode()
                }, onError: {[weak self] (message) in
                    self?.submitBtn?.hideLoading()
                    showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
                }, onConnectionError: {[weak self] in
                    self?.submitBtn?.hideLoading()
                    showNoInternetMessage()
            }).call(
                email: email.text ?? "",
                password: passwordEt.text ?? "",
                password_confirmation: passwordRepeatEt.text ?? ""
            )
        }
    }
    
    fileprivate func goToVerifyCode(){
        weak var presentingViewController = self.presentingViewController

        self.dismiss(animated: true, completion: {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "verifyVC"){
                presentingViewController?.present(vc, animated: true, completion: nil)
            }
        })
        
        
    }
    fileprivate func validateForm() -> Bool{
        
        if email.text == "" {
            showSwiftyMessage(title: "Email is empty!", content: "Please fill in your email", theme: .warning)
            return false
        }
        
        if !(email.text ?? "").isValidEmail() {
            showSwiftyMessage(title: "Email is invalid!", content: "Please enter a valid a email address", theme: .warning)
            return false
        }
        if passwordEt.text == "" {
            showSwiftyMessage(title: "Password is empty!", content: "Please fill in your password", theme: .warning)
            return false
        }
        
        if passwordRepeatEt.text == "" {
            showSwiftyMessage(title: "Password repeat is empty!", content: "Please fill in your password repeat", theme: .warning)
            return false
        }
        
        if passwordRepeatEt.text != passwordRepeatEt.text {
            showSwiftyMessage(title: "Passwords do not match!", content: "First and second entered passwords are not the same", theme: .warning)
            return false
        }
        
        return true
    }
}
