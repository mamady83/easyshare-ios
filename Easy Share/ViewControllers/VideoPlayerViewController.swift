//
//  VideoPlayerViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/27/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import BMPlayer
import RealmSwift
import Domain

class VideoPlayerViewController: UIViewController, BMPlayerDelegate {
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        
    }
    
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        
    }
    
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        
    }
    
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        
    }
    
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        
    }
    

    override var shouldAutorotate: Bool {
        return true
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
    let player = BMPlayer()
    public var file : FileTO?
    let realm = try! Realm()
    override func viewDidLoad() {
        super.viewDidLoad()

        player.frame = view.bounds
       
        self.view.addSubview(player)
        player.snp.makeConstraints {make -> Void in
            make.edges.equalTo(view.snp.edges)
        }
        // Do any additional setup after loading the view.
        
        
        player.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true { return }
            self.dismiss(animated: true, completion: nil)
        }
        let videoURL = URL(fileURLWithPath: file?.filePathOffline ?? "")

        let asset = BMPlayerResource(url: videoURL, name: file?.fileName ?? "")
        player.setVideo(resource: asset)
            
        
        
        player.delegate = self
    
    }
 
    
    
     func canRotate() -> Void {}
}
