//
//  SplitViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {
    var previousVC : UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        preferredDisplayMode = .allVisible
        
        if let vc = previousVC {
            vc.dismiss(animated: false, completion: nil)
            previousVC = nil
        }
        
    }
    
}
