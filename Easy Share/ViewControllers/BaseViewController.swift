//
//  BaseViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class BaseViewController : UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBarAppearance()
    }
    
    func setNavBarAppearance() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]
            navigationController?.navigationBar.standardAppearance = appearance
        } else {
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]
        }
    }
    
    
}
class BaseTableViewController : UITableViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBarAppearance()
    }
    
    func setNavBarAppearance() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]

            let buttonAppearance = UIBarButtonItemAppearance(style: .plain)
            buttonAppearance.normal.titleTextAttributes = [.font : FontKit.roundedFont(ofSize: 16, weight: .bold)]

            appearance.buttonAppearance = buttonAppearance

            navigationController?.navigationBar.standardAppearance = appearance
        } else {
            
            navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 18, weight: .bold)]
            navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor(named: "colorTextPrimary")!, .font : FontKit.roundedFont(ofSize: 34, weight: .heavy)]
        }
    }
    
    
}
