//
//  UploadListViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/25/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import NetworkPlatform

class UploadListViewController: BaseTableViewController {
    
    
    let uploadDAO = UploadDAO()
    var items: [UploadTO] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "UploadCell", bundle: nil), forCellReuseIdentifier: "UploadCell")
        
        tableView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUpload), name: NotificationNames.globalUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUpload), name: NotificationNames.updateUpload, object: nil)
        refreshUpload()
    }
    
    @objc func refreshUpload(){
        items.removeAll()
        items.append(contentsOf: uploadDAO.all())
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UploadCell", for: indexPath) as! UploadCell
        cell.initWithItem(item: items[indexPath.row], isSingle: true)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        if item.status == UploadStatus.FAILED {
            UploadService.shared().retryUpload(uploadId: item.uploadId)
        }else if item.status == UploadStatus.UPLOADING {
            UploadService.shared().cancelUpload(uploadId: item.uploadId)
        }
    }
    
}
