//
//  RegisterViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/23/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import Domain

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var termsCheckbox: UIImageView!
    private var termsChecked = false
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsCheckbox.image = UIImage(named: "m_checkbox")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func termsClicked(_ sender: Any) {
        termsChecked = !termsChecked
        if termsChecked {
            termsCheckbox.image = UIImage(named: "m_checkbox_checked")
        }else {
            termsCheckbox.image = UIImage(named: "m_checkbox")
        }
        
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        
        if validateForm() {
            updateName()
        }
    }
    
    fileprivate func updateName(){
        submitBtn.showLoading()
        ApiClientUpdate(onSuccess: {[weak self] in
            self?.submitBtn.hideLoading()
            if let this = self {
                goToHome(source: this)
            }
            }, onError: {[weak self] (message) in
                self?.submitBtn.hideLoading()
                showSwiftyMessage(title: "There was an error!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.submitBtn.hideLoading()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                self?.submitBtn.hideLoading()
                if let this = self {
                    logoutUser(vc: this)
                }
        }).updateName(
            first_name: firstName.text ?? "",
            last_name: lastName.text ?? ""
        )
    }
    
    fileprivate func validateForm() -> Bool{
        
        if firstName.text == "" {
            showSwiftyMessage(title: "First name is empty!", content: "Please fill in your first name", theme: .warning)
            return false
        }
        
        if lastName.text == "" {
            showSwiftyMessage(title: "Last name is empty!", content: "Please fill in your last name", theme: .warning)
            return false
        }
        
        if !termsChecked {
            showSwiftyMessage(title: "Terms not accepted!", content: "You must accept SweetShare's Terms and Conditions in order to register", theme: .warning)
            return false
        }
        
        
        return true
    }
    
}
