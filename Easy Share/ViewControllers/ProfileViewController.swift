//
//  ProfileViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/20/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import MultiProgressView
import SafariServices
import Domain
import NetworkPlatform
import CropViewController
import ALCameraViewController
import Kingfisher

class ProfileViewController: BaseTableViewController, MultiProgressViewDataSource, MultiProgressViewDelegate, CropViewControllerDelegate  {
    
    
    @IBOutlet weak var spaceUsedLable: RoundedLable!
    @IBOutlet weak var emailLable: RoundedLable!
    @IBOutlet weak var nameLable: RoundedLable!
    @IBOutlet weak var accountStatusLable: RoundedLable!
    @IBOutlet weak var upgradeBtn: UIButton!
    @IBOutlet weak var vipCrown: UIImageView!
    @IBOutlet weak var avatar: UIButton!
    @IBOutlet weak var avatar_loding: UIActivityIndicatorView!
    var chartItems : [StorageTO] = Array()

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 9://help
            openHelp()
            break
        case 10://terms
            openTerms()
            break
        case 11://policy
            openPolicy()
            break
        case 12://logout
            showLogoutSheet(vc: self) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    logoutUser(vc: self)
                }
            }
        default:
            print("")
        }
    }
    
    
    fileprivate func openHelp(){
        let urlString = "https://sweetsha.re/help"
        if let url = URL(string: urlString) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    
    fileprivate func openTerms(){
        let urlString = "https://sweetsha.re/terms"
        if let url = URL(string: urlString) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    
    fileprivate func openPolicy(){
        let urlString = "https://sweetsha.re/policy"
        if let url = URL(string: urlString) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    func numberOfSections(in progressView: MultiProgressView) -> Int {
        return chartItems.count + 1
    }
    
    
    
    func progressView(_ progressView: MultiProgressView, viewForSection section: Int) -> ProgressViewSection {
        let sectionView = ProgressViewSection()
        
        if section < chartItems.count {
            sectionView.backgroundColor = chartItems[section].color
        }
        return sectionView
    }
    
    
    @IBOutlet weak var multiProgressView: MultiProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatar_loding.hidesWhenStopped = true
        avatar_loding.stopAnimating()
        onRefresh()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor(named: "colorTextPrimary")
        tableView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: Notification.Name("ProfileUpdated"), object: nil)
        
        setStorageChart()
    }
    
    
    fileprivate func setStorageChart(){
        
        let totlaStorage = LocalStorage.shared().totalStorage
        let availableBytes = totlaStorage - LocalStorage.shared().storageUsed
        
        let usedTxt = Units(bytes: Int64(LocalStorage.shared().storageUsed)).getReadableUnit()
        let totalTxt = Units(bytes: Int64(LocalStorage.shared().totalStorage)).getReadableUnit()
        spaceUsedLable.text = "\(usedTxt) of \(totalTxt) used"
        chartItems = StorageDAO().all()
        
        chartItems.append(
            StorageTO(title: "Free",
                      color: UIColor(named: "colorFree")!,
                      space: "",
                      value: availableBytes,
                      type: 0,
                      size: 0
            )
        )
        
        multiProgressView.dataSource = self
        multiProgressView.delegate = self
        multiProgressView.lineCap = .round
        
        multiProgressView.trackBackgroundColor = UIColor(named: "colorFree")
        
        UIView.animate(
            withDuration: 0.8,
            delay: 0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: 0,
            options: .curveLinear,
            animations: { [weak self] in
                if let cItems = self?.chartItems {
                    for i in 0..<cItems.count {
                        self?.multiProgressView.setProgress(section: i, to: Float(cItems[i].value))
                    }
                }
        })
        
    }
    
    @objc func onRefresh(){
        ApiMe(onSuccess: { [weak self] in
            self?.tableView.refreshControl?.endRefreshing()
            self?.loadData()
            }, onError: { [weak self] (message) in
                self?.tableView.refreshControl?.endRefreshing()
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.tableView.refreshControl?.endRefreshing()
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                if let this = self {
                    logoutUser(vc: this)
                }
        }).call()
    }
    
    @IBAction func avatarClicked(_ sender: Any) {
        
       let cameraViewController = CameraViewController { [weak self] image, asset in
            if let img = image {
                self?.dismiss(animated: true, completion: { [weak self] in
                    let cropViewController = CropViewController(croppingStyle: .circular, image: img)
                    cropViewController.delegate = self
                    
                    self?.present(cropViewController, animated: true, completion: nil)
                })

            }
        }
        present(cameraViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true, completion: nil)
        let resized = resizeImage(image: image, targetSize: CGSize(width: 200, height: 200))
        avatar.setImage(resized, for: .normal)
        updateAvatar(image: resized)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }
    
    fileprivate func updateAvatar(image: UIImage){
        avatar_loding.startAnimating()
        ApiClientUpdate(onSuccess: { [weak self] in
            self?.avatar_loding.stopAnimating()
            self?.loadData()
        }, onError: {[weak self] (message) in
            self?.avatar_loding.stopAnimating()
            showSwiftyMessage(title: "There was an error", content: message, theme: .error)
        }, onConnectionError: { [weak self] in
            self?.avatar_loding.stopAnimating()
            showNoInternetMessage()
        }, onInvalidToken: {[weak self] in
            self?.avatar_loding.stopAnimating()
            if let this = self {
                logoutUser(vc: this)
            }
        }).updateAvatar(avatar: image.jpegData(compressionQuality: 0.5))
    }
    
    @objc private func loadData(){
        let storage = LocalStorage.shared()
        
        nameLable.text = "\(storage.firstName) \(storage.lastName)"
        emailLable.text = storage.email
        if let imageUrl = URL(string: LocalStorage.shared().avatar){
            let resource = ImageResource(downloadURL: imageUrl, cacheKey: "avatar")
            avatar.kf.setImage(with: resource, for: .normal, placeholder: UIImage(named: "m_avatar_placeholder"))
        }
        if storage.isVIP == true {
            vipCrown.isHidden = false
            accountStatusLable.text = "VIP Account"
        }else {
            vipCrown.isHidden = true
            accountStatusLable.text = "Free Account"
        }
        
    }
}
