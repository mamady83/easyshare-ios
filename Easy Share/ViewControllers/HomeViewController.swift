//
//  HomeViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain
import SwiftMessages
import MaterialShowcase
import NetworkPlatform

class HomeViewController: BaseViewController {
    
    private let EMPTY_LAYOUT_TAG = 58023
    private let EMPTY_LAYOUT_NAME = "HomeEmptyLayout"
    private let INTERNET_LAYOUT_TAG = 50182
    private let INTERNET_LAYOUT_NAME = "InternetLayout"
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var menuCell: MenuCell!
    private var items : [HomeTO] = Array()
    private var sort = "date"
    private let homeDAO = HomeDAO()
    private let uploadDAO = UploadDAO()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTable()
        initSearchController()
        initMenuTab()
        initData()
        refreshData()
        registerEventListener()

    }
    
    private func registerEventListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUpload), name: NotificationNames.updateUpload, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(globalUpdate), name: NotificationNames.globalUpdate, object: nil)
    }
    
    
    private func initTable(){
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "FileCell", bundle: nil), forCellReuseIdentifier: "FileCell")
        tableView.register(UINib(nibName: "UploadCell", bundle: nil), forCellReuseIdentifier: "UploadCell")
        tableView.register(UINib(nibName: "FolderCell", bundle: nil), forCellReuseIdentifier: "FolderCell")
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = UIColor(named: "colorTextPrimary")
        tableView.refreshControl?.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    
    @objc func onRefresh(){
        refreshData()
    }
    
    private func initSearchController(){
        definesPresentationContext = true
        
        let resultsController = SearchViewController()
        
        
        let searchController = UISearchController(searchResultsController: resultsController)
        searchController.searchResultsUpdater = resultsController
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = true
        
        searchController.searchBar.placeholder = "Search your files"
        if #available(iOS 13.0, *) {
            searchController.searchBar.searchTextField.font = FontKit.roundedFont(ofSize: 14, weight: .medium)
        } else {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
        navigationItem.searchController = searchController
        
    }
    
    private func initMenuTab(){
        menuCell.onMoreClicked = {
            showSortSheet(vc: self, onSortChangedListener: { [weak self] newSort in
                self?.sort = newSort
                if self?.menuCell.recentFilesBadge.isHidden == false {
                    self?.initData()
                    self?.refreshData()
                }else {
                    self?.loadSharedFiles()
                }
            })
        }
        
        menuCell.onTabChanged = {[weak self] tab in
            switch tab {
            case MenuTab.RECENT_FILES:
                self?.initData()
                self?.refreshData()
            case MenuTab.SHARED_FILES:
                self?.loadSharedFiles()
            default:
                print("unknown")
            }
            
        }
        menuCell.selectRecent()
    }
    
    fileprivate func initData(){
        if menuCell.recentFilesBadge.isHidden == true {
            loadSharedFiles()
            return
        }
        items.removeAll()
        items.append(HomeTO(type: HomeCellType.MENU))
        
        let dbItems = homeDAO.getAllSorted(sort: sort)
        
        items.append(contentsOf: dbItems)
        
        refreshUpload()
        
        tableView.reloadData()
        
    }
    
    func tapTarget() {

        if !LocalStorage.shared().filesFirstTime || LocalStorage.shared().homeFirstTime {
            return
        }
        for i in 0..<items.count {
            if let row = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as? FileCell {
                let showcase = MaterialShowcase()
                showcase.setTargetView(button: row.ellipsisBtn, tapThrough: true)
                showcase.primaryText = "See what you can do"
                showcase.secondaryText = "Click the menu button on each file to access the action sheet"
                showcase.backgroundPromptColor = UIColor(named: "colorAccent")
                showcase.backgroundPromptColorAlpha = 0.9
                showcase.show(completion: {
                    LocalStorage.shared().filesFirstTime = false
                })
                break
            }
        }
        
    }
    fileprivate func loadSharedFiles(){
        items.removeAll()
        items.append(HomeTO(type: HomeCellType.MENU))
        tableView.reloadData()
        tableView.showLoading()
        
        tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        tableView.hideTag(tag: INTERNET_LAYOUT_TAG)
        ApiSharedFilesAndFolders(
            onSuccess: {[weak self] homeItems in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                self?.initSharedFilesData(newItems: homeItems)
                self?.checkIfEmpty()
            }, onError: {[weak self] (message) in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                showSwiftyMessage(title: "There was an erorr!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                self?.showInternetLayout()
            }, onInvalidToken: {[weak self] in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                if let this = self {
                    logoutUser(vc: this)
                }
            }
        ).call(sort: sort)
    }
    
    fileprivate func initSharedFilesData(newItems: [HomeTO]){
        items.removeAll()
        items.append(HomeTO(type: HomeCellType.MENU))
        
        items.append(contentsOf: newItems)
        
        tableView.reloadData()
    }
    
    
    fileprivate func refreshData(){
        if items.count == 1 {
            tableView.showLoading()
        }
        tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        tableView.hideTag(tag: INTERNET_LAYOUT_TAG)
        ApiHome(
            onSuccess: {[weak self] in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                self?.initData()
                self?.checkIfEmpty()
                self?.tapTarget()

            }, onError: {[weak self] (message) in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                showSwiftyMessage(title: "There was an erorr!", content: message, theme: .error)
            }, onConnectionError: {[weak self] in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                self?.showInternetLayout()
            }, onInvalidToken: {[weak self] in
                self?.tableView.hideLoading()
                self?.tableView.refreshControl?.endRefreshing()
                if let this = self {
                    logoutUser(vc: this)
                }
            }
        ).call()
    }
    
    fileprivate func showInternetLayout(){
        if items.count == 1 {
            tableView.showNib(nibName: INTERNET_LAYOUT_NAME, tag: INTERNET_LAYOUT_TAG)
        }
    }
    fileprivate func checkIfEmpty(){
        if items.count == 1 {
            tableView.showNib(nibName: EMPTY_LAYOUT_NAME, tag: EMPTY_LAYOUT_TAG)
        }else {
            tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
        }
    }
    
    @objc func refreshUpload(){
        var uploadIndex = -1
        for i in 0..<items.count {
            if items[i].type == HomeCellType.UPLOAD {
                uploadIndex = i
            }
        }
        if uploadIndex != -1 {
            items.remove(at: uploadIndex)
        }
        
        if !uploadDAO.all().isEmpty {
            tableView.hideTag(tag: EMPTY_LAYOUT_TAG)
            tableView.hideTag(tag: INTERNET_LAYOUT_TAG)
            
            let upload = HomeTO(
                type: HomeCellType.UPLOAD,
                item: UploadDAO().activeUpload()
            )
            
            items.insert(upload, at: 1)
        }
        tableView.reloadData()
    }
    @objc func globalUpdate(){
        initData()
        refreshData()
    }
    
}


//MARK:- Table View
extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        switch item.type {
        case HomeCellType.MENU:
            return menuCell
        case HomeCellType.HEADER:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            cell.initWithItem(item: item.item)
            return cell
        case HomeCellType.FILE:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as! FileCell
            cell.initWithItem(vc: self, item: item.item)
            return cell
        case HomeCellType.FOLDER:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath) as! FolderCell
            cell.initWithItem(vc: self, item: item.item)
            return cell
        case HomeCellType.UPLOAD:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadCell", for: indexPath) as! UploadCell
            cell.initWithItem(item: item.item, isSingle: false)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch items[indexPath.row].type {
        case HomeCellType.UPLOAD:
            performSegue(withIdentifier: "uploadListSegue", sender: nil)
        case HomeCellType.FILE:
            if let file = items[indexPath.row].item as? FileTO {
                onFileClicked(vc: self, fileId: file.fileId)
            }
        case HomeCellType.FOLDER:
            if let folder = items[indexPath.row].item as? FolderTO {
                onFolderClicked(vc: self, item: folder)
            }
        default:
            print("Cell Type not recognized")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.row]
        
        switch item.type {
        case HomeCellType.MENU:
            return 40
        case HomeCellType.FOLDER:
            return 72
        case HomeCellType.HEADER:
            return 28
        case HomeCellType.FILE:
            return 72
        case HomeCellType.UPLOAD:
            return 72
        default:
            return 50
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if items[indexPath.row].type == HomeCellType.FILE {
            return true
        }else {
            return false
        }
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(
            style: .destructive,
            title: "",
            handler: {[weak self] (action, view, completion) in
                if let this = self {
                    showDeleteSheet(vc: this, item: this.items[indexPath.row].item)
                }
                completion(false)
                
            })
        if let cgImageX =  UIImage(named: "m_row_delete")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImageX, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor(named: "colorBackground")
        
        let configuration = UISwipeActionsConfiguration(actions: [action])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
}
