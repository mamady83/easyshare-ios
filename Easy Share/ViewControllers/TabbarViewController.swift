//
//  TabbarViewController.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/19/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import NetworkPlatform
import Foundation
import Domain
import MaterialShowcase

class TabbarViewController: UITabBarController, UITabBarControllerDelegate, MaterialShowcaseDelegate{
    
    
    var tempSelectedFolderId: Int = 0
    var tempFileUrl: URL? = nil
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        if let items = tabBar.items {
            for item in items {
                if item.title == "" {
                    item.title = nil
                    item.imageInsets = UIEdgeInsets(top: 6,left: 0,bottom: -6,right: 0)
                }
                
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.showTapTarget()
        }
    }
    
    func showCaseDidDismiss(showcase: MaterialShowcase, didTapTarget: Bool) {
        showUploadSheet(vc: self)
    }
    
    func showTapTarget(){
        if !LocalStorage.shared().homeFirstTime {
            return
        }
        let showcase = MaterialShowcase()
        showcase.setTargetView(tabBar: tabBar, itemIndex: 3)
        showcase.primaryText = "Lets Get Started"
        showcase.secondaryText = "Start by Uploading a file in SweetShare"
        showcase.backgroundPromptColor = UIColor(named: "colorBlue")
        showcase.backgroundPromptColorAlpha = 0.9
        showcase.show(completion: {
            LocalStorage.shared().homeFirstTime = false
        })
        showcase.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is MoreViewController {
            showUploadSheet(vc: self)
            return false
        }
        return true
    }
    
    func launchImagePicker(source: UIImagePickerController.SourceType){
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        if let types = UIImagePickerController.availableMediaTypes(for: source)  {
            imagePicker.mediaTypes = types
        }
        
        imagePicker.sourceType = source
        present(imagePicker, animated: true, completion: nil)
    }
    
    func launchDocumentPicker(){
        let picker = UIDocumentPickerViewController(documentTypes: AllowedFileTypes.docsTypes, in: .import)
        picker.delegate = self
        picker.modalPresentationStyle = .formSheet
        picker.allowsMultipleSelection = false
        self.present(picker, animated: true, completion: nil)
    }
    
    
    fileprivate func launchFolderChooser(){
        if let choosefolderVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "moveVC") as? MoveViewController {
            choosefolderVC.moveType = MOVE_TYPE_CHOOSE_FOLDER
            choosefolderVC.onFolderSelected = { [weak self] folder in
                self?.tempSelectedFolderId = folder.folderId
                self?.createDownload()
            }
            present(choosefolderVC, animated: true, completion: nil)
        }
    }
    
    fileprivate func createDownload(){
        if let filepath = tempFileUrl {
            UploadService.shared().addToUploadQueue(
                uploadId: Int.random(in: 1..<1000),
                filename: filepath.lastPathComponent,
                folderId: tempSelectedFolderId,
                filepath: filepath,
                filesize: 0, //FIXME: determine file size
                filetype: 0, //FIXME: determine file type
                fileExtension: filepath.pathExtension.lowercased()
            )
        }
        
    }
}

//MARK:- Picker

extension TabbarViewController: UIDocumentPickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            return
        }
        
        tempFileUrl = url
        launchFolderChooser()
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let url = info[.imageURL] as? URL{
            tempFileUrl = url
            picker.dismiss(animated: true, completion: nil)
            launchFolderChooser()
        }else if let image = info[.originalImage] as? UIImage{
            
            var imagename = "picked_image.png"
            if let imgUrl = info[.imageURL] as? URL{
                imagename = imgUrl.lastPathComponent
            }
            
            let docDirURL = try! FileManager.default.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: true)
            let fileURL = docDirURL.appendingPathComponent(imagename)
            
            
            let data = image.pngData()! as NSData
            
            do{
                try data.write(to: fileURL , options: .noFileProtection)
            }catch{
                print(error)
            }
            
            
            tempFileUrl = fileURL
            picker.dismiss(animated: true, completion: nil)
            launchFolderChooser()
            
        }
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
    
}
