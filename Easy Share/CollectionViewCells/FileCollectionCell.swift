//
//  FileCollectionCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class FileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLable: RoundedLable!
    @IBOutlet weak var subtitleLable: RoundedLable!
    @IBOutlet weak var fileImage: UIImageView!
    
    @IBOutlet weak var moreBtn: UIButton!
    var onEllipsisClickedClosure : (()->())?
    
    func initWithItem(vc: UIViewController, item: FileTO) {
        titleLable.text = item.fileName
        subtitleLable.text = "12MB"//FIXME: set real value
        
        switch item.fileType {
        case FileType.PHOTO:
            fileImage.image = UIImage(named: "m_image_placeholder")
            fileImage.backgroundColor = UIColor(named: "colorImageOpaque")
        case FileType.MUSIC:
            fileImage.image = UIImage(named: "m_music_placeholder")
            fileImage.backgroundColor = UIColor(named: "colorMusicOpaque")
        case FileType.VIDEO:
            fileImage.image = UIImage(named: "m_video_placeholder")
            fileImage.backgroundColor = UIColor(named: "colorVideoOpaque")
        case FileType.DOCUMENT:
            fileImage.image = UIImage(named: "m_file_placeholder")
            fileImage.backgroundColor = UIColor(named: "colorFileOpaque")
        default:
            fileImage.image = UIImage(named: "m_image_placeholder")
        }
        onEllipsisClickedClosure = {
            showFileSheet(vc: vc, file: item)
        }
    }
    @IBAction func onEllipsisClicked(_ sender: Any) {
        onEllipsisClickedClosure?()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        var view = moreBtn.hitTest(moreBtn.convert(point, from: self), with: event)
        if view == nil {
            view = super.hitTest(point, with: event)
        }

        return view
    }
}
