//
//  OnboardingCollectionCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import SnapKit
import Domain

class OnboardingCollectionCell : UICollectionViewCell {
    
    
    let titleLable: UILabel = {
        let label = UILabel()
        label.font = FontKit.roundedFont(ofSize: 20, weight: .bold)
        label.textColor = UIColor(named: "colorTextPrimary")
        label.text = "test"
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let subtitleLable: UILabel = {
        let label = UILabel()
        label.font = FontKit.roundedFont(ofSize: 14, weight: .semibold)
        label.textColor = UIColor(named: "colorTextSecondary")
        label.text = "test description"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    func addViews(){
        backgroundColor = UIColor.clear
        
        addSubview(titleLable)
        addSubview(subtitleLable)
        
        
        titleLable.snp.makeConstraints { make -> Void in
            make.centerX.equalTo(snp.centerX)
            make.left.equalTo(snp.left)
            make.right.equalTo(snp.right)
            make.top.equalTo(snp.centerY).offset(50)
        }
        
        subtitleLable.snp.makeConstraints { make -> Void in
            make.centerX.equalTo(snp.centerX)
            make.left.equalTo(snp.left)
            make.right.equalTo(snp.right)
            make.top.equalTo(titleLable.snp.bottom).offset(12)
        }
        
    }
    
    func initWithItem(item: OnboardingTO){
        titleLable.text = item.title
        subtitleLable.text = item.subtitle
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
