//
//  FolderCollectionCell.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import Domain

class FolderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var titleLable: RoundedLable!
    @IBOutlet weak var subtitleLable: RoundedLable!
    
    var onEllipsisClickedClosure : (()->())?
    
    func initWithItem(vc: UIViewController, item: FolderTO) {
        titleLable.text = item.folderName
        subtitleLable.text = "\(item.filesCount) files"
        
        onEllipsisClickedClosure = {
            showFolderSheet(vc: vc, folder: item)
        }
    }
    @IBAction func onEllipsisClicked(_ sender: Any) {
        onEllipsisClickedClosure?()
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
           var view = moreBtn.hitTest(moreBtn.convert(point, from: self), with: event)
           if view == nil {
               view = super.hitTest(point, with: event)
           }

           return view
       }
}
