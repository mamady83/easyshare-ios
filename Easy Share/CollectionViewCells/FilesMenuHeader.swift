//
//  FilesMenuHeader.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/21/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit

class FilesMenuHeader: UICollectionReusableView {
    
    @IBOutlet weak var sortTitle: RoundedLable!
    var onMoreClickedClosure: (()->())?
    
    @IBAction func onMoreBtnClicked(_ sender: Any) {
        onMoreClickedClosure?()
    }
}
