//
//  AppDelegate.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/18/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import UIKit
import GoogleSignIn
import Domain
import NetworkPlatform
import RealmSwift
import IQKeyboardManagerSwift
import StoreKit
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    var window: UIWindow?
    
    let iapObserver = StoreObserver()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        SKPaymentQueue.default().add(iapObserver)
        
        IQKeyboardManager.shared.enable = true
        
        self.window?.tintColor = UIColor(named: "colorTextSecondary")
        UITabBar.appearance().tintColor = UIColor(named: "colorAccent")!
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: FontKit.roundedFont(ofSize: 12, weight: .semibold)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: FontKit.roundedFont(ofSize: 12, weight: .semibold)], for: .selected)
        
        
        
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSAttributedString.Key.font:  FontKit.roundedFont(ofSize: 12, weight: .semibold),
            NSAttributedString.Key.foregroundColor: UIColor(named: "colorTextSecondary")!], for: UIControl.State.normal)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSAttributedString.Key.font:  FontKit.roundedFont(ofSize: 12, weight: .semibold),
            NSAttributedString.Key.foregroundColor: UIColor(named: "colorTextSecondary")!], for: UIControl.State.selected)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSAttributedString.Key.font:  FontKit.roundedFont(ofSize: 12, weight: .semibold),
            NSAttributedString.Key.foregroundColor: UIColor(named: "colorTextSecondary")!], for: UIControl.State.highlighted)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSAttributedString.Key.font: FontKit.roundedFont(ofSize: 12, weight: .semibold),
            NSAttributedString.Key.foregroundColor: UIColor(named: "colorTextSecondary")!], for: UIControl.State.disabled)
        
        GIDSignIn.sharedInstance().clientID = "742052045403-jn7il06q8u8gfd3a6r9s1va5av9s5fnc.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        
        
        
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        var initialViewController = storyboard.instantiateViewController(withIdentifier: "splitVC")
        
        if !LocalStorage.shared().isLoggedIn {
            if LocalStorage.shared().passedOnboarding {
                initialViewController = storyboard.instantiateViewController(withIdentifier: "loginVC")
            }else {
                initialViewController = storyboard.instantiateViewController(withIdentifier: "onboardingVC")
            }
            
        }
        
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        
        
        ApiFolderList(onSuccess: nil,onError: nil,onConnectionError: nil,onInvalidToken: nil).call(page: 1, count: 999, sort: "")
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if !GIDSignIn.sharedInstance().handle(url) {
            print(url.absoluteString)
        }
        return true
    }
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        //      let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        //      let fullName = user.profile.name
        //      let givenName = user.profile.givenName
        //      let familyName = user.profile.familyName
        //      let email = user.profile.email
        
        print(user.profile.name ?? "")
        print(user.profile.email ?? "")
        print(idToken ?? "")
        
        NotificationCenter.default.post(name: Notification.Name("LoggedInGoogle"), object: nil, userInfo: ["token": idToken ?? ""])
        
        
        // ...
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    func application(
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([UIUserActivityRestoring]?
        ) -> Void) -> Bool {
        
        // 1
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb, let url = userActivity.webpageURL else {
            return false
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
                
        let shareVC = ShareViewController()
        
        shareVC.url = url
        
        self.window?.rootViewController = shareVC
        self.window?.makeKeyAndVisible()
        
        
        return false
    }
    
    
    
}

