//
//  LogoutSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 8/2/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages


class LogoutSheet : MessageView {
  
    var onLogoutClicked: (()->Void)?
    
  
    @IBAction func logoutClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        onLogoutClicked?()
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        SwiftMessages.hideAll()
    }
    
}

func showLogoutSheet(vc: UIViewController, onLogoutClicked: (()->Void)?){
    var view: LogoutSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "LogoutSheet")
    }catch {
        print(error)
    }
    view?.onLogoutClicked = onLogoutClicked
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

