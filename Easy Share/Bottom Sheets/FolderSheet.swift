//
//  FolderSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import Domain

class FolderSheet : MessageView {
    
    var folder: FolderTO!
    var vc: UIViewController!

    @IBAction func renameClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        onRenameClicked(vc: vc, item: folder!)
    }
    @IBAction func deleteClicked(_ sender: Any) {
        SwiftMessages.hide(id: "FolderSheet")
        showDeleteSheet(vc: vc, item: folder!)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        id = "FolderSheet"
    }
    
}

func showFolderSheet(vc: UIViewController, folder: FolderTO){
    var view: FolderSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "FolderSheet")
    }catch {
        print(error)
    }
    view?.folder = folder
    view?.vc = vc
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

