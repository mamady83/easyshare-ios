//
//  FileSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//


import Foundation
import UIKit
import SwiftMessages
import Domain
import MaterialShowcase

class FileSheet : MessageView {
    
    

    @IBOutlet weak var linkImage: UIImageView!
    var file: FileTO!
    var vc: UIViewController!
    

    @IBAction func createLinkClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        onFileLinksClicked(vc: vc, file: file)
        //b onCreateLinkClicked(vc: vc, file: file)
    }
    @IBAction func exportClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        
        let url = URL(fileURLWithPath: file.filePathOffline)
        
        print("trying to share \(url.path)")
        print("file exists \(FileManager.default.fileExists(atPath: url.path))")
        
        if file.filePathOffline != "", FileManager.default.fileExists(atPath: url.path) {
            shareFile(vc: vc, path: file.filePathOffline)
        }else {
            onFileClicked(vc: vc, fileId: file.fileId)
        }
    }
    
    
    @IBAction func moveClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        onMoveClicked(vc: vc, item: file!)
    }
    @IBAction func renameClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        onRenameClicked(vc: vc, item: file!)
    }
    @IBAction func deleteClicked(_ sender: Any) {
        SwiftMessages.hide(id: "FileSheet")
        showDeleteSheet(vc: vc, item: file)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        id = "FileSheet"
    }
    override func awakeFromNib() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.tapTarget()
        }
    }
    func tapTarget() {
        
        if !LocalStorage.shared().filesSheetFirstTime {
            return
        }
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: linkImage)
        showcase.primaryText = "Start Sharing!"
        showcase.secondaryText = "You can create and manage your links through this menu"
        showcase.backgroundPromptColor = UIColor(named: "colorImage")
        showcase.backgroundPromptColorAlpha = 0.8
        showcase.targetHolderRadius = 28
        showcase.show(completion: {
            LocalStorage.shared().filesSheetFirstTime = false
        })
        
        
    }
    
    
}
func showFileSheet(vc: UIViewController, file: FileTO){
    var view: FileSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "FileSheet")
    }catch {
        print(error)
    }
    view?.file = file
    view?.vc = vc
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

