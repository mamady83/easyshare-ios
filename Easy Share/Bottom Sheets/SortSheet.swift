//
//  SortSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages

class SortSheet : MessageView {
    
    var onSortChangedListener: ((String)->())?
    
    @IBAction func sortByNameTapped(_ sender: Any) {
        SwiftMessages.hideAll()
        onSortChangedListener?("alphabet")
    }
    
    @IBAction func sortByDateTapped(_ sender: Any) {
        SwiftMessages.hideAll()
        onSortChangedListener?("date")
    }
    
}

func showSortSheet(vc: UIViewController, onSortChangedListener: ((String)->())?){
    var view: SortSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "SortSheet")
    }catch {
        print(error)
    }
    view?.onSortChangedListener = onSortChangedListener
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

