//
//  UploadSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import Domain

class UploadSheet : MessageView {
    
    
    var file: FileTO!
    var vc: UIViewController?
    @IBAction func takePhotoClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        if let tabbar = vc as? TabbarViewController {
            tabbar.launchImagePicker(source: .camera)
        }
    }
    @IBAction func uploadPhotoClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        if let tabbar = vc as? TabbarViewController {
            tabbar.launchImagePicker(source: .photoLibrary)
        }
    }
    @IBAction func uploadFileClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        if let tabbar = vc as? TabbarViewController {
            tabbar.launchDocumentPicker()
        }
    }
    @IBAction func createFolderClicked(_ sender: Any) {
        SwiftMessages.hideAll()
        if let this = vc {
            onCreateFolderClicked(vc: this)
        }
    }
    
}
func showUploadSheet(vc: UIViewController){
    var view: UploadSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "UploadSheet")
    }catch {
        print(error)
    }
    view?.vc = vc
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

