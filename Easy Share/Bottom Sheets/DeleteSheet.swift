//
//  DeleteSheet.swift
//  Easy Share
//
//  Created by mohamd yeganeh on 7/26/20.
//  Copyright © 2020 Techlaim. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import Domain
import NetworkPlatform

class DeleteSheet : MessageView {
  
    @IBOutlet weak var deleteBtn: UIButton!
    var item: Any?
    var vc: UIViewController?
    
    @IBAction func deleteClicked(_ sender: Any) {
        if let folder = item as? FolderTO {
            SwiftMessages.hideAll()
            ApiFolderDelete(onSuccess: {
                EventDispatcher.shared().sendGlobalUpdateEvent()
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                if let this = self?.vc {
                    logoutUser(vc: this)
                }
            }).call(folderId: folder.folderId)
        }else if let file = item as? FileTO {
            SwiftMessages.hideAll()
            ApiFileDelete(onSuccess: {
                EventDispatcher.shared().sendGlobalUpdateEvent()
            }, onError: { (message) in
                showSwiftyMessage(title: "There was an error", content: message, theme: .error)
            }, onConnectionError: {
                showNoInternetMessage()
            }, onInvalidToken: {[weak self] in
                if let this = self?.vc {
                    logoutUser(vc: this)
                }
            }).call(fileId: file.fileId)
            
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        SwiftMessages.hideAll()
    }
    
}

func showDeleteSheet(vc: UIViewController, item: Any?){
    var view: DeleteSheet? = nil
    do {
        view = try SwiftMessages.viewFromNib(named: "DeleteSheet")
    }catch {
        print(error)
    }
    view?.item = item
    view?.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    view?.backgroundView?.layer.cornerRadius = 18
    var config = SwiftMessages.Config()
    config.presentationStyle = .bottom
    config.duration = .forever
    config.dimMode = .color(color: (UIColor(named: "colorBackground")?.withAlphaComponent(0.4))!, interactive: true)
    config.interactiveHide = true
    if let sheet = view {
        SwiftMessages.show(config: config, view: sheet)
    }
}

